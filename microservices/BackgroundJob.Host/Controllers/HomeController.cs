﻿using Microsoft.AspNetCore.Mvc;
using Volo.Abp.AspNetCore.Mvc;

namespace Bridge.BackgroundJobService.Host.Controllers
{
    public class HomeController : AbpController
    {
        [HttpGet]
        public ActionResult Index()
        {
            return Redirect("/swagger");
        }
    }
}
