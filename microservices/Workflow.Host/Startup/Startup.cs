﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Volo.Abp.Identity.Settings;
using Volo.Abp.Settings;

namespace Bridge.WorkflowService.Host.Startup
{
    /// <summary>
    /// 
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            // Configure Abp and Dependency Injection
            services.AddApplication<WorkflowWebHostModule>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        /// <param name="loggerFactory"></param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IHostApplicationLifetime applicationLifetime)
        {

            app.ApplicationServices.GetService<ISettingDefinitionManager>().Get(IdentitySettingNames.Password.RequireDigit).DefaultValue = false.ToString();
            app.ApplicationServices.GetService<ISettingDefinitionManager>().Get(IdentitySettingNames.Password.RequireLowercase).DefaultValue = false.ToString();
            app.ApplicationServices.GetService<ISettingDefinitionManager>().Get(IdentitySettingNames.Password.RequireUppercase).DefaultValue = false.ToString();
            app.ApplicationServices.GetService<ISettingDefinitionManager>().Get(IdentitySettingNames.Password.RequireNonAlphanumeric).DefaultValue = false.ToString();
            app.ApplicationServices.GetService<ISettingDefinitionManager>().Get(IdentitySettingNames.Password.RequiredLength).DefaultValue = 6.ToString();

            app.InitializeApplication();
        }
    }
}
