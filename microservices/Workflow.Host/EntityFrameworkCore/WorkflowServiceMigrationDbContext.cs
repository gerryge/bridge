﻿
using Microsoft.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;

namespace Bridge.Workflow.EntityFrameworkCore
{
    public class WorkflowServiceMigrationDbContext : AbpDbContext<WorkflowServiceMigrationDbContext>
    {
        public WorkflowServiceMigrationDbContext(
            DbContextOptions<WorkflowServiceMigrationDbContext> options
            ) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            //builder.ConfigureAuditLogging();


            builder.ConfigWorkflowCore();
            //builder.ConfigureBaseAgencyDept();
            //builder.ConfigureSupplier();
            //builder.ConfigureBaseLogs();
        }
    }
}
