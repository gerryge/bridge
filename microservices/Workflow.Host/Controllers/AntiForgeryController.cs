using Bridge.Workflow;
using Microsoft.AspNetCore.Antiforgery;
using Volo.Abp.AspNetCore.Mvc.AntiForgery;

namespace Bridge.WorkflowService.Host.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class AntiForgeryController : WorkflowController
    {
        private readonly IAntiforgery _antiforgery;
        private readonly IAbpAntiForgeryManager _antiForgeryManager;

        public AntiForgeryController(IAntiforgery antiforgery, IAbpAntiForgeryManager antiForgeryManager)
        {
            _antiforgery = antiforgery;
            _antiForgeryManager = antiForgeryManager;
        }

        public void GetToken()
        {
            _antiforgery.SetCookieTokenAndHeader(HttpContext);
        }

        public void SetCookie()
        {
            _antiForgeryManager.SetCookie();
        }
    }
}
