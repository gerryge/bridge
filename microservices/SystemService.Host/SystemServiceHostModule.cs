using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.Extensions.DependencyInjection;
using StackExchange.Redis;
using Microsoft.OpenApi.Models;
using Bridge.Shared;
using Bridge.Shared.Extensions;
using Volo.Abp;
using Volo.Abp.AspNetCore.MultiTenancy;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.Auditing;
using Volo.Abp.AuditLogging.EntityFrameworkCore;
using Volo.Abp.Autofac;
using Volo.Abp.Domain.Entities.Events.Distributed;
using Volo.Abp.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore.SqlServer;
using Volo.Abp.EventBus.RabbitMq;
using Volo.Abp.Identity;
using Volo.Abp.Identity.EntityFrameworkCore;
using Volo.Abp.Localization;
using Volo.Abp.Modularity;
using Volo.Abp.MultiTenancy;
using Volo.Abp.PermissionManagement.EntityFrameworkCore;
using Volo.Abp.Security.Claims;
using Volo.Abp.SettingManagement.EntityFrameworkCore;
using Volo.Abp.TenantManagement.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore.MySQL;
using Volo.Abp.AspNetCore.Mvc.AntiForgery;
using Bridge.Shared.Middlewares;
using Microsoft.AspNetCore.Mvc;
using Bridge.Shared.Filter;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Volo.Abp.Json;
using Bridge.System.Application;
using Microsoft.AspNetCore.Http;
using Bridge.Shared.Helper;

namespace Bridge.IdentityService.Host
{
    [DependsOn(
        
        typeof(AbpAutofacModule),
        typeof(AbpAspNetCoreMvcModule),
         //typeof(AbpEventBusRabbitMqModule),
         typeof(AbpEntityFrameworkCoreMySQLModule),
        //typeof(AbpEntityFrameworkCoreSqlServerModule),
        typeof(AbpAuditLoggingEntityFrameworkCoreModule),
        typeof(AbpPermissionManagementEntityFrameworkCoreModule),
        typeof(AbpSettingManagementEntityFrameworkCoreModule),
        typeof(AbpIdentityHttpApiModule),
        typeof(AbpIdentityEntityFrameworkCoreModule),
        typeof(AbpIdentityApplicationModule),
        typeof(AbpAspNetCoreMultiTenancyModule),
        typeof(AbpTenantManagementEntityFrameworkCoreModule),
        typeof(SystemApplicationModule),
        typeof(SystemEntityFrameworkCoreModule),
        typeof(SystemHttpApiModule)
        
        )]
    public class IdentityServiceHostModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            var configuration = context.Services.GetConfiguration();
            //注入公共的服务
            context.Services.ConfigureCommonServices(new InitializerOptions());
            Configure<AbpMultiTenancyOptions>(options =>
            {
                options.IsEnabled = BridgeConsts.IsMultiTenancyEnabled;
            });
            Configure<AbpAspNetCoreMvcOptions>(options =>
            {
                options.ConventionalControllers.Create(typeof(SystemApplicationModule).Assembly, option =>
                {
                    option.TypePredicate = type => { return true; };
                });
                
            });
            Configure<AbpJsonOptions>(options =>
            {

                options.OutputDateTimeFormat = "yyyy-MM-dd HH:mm:ss"; //对类型为DateTimeOffset生效
            }
           );
            Configure<MvcNewtonsoftJsonOptions>(options =>
            {
                options.SerializerSettings.DateFormatString = "yyyy-MM-dd HH:mm:ss";//对类型为DateTime的生效
            });
            //context.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            //    .AddJwtBearer(options =>
            //    {
            //        options.Authority = configuration["AuthServer:Authority"];
            //        options.Audience = configuration["AuthServer:ApiName"];
            //        options.RequireHttpsMetadata = Convert.ToBoolean(configuration["AuthServer:RequireHttpsMetadata"]);
            //        options.TokenValidationParameters.ValidateIssuer = false;
            //    });

            //context.Services.AddSwaggerGen(options =>
            //{
            //    options.SwaggerDoc("v1", new OpenApiInfo {Title = "Identity Service API", Version = "v1"});
            //    //启用Swagger中的【Authorize】按钮。这样就不用每个项目的AddSwaggerGen中单独配置了
            //    options.AddAuthenticationHeader();
            //});

            //Configure<AbpLocalizationOptions>(options =>
            //{
            //    options.Languages.Add(new LanguageInfo("en", "en", "English"));
            //});
            context.Services.AddMvc(options =>
            {
                //全局路由前缀配置
                options.UseCentralRoutePrefix(new RouteAttribute("api/v1/sys"));
                //全局授权配置
               // options.Filters.Add<CoustomAuthorizeFilter>();
   
            });
            //Configure<AbpDbContextOptions>(options =>
            //{
            //    options.UseDbCon();

            //});

            Configure<AbpDistributedEntityEventOptions>(options =>
            {
                options.AutoEventSelectors.Add<IdentityUser>();
            });
            //跨域
            //context.Services.AddCors(options => options.AddPolicy("CorsPolicy",
            //builder =>
            //{
            //    builder.AllowAnyMethod()
            //         .SetIsOriginAllowed(_ => true)
            //         .AllowAnyHeader()
            //         .AllowCredentials();
            //}));
            //context.Services.UseRedis(configuration);
            context.Services.AddCaptcha(configuration);

            Configure<AbpAuditingOptions>(options =>
            {
                options.IsEnabledForGetRequests = true;
                options.ApplicationName = "IdentityServer";
            });
            //Configure<AbpAntiForgeryOptions>(options =>
            //{
            //    options.AutoValidate = false; //表示不验证防伪令牌
            //});
           
        }

        public override void OnApplicationInitialization(ApplicationInitializationContext context)
        {
            var app = context.GetApplicationBuilder();
            
            app.UseCorrelationId();
            app.UseStaticFiles();
            app.UseRouting();
            //跨域
            app.UseCors("CorsPolicy");
            app.UseAuthentication();
            app.UseAbpClaimsMap();

            if (BridgeConsts.IsMultiTenancyEnabled)
            {
                app.UseMultiTenancy();
            }

            app.UseAbpRequestLocalization(); //TODO: localization?
            //swagger页面访问授权
            app.UseSwaggerAuthorized();
            app.UseSwagger();
            app.UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint("/swagger/v1/swagger.json", "Identity Service API");
            });
            app.UseAuditing();
            //app.UseMiddleware<GlobalExceptionMiddleware>();
            app.UseConfiguredEndpoints();
            //注入Http
           HttpHelper.Configure(app.ApplicationServices.GetRequiredService<IHttpContextAccessor>());
        }
    }
}
