﻿using Microsoft.AspNetCore.Mvc;
using Volo.Abp.AspNetCore.Mvc;

namespace Bridge.IdentityService.Host.Controllers
{
    public class HomeController : AbpController
    {
        [HttpGet]
        public ActionResult Index()
        {
            return Redirect("/swagger");
        }
    }
}
