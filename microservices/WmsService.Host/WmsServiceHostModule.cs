using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.Extensions.DependencyInjection;
using StackExchange.Redis;
using Microsoft.OpenApi.Models;
using Bridge.Shared;
using Bridge.Shared.Extensions;
using Volo.Abp;
using Volo.Abp.AspNetCore.MultiTenancy;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.Auditing;
using Volo.Abp.AuditLogging.EntityFrameworkCore;
using Volo.Abp.Autofac;
using Volo.Abp.Domain.Entities.Events.Distributed;
using Volo.Abp.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore.SqlServer;
using Volo.Abp.EventBus.RabbitMq;
using Volo.Abp.Identity;
using Volo.Abp.Localization;
using Volo.Abp.Modularity;
using Volo.Abp.MultiTenancy;
using Volo.Abp.PermissionManagement.EntityFrameworkCore;
using Volo.Abp.Security.Claims;
using Volo.Abp.SettingManagement.EntityFrameworkCore;
using Volo.Abp.TenantManagement.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore.MySQL;
using Volo.Abp.AspNetCore.Mvc.AntiForgery;
using Bridge.Shared.Filter;
using Microsoft.AspNetCore.Mvc;

namespace Bridge.WmsService.Host
{
    [DependsOn(
        typeof(AbpAutofacModule),
        typeof(AbpAspNetCoreMvcModule),
         //typeof(AbpEventBusRabbitMqModule),
         typeof(AbpEntityFrameworkCoreMySQLModule),
        //typeof(AbpEntityFrameworkCoreSqlServerModule),
        typeof(AbpAuditLoggingEntityFrameworkCoreModule),
        typeof(AbpPermissionManagementEntityFrameworkCoreModule),
        typeof(AbpSettingManagementEntityFrameworkCoreModule),
        typeof(AbpAspNetCoreMultiTenancyModule),
        typeof(AbpTenantManagementEntityFrameworkCoreModule),
        typeof(WmsApplicationModule),
        typeof(WmsEntityFrameworkCoreModule),
        typeof(WmsHttpApiModule)
        )]
    public class WmsServiceHostModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            var configuration = context.Services.GetConfiguration();

            context.Services.ConfigureCommonServices(new InitializerOptions());
            //Configure<AbpMultiTenancyOptions>(options =>
            //{
            //    options.IsEnabled = BridgeConsts.IsMultiTenancyEnabled;
            //});

            //context.Services.AddAuthentication("Bearer")
            //    .AddIdentityServerAuthentication(options =>
            //    {
            //        options.Authority = configuration["AuthServer:Authority"];
            //        options.ApiName = configuration["AuthServer:ApiName"];
            //        options.RequireHttpsMetadata = Convert.ToBoolean(configuration["AuthServer:RequireHttpsMetadata"]);
            //    });

            //context.Services.AddSwaggerGen(options =>
            //{
            //    options.SwaggerDoc("v1", new OpenApiInfo {Title = "Wms Service API", Version = "v1"});
            //    options.DocInclusionPredicate((docName, description) => true);
            //    options.CustomSchemaIds(type => type.FullName);
            //    options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
            //    {
            //        Description = "请输入JWT令牌，例如：Bearer 12345abcdef",
            //        Name = "Authorization",
            //        In = ParameterLocation.Header,
            //        Type = SecuritySchemeType.ApiKey,
            //        Scheme = "Bearer"
            //    });

            //    options.AddSecurityRequirement(new OpenApiSecurityRequirement()
            //      {
            //        {
            //          new OpenApiSecurityScheme
            //          {
            //            Reference = new OpenApiReference
            //              {
            //                Type = ReferenceType.SecurityScheme,
            //                Id = "Bearer"
            //              },
            //              Scheme = "oauth2",
            //              Name = "Bearer",
            //              In = ParameterLocation.Header,

            //            },
            //            new List<string>()
            //          }
            //        });
            //});

            //Configure<AbpLocalizationOptions>(options =>
            //{
            //    options.Languages.Add(new LanguageInfo("en", "en", "English"));
            //});
            context.Services.AddMvc(options =>
            {
                //全局路由前缀配置
                options.UseCentralRoutePrefix(new RouteAttribute("api/v1/wms"));
                //全局授权配置
                options.Filters.Add<CoustomAuthorizeFilter>();

            });
            //Configure<AbpDbContextOptions>(options =>
            //{
            //    options.UseDbCon();
            //});

            Configure<AbpDistributedEntityEventOptions>(options =>
            {
                options.AutoEventSelectors.Add<IdentityUser>();
            });
            //跨域
            //context.Services.AddCors(options => options.AddPolicy("CorsPolicy",
            //builder =>
            //{
            //    builder.AllowAnyMethod()
            //         .SetIsOriginAllowed(_ => true)
            //         .AllowAnyHeader()
            //         .AllowCredentials();
            //}));
            //context.Services.AddStackExchangeRedisCache(options =>
            //{
            //    options.Configuration = configuration["Redis:Configuration"];
            //});

            Configure<AbpAuditingOptions>(options =>
            {
                options.IsEnabledForGetRequests = true;
                options.ApplicationName = "WmsServer";
            });
            //Configure<AbpAntiForgeryOptions>(options =>
            //{
                
            //    options.AutoValidate = false; //表示不验证防伪令牌
            //});
            //context.Services.UseRedis(configuration);
         
        }

        public override void OnApplicationInitialization(ApplicationInitializationContext context)
        {
            var app = context.GetApplicationBuilder();
            
            app.UseCorrelationId();
            app.UseStaticFiles();
            app.UseRouting();
            //跨域
            app.UseCors("CorsPolicy");
            app.UseAuthentication();
            app.UseAbpClaimsMap();

            if (BridgeConsts.IsMultiTenancyEnabled)
            {
                app.UseMultiTenancy();
            }

            app.UseAbpRequestLocalization(); //TODO: localization?
            //swagger页面访问授权
            app.UseSwaggerAuthorized();
            app.UseSwagger();
            app.UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint("/swagger/v1/swagger.json", "Wms Service API");
            });
            app.UseAuditing();
            app.UseConfiguredEndpoints();
        }
    }
}
