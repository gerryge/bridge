using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.Extensions.DependencyInjection;
using StackExchange.Redis;
using Microsoft.OpenApi.Models;
using Bridge.Shared;
using Bridge.Shared.Extensions;
using Volo.Abp;
using Volo.Abp.AspNetCore.MultiTenancy;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.Auditing;
using Volo.Abp.AuditLogging.EntityFrameworkCore;
using Volo.Abp.Autofac;
using Volo.Abp.Domain.Entities.Events.Distributed;
using Volo.Abp.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore.SqlServer;
using Volo.Abp.EventBus.RabbitMq;
using Volo.Abp.Identity;
using Volo.Abp.Identity.EntityFrameworkCore;
using Volo.Abp.Localization;
using Volo.Abp.Modularity;
using Volo.Abp.MultiTenancy;
using Volo.Abp.PermissionManagement.EntityFrameworkCore;
using Volo.Abp.Security.Claims;
using Volo.Abp.SettingManagement.EntityFrameworkCore;
using Volo.Abp.TenantManagement.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore.MySQL;
using Volo.Abp.AspNetCore.Mvc.AntiForgery;
using Bridge.Shared.Filter;
using Microsoft.AspNetCore.Mvc;

namespace Bridge.ScmService.Host
{
    [DependsOn(
        typeof(AbpAutofacModule),
        typeof(AbpAspNetCoreMvcModule),
         typeof(AbpEntityFrameworkCoreMySQLModule),
        typeof(AbpAuditLoggingEntityFrameworkCoreModule),
        typeof(AbpPermissionManagementEntityFrameworkCoreModule),
        typeof(AbpSettingManagementEntityFrameworkCoreModule),
        //typeof(AbpIdentityHttpApiModule),
        //typeof(AbpIdentityEntityFrameworkCoreModule),
        //typeof(AbpIdentityApplicationModule),
        typeof(AbpAspNetCoreMultiTenancyModule),
        typeof(AbpTenantManagementEntityFrameworkCoreModule),
        typeof(ScmApplicationModule),
        typeof(ScmEntityFrameworkCoreModule),
        typeof(ScmHttpApiModule)
        )]
    public class ScmServiceHostModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            var configuration = context.Services.GetConfiguration();

            context.Services.ConfigureCommonServices(new InitializerOptions());

            //Configure<AbpMultiTenancyOptions>(options =>
            //{
            //    options.IsEnabled = BridgeConsts.IsMultiTenancyEnabled;
            //});

            //context.Services.AddAuthentication("Bearer")
            //    .AddIdentityServerAuthentication(options =>
            //    {
            //        options.Authority = configuration["AuthServer:Authority"];
            //        options.ApiName = configuration["AuthServer:ApiName"];
            //        options.RequireHttpsMetadata = Convert.ToBoolean(configuration["AuthServer:RequireHttpsMetadata"]);
            //    });

            //context.Services.AddSwaggerGen(options =>
            //{
            //    options.SwaggerDoc("v1", new OpenApiInfo {Title = "Scm Service API", Version = "v1"});
            //    //启用Swagger中的【Authorize】按钮。这样就不用每个项目的AddSwaggerGen中单独配置了
            //    options.AddAuthenticationHeader();
            //});

            //Configure<AbpLocalizationOptions>(options =>
            //{
            //    options.Languages.Add(new LanguageInfo("en", "en", "English"));
            //});
            context.Services.AddMvc(options =>
            {
                //全局路由前缀配置
                options.UseCentralRoutePrefix(new RouteAttribute("api/v1/scm"));
                //全局授权配置
                options.Filters.Add<CoustomAuthorizeFilter>();

            });
            //Configure<AbpDbContextOptions>(options =>
            //{
            //    options.UseDbCon();
                
            //});

            Configure<AbpDistributedEntityEventOptions>(options =>
            {
                options.AutoEventSelectors.Add<IdentityUser>();
            });
            //跨域
            //context.Services.AddCors(options => options.AddPolicy("CorsPolicy",
            //builder =>
            //{
            //    builder.AllowAnyMethod()
            //         .SetIsOriginAllowed(_ => true)
            //         .AllowAnyHeader()
            //         .AllowCredentials();
            //}));
            //context.Services.AddStackExchangeRedisCache(options =>
            //{
            //    options.Configuration = configuration["Redis:Configuration"];
            //});

            Configure<AbpAuditingOptions>(options =>
            {
                options.IsEnabledForGetRequests = true;
                options.ApplicationName = "ScmServer";
            });
            //Configure<AbpAntiForgeryOptions>(options =>
            //{
            //    //options.TokenCookie.Expiration = TimeSpan.Zero;
            //    options.AutoValidate = false; //表示不验证防伪令牌
            //});
            //var redis = ConnectionMultiplexer.Connect(configuration["Redis:Configuration"]);
            //context.Services.AddDataProtection()
            //    .PersistKeysToStackExchangeRedis(redis, "ScmServer-DataProtection-Keys");
        }

        public override void OnApplicationInitialization(ApplicationInitializationContext context)
        {
            var app = context.GetApplicationBuilder();
            
            app.UseCorrelationId();
            app.UseStaticFiles();
            app.UseRouting();
            //跨域
            app.UseCors("CorsPolicy");
            app.UseAuthentication();
            app.UseAbpClaimsMap();

            if (BridgeConsts.IsMultiTenancyEnabled)
            {
                app.UseMultiTenancy();
            }

            app.UseAbpRequestLocalization(); //TODO: localization?
            //swagger页面访问授权
            app.UseSwaggerAuthorized();
            app.UseSwagger();
            app.UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint("/swagger/v1/swagger.json", "Scm Service API");
            });
            app.UseAuditing();
            app.UseConfiguredEndpoints();
        }
    }
}
