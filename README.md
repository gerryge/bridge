# 🍟 基于ABP-VNext构建的用户权限管理系统

## 🍁 前端技术

Vue 版前端技术栈 ：基于 vue3.x/uniapp、vuex、vue-router 、vue-cli 、axios、 element-ui、echats、i18n 国际化等，前端采用 vscode 工具开发

## 🍀 后端技术

- 核心框架：abp-vnext + .Net7.0  + EFCORE + signalR +  Quartz.net + Redis + mysql/达梦/sqlserver + minio
- 定时计划任务：Quartz.Net 组件，支持执行程序集或者 http 网络请求
- 安全支持：过滤器(数据权限过滤)、Sql 注入、请求伪造
- 日志管理：登录日志、操作日志、定时任务日志
- 缓存数据：内置内存缓存和 Redis
- signalR：使用 signalr 管理用户在线状态

## 🍖 内置功能

1. 用户管理：用户是系统操作者，该功能主要完成系统用户配置。
2. 部门管理：配置系统组织机构（公司、部门、小组），树结构展现。
3. 菜单管理：配置系统菜单，操作权限，按钮权限标识等。
4. 角色管理：角色菜单权限分配。
5. 字典管理：对系统中经常使用的一些较为固定的数据进行维护。
6. 操作日志：系统正常操作日志记录和查询；系统异常信息日志记录和查询。
7. 登录日志：系统登录日志记录查询包含登录异常。
8. Abp-VNext所有功能: 自带abp框架的所有功能
9. 任务系统：基于 Quartz.NET，可以在线（添加、修改、删除、手动执行）任务调度包含执行结果日志。
10. 微信关注：关注微信API已发开发
11. 工作流：规划中，正在开发。。。
12. 文件服务，minio存储文件

## 📈 系统启动流程
1、准备你自己的服务器，安装好mysql、redis服务

2、修改配置文件连接为你的服务器ip和密码：
mysql:"Default": "Server=123.249.14.35;Port=3306; Database=bridge; User=root; Password=xxxxx;"
redis: "Configuration": "123.249.14.35:6379,password=xxxxx,ConnectTimeout=15000,SyncTimeout=5000",

3、迁移数据库

4、前端启动：在vscode中打开文件，bridge\applications\web.vue3，并按正常的vue运行项目

5、后端启动：配置多项目启动,要启动的服务如下
Bridge.AuthServer.Host
Bridge.BackgroundJobServer.Host
Bridge.SystemServer.Host
Bridge.PublicWebSiteGateway.Host

5、演示用户：
admin   1q2w3E*

####  :star: 演示截图： 
 <table>
    <tr>
        <td><img src="applications/web.vue3/src/assets/images/1.png"/></td>
        <td><img src="applications/web.vue3/src/assets/images/2.png"/></td>
    </tr>
    <tr>
        <td><img src="applications/web.vue3/src/assets/images/3.png"/></td>
        <td><img src="applications/web.vue3/src/assets/images/4.png"/></td>
    </tr>
</table>

## 💐 特别鸣谢

- 👉Ruoyi.vue：[Ruoyi](http://www.ruoyi.vip/)
- 👉abp-vnext：[abp-vnext](https://aspnetboilerplate.com/)
- 👉abp-vnext中文文档：[abp-vnext中文文档](https://docs.abp.io/zh-Hans/abp/latest/API/Auto-API-Controllers)
- 👉vue-element-admin：[vue-element-admin](https://github.com/PanJiaChen/vue-element-admin)
- 👉ZRAdmin.NET：[ZRAdmin.NET](http://demo.izhaorui.cn/vue3/login?redirect=/vue3/login)

## 🎀 捐赠

如果你觉得这个项目帮助到了你，你可以请作者喝杯咖啡表示鼓励 ☕️
<img src="applications/web.vue3/src/assets/images/reward.jpg"/>
 
 


