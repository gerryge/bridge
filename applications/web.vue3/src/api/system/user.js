import request from '@/utils/request'
import { praseStrZero } from '@/utils/ruoyi'
import { downFile } from '@/utils/request'

// 查询用户列表
export function listUser(data) {
  return request({
    url: '/api/v1/sys/User/GetPageList',
    method: 'post',
    data: data
  })
}

// 查询用户详细
export function getUser(id) {
  return request({
    url: '/api/v1/sys/User/GetUserById/' + id,
    method: 'get'
  })
}

// 新增用户
export function addUser(data) {
  return request({
    url: '/api/v1/sys/User/CreateUser',
    method: 'post',
    data: data
  })
}

// 修改用户
export function updateUser(data) {
  return request({
    url: '/api/v1/sys/User/Update',
    method: 'post',
    data: data
  })
}

// 删除用户
export function delUser(data) {
  return request({
    url: '/api/v1/sys/User/Delete',
    method: 'post',
    data: data
  })
}

// 导出用户
export async function exportUser(query) {
  // return request({
  //   url: '/system/User/export',
  //   method: 'get',
  //   params: query
  // })
  await downFile('/system/user/export', { ...query })
}

// 用户密码重置
export function resetUserPwd(userId, password) {
  const data = {
    userId,
    password
  }
  return request({
    url: '/system/user/resetPwd',
    method: 'put',
    data: data
  })
}

// 用户状态修改
export function changeUserStatus(id, isActive) {
  const data = {
    id,
    isActive
  }
  return request({
    url: '/api/v1/sys/User/ChangeIsActive',
    method: 'post',
    data: data
  })
}

// 查询用户个人信息
export function getUserProfile() {
  return request({
    url: '/system/user/Profile',
    method: 'get'
  })
}

// 修改用户个人信息
export function updateUserProfile(data) {
  return request({
    url: '/system/user/profile',
    method: 'put',
    data: data
  })
}

// 用户密码重置
export function updateUserPwd(oldPassword, newPassword) {
  const data = {
    oldPassword,
    newPassword
  }
  return request({
    url: '/system/user/profile/updatePwd',
    method: 'put',
    params: data
  })
}

// 用户头像上传
export function uploadAvatar(data) {
  return request({
    url: '/system/user/profile/avatar',
    method: 'post',
    data: data,
    headers: { 'Content-Type': 'multipart/form-data' }
  })
}

// 下载用户导入模板
export function importTemplate() {
  return request({
    url: '/system/user/importTemplate',
    method: 'get',
    responseType: 'blob' //1.首先设置responseType对象格式为 blob:
  })
}
