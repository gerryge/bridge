import request from '@/utils/request'
import { downFile } from '@/utils/request'

// 查询角色列表
export function listRole(data) {
  return request({
    url: '/api/v1/sys/role/GetRoleList',
    method: 'post',
    data: data
  })
}

// 查询角色详细
export function getRole(roleId) {
  return request({
    url: '/api/v1/sys/role/GetRoleInfo/' + roleId,
    method: 'get'
  })
}

// 新增角色
export const addRole = (data) => {
  return request({
    url: '/api/v1/sys/role/CreateRole',
    method: 'post',
    data: data
  })
}

// 修改角色
export function updateRole(data) {
  return request({
    url: '/api/v1/sys/role/CreateRole',
    method: 'post',
    data: data
  })
}

// 角色数据权限
export function dataScope(data) {
  return request({
    url: '/api/v1/sys/role/AddMenuForRolse',
    method: 'post',
    data: data
  })
}

// 角色状态修改
export function changeRoleStatus(roleId, status) {
  const data = {
    roleId,
    status
  }
  return request({
    url: '/system/role/changeStatus',
    method: 'put',
    data: data
  })
}

// 删除角色
export function delRole(data) {
  return request({
    url: '/api/v1/sys/role/delete',
    method: 'post',
    data: data
  })
}

// 导出角色
export function exportRole(query) {
  return request({
    url: '/system/role/export',
    method: 'get',
    params: query
  })
}
// 导出角色菜单
export async function exportRoleMenu(query) {
  await downFile('/system/role/exportRoleMenu', { ...query })
}
