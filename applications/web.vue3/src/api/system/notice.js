import request from '@/utils/request'

// 导航栏查询公告列表
export function queryNotice(query) {
  return request({
    url: '/system/notice/queryNotice',
    method: 'get',
    params: query
  })
}

// 查询公告列表
export function listNotice(data) {
  return request({
    url: '/api/v1/sys/Notice/GetPageList',
    method: 'post',
    data: data
  })
}

// 查询公告详细
export function getNotice(noticeId) {
  return request({
    url: '/api/v1/sys/Notice/Get/' + noticeId,
    method: 'get'
  })
}

// 新增公告
export function addNotice(data) {
  return request({
    url: '/api/v1/sys/Notice/Create',
    method: 'post',
    data: data
  })
}

// 修改公告
export function updateNotice(data) {
  return request({
    url: '/api/v1/sys/Notice/Update',
    method: 'post',
    data: data
  })
}

// 删除公告
export function delNotice(data) {
  return request({
    url: '/api/v1/sys/Notice/Delete',
    method: 'post',
    data: data
  })
}

// 发送通知公告
export function sendNotice(noticeId) {
  return request({
    url: '/api/v1/sys/Notice/SendNotice/' + noticeId,
    method: 'PUT'
  })
}