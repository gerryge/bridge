import Entity from "./entity"

export default class Operate extends Entity<number>{

    operateName: string;
    operateEvent: string;
    businessStatus: string;
    pageUrl:string;
    public constructor(operateName?: string,operateEvent?:string, businessStatus?:string, pageUrl?:string) {
        super();
        this.operateName = operateName;
        this.operateEvent = operateEvent;
        this.businessStatus = businessStatus;
        this.pageUrl=pageUrl;
    }
}




