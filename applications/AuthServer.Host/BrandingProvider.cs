﻿using Volo.Abp.DependencyInjection;
using Volo.Abp.Ui.Branding;

namespace Bridge.AuthServer.Host
{
    public class BrandingProvider : DefaultBrandingProvider, ISingletonDependency
    {
        public override string AppName => "Authentication Server";
    }
}
