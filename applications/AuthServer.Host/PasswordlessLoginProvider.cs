﻿using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace Bridge.AuthServer.Host
{
    public class PasswordlessLoginProvider<TUser> : TotpSecurityStampBasedTokenProvider<TUser> //PhoneNumberTokenProvider
         where TUser : class
    {
        public override Task<bool> CanGenerateTwoFactorTokenAsync(UserManager<TUser> manager, TUser user)
        {
            return Task.FromResult(false);
        }
    }
}
