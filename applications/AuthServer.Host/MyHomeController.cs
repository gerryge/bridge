﻿using IdentityModel.Client;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.Identity;

namespace Bridge.AuthServer.Host
{
    [Route("MyHome")]
    public class MyHomeController : AbpController
    {
        protected IdentityUserManager UserManager { get; }

        private readonly IIdentityUserRepository _userRepository;
        public MyHomeController(IdentityUserManager userManager, IIdentityUserRepository userRepository) {
            UserManager = userManager;
            _userRepository = userRepository;
        }
        [HttpGet]
        [Route("generatePasswordlessTokenAsync")]
        public async Task<string> GeneratePasswordlessTokenAsync()
        {
            var adminUser = await _userRepository.FindByNormalizedUserNameAsync("admin");
            var token = await UserManager.GenerateUserTokenAsync(adminUser, "PasswordlessLoginProvider",
                "passwordless-auth");
            return token;





        }

    }
}
