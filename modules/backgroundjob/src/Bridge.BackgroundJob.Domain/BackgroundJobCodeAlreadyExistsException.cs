﻿using Volo.Abp;


public class BackgroundJobCodeAlreadyExistsException : BusinessException
{
    public BackgroundJobCodeAlreadyExistsException(string productCode)
        : base("PM:000001", $"A product with code {productCode} has already exists!")
    {

    }
}
