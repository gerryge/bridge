﻿using Bridge.BackgroundJob;
using Bridge.BackgroundJob.Localization;
using Volo.Abp.EntityFrameworkCore;
using Volo.Abp.Localization;
using Volo.Abp.Localization.ExceptionHandling;
using Volo.Abp.Modularity;
using Volo.Abp.VirtualFileSystem;


/* This module directly depends on EF Core by its design.
 * In this way, we can directly use EF Core async LINQ extension methods.
 */
[DependsOn(
    typeof(BackgroundJobDomainSharedModule),
    typeof(AbpEntityFrameworkCoreModule)
    )]
public class BackgroundJobDomainModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        Configure<AbpVirtualFileSystemOptions>(options =>
        {
            options.FileSets.AddEmbedded<BackgroundJobDomainModule>();
        });

        Configure<AbpLocalizationOptions>(options =>
        {
            options.Resources.Get<BackgroundJobResource>().AddVirtualJson("/Bridge.BackgroundJob/Localization/Domain");
        });

        Configure<AbpExceptionLocalizationOptions>(options =>
        {
            options.MapCodeNamespace("Bridge.BackgroundJob", typeof(BackgroundJobResource));
        });
    }
}

