﻿using Volo.Abp.AutoMapper;
using Volo.Abp.Modularity;


[DependsOn(
    typeof(BackgroundJobDomainModule),
    typeof(BackgroundJobApplicationContractsModule),
    typeof(AbpAutoMapperModule)
    )]
public class BackgroundJobApplicationModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        Configure<AbpAutoMapperOptions>(options =>
        {
            options.AddProfile<BackgroundJobApplicationAutoMapperProfile>(validate: true);
        });

    }
}

