﻿using Bridge.Quartz.MUI.BaseService;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridge.BackgroundJob.Application
{
    public class TestJob : IJobService
    {
        private ILogger<TestJob> _logger { get; set; }

        public TestJob(ILogger<TestJob> logger) {  _logger = logger; }

        public string ExecuteService(string parameter)
        {
            try
            {
                _logger.LogError("定时任务开启！");
                return string.Empty;
            }
            catch (Exception ex)
            {
                _logger.LogError($"测试定时任务异常，原因：{ex.Message}");
                return ex.Message;
            }
        }
    }
}
