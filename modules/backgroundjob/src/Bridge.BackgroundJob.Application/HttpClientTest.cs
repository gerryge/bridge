﻿using Bridge.BackgroundJob.Application.Contracts;
using IdentityModel.Client;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Users;
using Volo.Abp;
using Volo.Abp.Application.Services;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Users;

namespace Bridge.BackgroundJob.Application
{
    public class HttpClientTest: ApplicationService,IHttpClientTest
    {
        private readonly IUserService _userService;
        public HttpClientTest(IUserService userService)
        {
            _userService = userService;
        }
        public HttpClientTest() { }

        public async Task Test()
        {
            var userInfo = await _userService.GetInfo();
            var currentUser = CurrentUser;
            return;
        }
    }
}
