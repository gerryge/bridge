﻿using Volo.Abp.Localization;

namespace Bridge.BackgroundJob.Localization
{
    [LocalizationResourceName("BackgroundJob")]
    public class BackgroundJobResource
    {
        
    }
}
