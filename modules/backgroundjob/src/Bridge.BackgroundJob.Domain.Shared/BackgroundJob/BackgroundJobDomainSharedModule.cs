﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Modularity;
using Volo.Abp.Localization;
using Bridge.BackgroundJob.Localization;
namespace Bridge.BackgroundJob
{
    [DependsOn(
        typeof(AbpLocalizationModule)
        )]
    public class BackgroundJobDomainSharedModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            Configure<AbpLocalizationOptions>(options =>
            {
                options.Resources.Add<BackgroundJobResource>("en");
            });
        }
    }
}
