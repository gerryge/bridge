﻿using JetBrains.Annotations;
using Volo.Abp.EntityFrameworkCore.Modeling;


public class BackgroundJobModelBuilderConfigurationOptions : AbpModelBuilderConfigurationOptions
{
    public BackgroundJobModelBuilderConfigurationOptions(
        [NotNull] string tablePrefix = BackgroundJobConsts.DefaultDbTablePrefix,
        [CanBeNull] string schema = BackgroundJobConsts.DefaultDbSchema)
        : base(
            tablePrefix,
            schema)
    {

    }
}
