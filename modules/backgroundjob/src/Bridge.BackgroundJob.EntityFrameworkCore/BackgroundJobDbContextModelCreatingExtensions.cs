﻿using System;
using Microsoft.EntityFrameworkCore;
using Volo.Abp;
using Volo.Abp.EntityFrameworkCore.Modeling;


public static class BackgroundJobDbContextModelCreatingExtensions
{
    public static void ConfigureBackgroundJob(
        this ModelBuilder builder,
        Action<BackgroundJobModelBuilderConfigurationOptions> optionsAction = null)
    {
        Check.NotNull(builder, nameof(builder));

        var options = new BackgroundJobModelBuilderConfigurationOptions();

        optionsAction?.Invoke(options);
     
        //builder.Entity<Product>(b =>
        //{
        //    b.ToTable(options.TablePrefix + "Products", options.Schema);

        //    b.ConfigureConcurrencyStamp();
        //    b.ConfigureExtraProperties();
        //    b.ConfigureAudited();

        //    b.Property(x => x.Code).IsRequired().HasMaxLength(ProductConsts.MaxCodeLength);
        //    b.Property(x => x.Name).IsRequired().HasMaxLength(ProductConsts.MaxNameLength);
        //    b.Property(x => x.ImageName).HasMaxLength(ProductConsts.MaxImageNameLength);

        //    b.HasIndex(q => q.Code);
        //    b.HasIndex(q => q.Name);
        //}
        //);
    }
}
