﻿using Microsoft.EntityFrameworkCore;
using Volo.Abp.Data;
using Volo.Abp.EntityFrameworkCore;


[ConnectionStringName("BackgroundJob")]
public class BackgroundJobDbContext : AbpDbContext<BackgroundJobDbContext>, IBackgroundJobDbContext
{
    public static string TablePrefix { get; set; } = BackgroundJobConsts.DefaultDbTablePrefix;

    public static string Schema { get; set; } = BackgroundJobConsts.DefaultDbSchema;

    //public DbSet<Product> Products { get; set; }

    public BackgroundJobDbContext(DbContextOptions<BackgroundJobDbContext> options)
        : base(options)
    {

    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);

        builder.ConfigureBackgroundJob(options =>
        {
            options.TablePrefix = TablePrefix;
            options.Schema = Schema;
        });
    }
}
