﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.EntityFrameworkCore;
using Volo.Abp.Modularity;


[DependsOn(
    typeof(BackgroundJobDomainModule),
    typeof(AbpEntityFrameworkCoreModule)
)]
public class BackgroundJobEntityFrameworkCoreModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        context.Services.AddAbpDbContext<BackgroundJobDbContext>(options =>
        {
            options.AddDefaultRepositories();
        });
    }
}
