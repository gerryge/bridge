﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bridge.Quartz.MUI.BaseService
{
    public interface IJobService
    {
        string ExecuteService( string parameter);
    }
}
