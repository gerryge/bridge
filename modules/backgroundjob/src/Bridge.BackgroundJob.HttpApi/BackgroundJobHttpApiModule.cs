﻿using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.Modularity;
using Microsoft.Extensions.DependencyInjection;
using Bridge.BackgroundJob;

[DependsOn(
    typeof(BackgroundJobApplicationContractsModule),
    typeof(BackgroundJobHttpApiClientModule),
    typeof(AbpAspNetCoreMvcModule))]
public class BackgroundJobHttpApiModule : AbpModule
{
    public override void PreConfigureServices(ServiceConfigurationContext context)
    {
        PreConfigure<IMvcBuilder>(mvcBuilder =>
        {
            mvcBuilder.AddApplicationPartIfNotExists(typeof(BackgroundJobHttpApiModule).Assembly);
        });
    }

   
}

