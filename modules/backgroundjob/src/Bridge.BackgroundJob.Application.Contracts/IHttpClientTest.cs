﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace Bridge.BackgroundJob.Application.Contracts
{
    public interface IHttpClientTest: IApplicationService
    {
        Task Test();
    }
}
