﻿using Bridge.BackgroundJob;
using Bridge.BackgroundJob.Localization;
using Volo.Abp.Application;
using Volo.Abp.Authorization.Permissions;
using Volo.Abp.Localization;
using Volo.Abp.Modularity;
using Volo.Abp.Settings;
using Volo.Abp.VirtualFileSystem;


[DependsOn(
    typeof(BackgroundJobDomainSharedModule),
    typeof(AbpDddApplicationModule)
    )]
public class BackgroundJobApplicationContractsModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        Configure<AbpVirtualFileSystemOptions>(options =>
        {
            options.FileSets.AddEmbedded<BackgroundJobApplicationContractsModule>();
        });

        Configure<AbpLocalizationOptions>(options =>
        {
            options.Resources
                .Get<BackgroundJobResource>()
                .AddVirtualJson("/BackgroundJob/Localization/ApplicationContracts");
        });
    }
}

