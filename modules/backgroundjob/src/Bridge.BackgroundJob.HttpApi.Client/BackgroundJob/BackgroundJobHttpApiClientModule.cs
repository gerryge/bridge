﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Http.Client;
using Volo.Abp.Modularity;

namespace Bridge.BackgroundJob
{
    [DependsOn(
       typeof(SystemApplicationContractsModule),
        typeof(BackgroundJobApplicationContractsModule),
        typeof(AbpHttpClientModule))]
    public class BackgroundJobHttpApiClientModule : AbpModule
    {
        public const string BackgroundJobRemoteServiceName = "BackgroundJobService";
        public const string IdentityRemoteServiceName = "IdentityService";

        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddHttpClientProxies(
                typeof(BackgroundJobApplicationContractsModule).Assembly,
                BackgroundJobRemoteServiceName
            );
            context.Services.AddHttpClientProxies(
               typeof(SystemApplicationContractsModule).Assembly,
               IdentityRemoteServiceName
           );
        }
    }
}
