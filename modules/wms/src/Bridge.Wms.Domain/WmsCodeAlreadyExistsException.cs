﻿using Volo.Abp;


public class WmsCodeAlreadyExistsException : BusinessException
{
    public WmsCodeAlreadyExistsException(string productCode)
        : base("PM:000001", $"A product with code {productCode} has already exists!")
    {

    }
}
