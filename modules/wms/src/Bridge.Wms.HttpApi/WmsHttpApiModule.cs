﻿using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.Modularity;
using Microsoft.Extensions.DependencyInjection;

[DependsOn(
    typeof(WmsApplicationContractsModule),
    typeof(AbpAspNetCoreMvcModule))]
public class WmsHttpApiModule : AbpModule
{
    public override void PreConfigureServices(ServiceConfigurationContext context)
    {
        PreConfigure<IMvcBuilder>(mvcBuilder =>
        {
            mvcBuilder.AddApplicationPartIfNotExists(typeof(WmsHttpApiModule).Assembly);
        });
    }

   
}

