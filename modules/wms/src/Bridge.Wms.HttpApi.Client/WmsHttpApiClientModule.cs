﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Http.Client;
using Volo.Abp.Modularity;


[DependsOn(
    typeof(WmsApplicationContractsModule),
    typeof(AbpHttpClientModule))]
public class WmsHttpApiClientModule : AbpModule
{
    public const string RemoteServiceName = "Wms";

    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        context.Services.AddHttpClientProxies(
            typeof(WmsApplicationContractsModule).Assembly,
            RemoteServiceName
        );
    }
}

