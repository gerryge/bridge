﻿
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Data;
using Volo.Abp.EntityFrameworkCore;


[ConnectionStringName("Default")]
public interface IWmsDbContext : IEfCoreDbContext
{

}
