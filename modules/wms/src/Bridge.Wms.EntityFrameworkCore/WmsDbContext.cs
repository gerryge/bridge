﻿using Microsoft.EntityFrameworkCore;
using Volo.Abp.Data;
using Volo.Abp.EntityFrameworkCore;


[ConnectionStringName("System")]
public class WmsDbContext : AbpDbContext<WmsDbContext>, IWmsDbContext
{
    public static string TablePrefix { get; set; } = WmsConsts.DefaultDbTablePrefix;

    public static string Schema { get; set; } = WmsConsts.DefaultDbSchema;



    public WmsDbContext(DbContextOptions<WmsDbContext> options)
        : base(options)
    {

    }
    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);

        builder.ConfigureSystem(options =>
        {
            options.TablePrefix = TablePrefix;
            options.Schema = Schema;
        });
    }
}
