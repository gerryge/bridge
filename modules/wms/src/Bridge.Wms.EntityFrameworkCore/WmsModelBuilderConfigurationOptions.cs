﻿using JetBrains.Annotations;
using Volo.Abp.EntityFrameworkCore.Modeling;


public class WmsModelBuilderConfigurationOptions : AbpModelBuilderConfigurationOptions
{
    public WmsModelBuilderConfigurationOptions(
        [NotNull] string tablePrefix = WmsConsts.DefaultDbTablePrefix,
        [CanBeNull] string schema = WmsConsts.DefaultDbSchema)
        : base(
            tablePrefix,
            schema)
    {

    }
}
