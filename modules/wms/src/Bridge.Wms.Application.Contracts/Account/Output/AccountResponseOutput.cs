﻿using Bridge.Shared.Enums.IdentityService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Account.Output
{
    public class AccountResponseOutput
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="result"></param>
        public AccountResponseOutput(LoginResultTypeEnum result)
        {
            this.Result = result;
        }

        public LoginResultTypeEnum Result { get; set; }

        public string Description { get; set; }
    }
}
