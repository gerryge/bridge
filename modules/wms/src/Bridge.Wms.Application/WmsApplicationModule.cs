﻿using Volo.Abp.AutoMapper;
using Volo.Abp.Modularity;


[DependsOn(
    typeof(WmsDomainModule),
    typeof(WmsApplicationContractsModule),
    typeof(AbpAutoMapperModule)
    )]
public class WmsApplicationModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        Configure<AbpAutoMapperOptions>(options =>
        {
            options.AddProfile<WmsApplicationAutoMapperProfile>(validate: true);
        });
    }
}

