﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Users;

namespace Bridge.Workflow.Extensions
{
    public static class StringExtensions
    {
        /// <summary>
        /// 获取用户所属机构名称
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static Guid GetAgencyId(this ICurrentUser user)
        {
            var str = user.FindClaim("AgencyId")?.Value;
            if (str.IsNullOrEmpty()) return Guid.Empty;

            return Guid.Parse(str);
        }
    }
}
