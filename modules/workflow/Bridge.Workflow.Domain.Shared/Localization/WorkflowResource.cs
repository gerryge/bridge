﻿using Volo.Abp.Localization;

namespace Bridge.Workflow.Localization
{
    [LocalizationResourceName("Workflow")]
    public class WorkflowResource
    {
        
    }
}
