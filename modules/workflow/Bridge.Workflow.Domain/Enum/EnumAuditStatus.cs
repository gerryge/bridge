﻿using System.ComponentModel;

namespace Bridge.Workflow
{
    public enum EnumAuditStatus
    {
        /// <summary>
        /// 待审核
        /// </summary>
        [Description("待审核")]
        UnAudited = 0,

        /// <summary>
        /// 审核通过
        /// </summary>
        [Description("审核通过")]
        Pass = 1,

        /// <summary>
        /// 审核未通过
        /// </summary>
        [Description("审核未通过")]
        Unapprove = 2,

        /// <summary>
        /// 驳回
        /// </summary>
        [Description("驳回")]
        Refuse = 3
    }
}
