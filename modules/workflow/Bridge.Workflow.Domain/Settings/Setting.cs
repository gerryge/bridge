﻿using System;
namespace Bridge.Workflow.Settings
{
    public class Setting
    {
        public string Authority { get; set; }
        public bool RequireHttpsMetadata { get; set; }
        public string ApiName { get; set; }
    }
}
