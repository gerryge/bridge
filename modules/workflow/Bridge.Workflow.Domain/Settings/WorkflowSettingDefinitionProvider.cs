﻿using Volo.Abp.Settings;

namespace Bridge.Workflow.Settings
{
    public class WorkflowSettingDefinitionProvider : SettingDefinitionProvider
    {
        public override void Define(ISettingDefinitionContext context)
        {
            /* Define module settings here.
             * Use names from WorkflowSettings class.
             */
        }
    }
}