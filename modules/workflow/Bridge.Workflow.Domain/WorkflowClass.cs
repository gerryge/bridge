﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp;
using Volo.Abp.Domain.Entities.Auditing;
using Volo.Abp.MultiTenancy;

namespace Bridge.Workflow
{
    public class WorkflowClass : AuditedAggregateRoot<Guid>, ISoftDelete
    {
        public string Name { get; set; } 
        public string Code { get; set; }
        public string Principal { get; set; }
        public int Status { get; set; }
        public bool IsDeleted { get; set; } = false;
        public string Remark { get; set; }
        public WorkflowClass(Guid keyId)
        {
            this.Id = keyId;
        }

        public WorkflowClass() { }
    }
}
