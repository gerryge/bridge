﻿using System;
using Bridge.Workflow.Domain;
using Newtonsoft.Json;
using WorkflowCore.Models;
using System.Linq;
using System.Collections.Generic;

namespace Bridge.Workflow.Extension
{
    /// <summary>
    /// 
    /// </summary>
    public static class ExtensionMethods
    {
        /// <summary>
        /// 
        /// </summary>
        private static JsonSerializerSettings SerializerSettings = new JsonSerializerSettings()
        {
            TypeNameHandling = TypeNameHandling.All
        };

        /// <summary>
        /// 
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static Guid ConvertToGuid(this string str)
        {
            if (string.IsNullOrEmpty(str)) return Guid.Empty;

            if (!Guid.TryParse(str, out Guid guid)) return Guid.Empty;

            return guid;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="instance"></param>
        /// <param name="persistable"></param>
        /// <returns></returns>
        public static PersistedWorkflow ToPersistable(this WorkflowInstance instance, PersistedWorkflow persistable = null)
        {
            if (persistable == null)
                persistable = new PersistedWorkflow(instance.Id);
            //persistable.Id = new Guid(instance.Id);
            persistable.Data = JsonConvert.SerializeObject(instance.Data, SerializerSettings);
            persistable.Description = instance.Description;
            persistable.Reference = instance.Reference;
            persistable.NextExecution = instance.NextExecution;
            persistable.Version = instance.Version;
            persistable.WorkflowDefinitionId = instance.WorkflowDefinitionId;
            persistable.Status = instance.Status;
            persistable.CreationTime = instance.CreateTime;
            persistable.CompleteTime = instance.CompleteTime;

            foreach (var ep in instance.ExecutionPointers)
            {
                var persistedEP = persistable.ExecutionPointers.FindById(ep.Id);

                if (persistedEP == null)
                {
                    persistedEP = new PersistedExecutionPointer(ep.Id);
                    //persistedEP.Id = .ToString();
                    persistable.ExecutionPointers.Add(persistedEP);
                }

                persistedEP.StepId = ep.StepId;
                persistedEP.Active = ep.Active;
                persistedEP.SleepUntil = ep.SleepUntil;
                persistedEP.PersistenceData = JsonConvert.SerializeObject(ep.PersistenceData, SerializerSettings);
                persistedEP.StartTime = ep.StartTime;
                persistedEP.EndTime = ep.EndTime;
                persistedEP.StepName = ep.StepName;
                persistedEP.RetryCount = ep.RetryCount;
                persistedEP.PredecessorId = ep.PredecessorId;
                persistedEP.ContextItem = JsonConvert.SerializeObject(ep.ContextItem, SerializerSettings);
                persistedEP.Children = string.Empty;

                foreach (var child in ep.Children)
                    persistedEP.Children += child + ";";

                persistedEP.EventName = ep.EventName;
                persistedEP.EventKey = ep.EventKey;
                persistedEP.EventPublished = ep.EventPublished;
                persistedEP.EventData = JsonConvert.SerializeObject(ep.EventData, SerializerSettings);
                persistedEP.Outcome = JsonConvert.SerializeObject(ep.Outcome, SerializerSettings);
                persistedEP.Status = ep.Status;

                persistedEP.Scope = string.Empty;
                foreach (var item in ep.Scope)
                    persistedEP.Scope += item + ";";

                foreach (var attr in ep.ExtensionAttributes)
                {
                    var persistedAttr = persistedEP.ExtensionAttributes.FirstOrDefault(x => x.AttributeKey == attr.Key);
                    if (persistedAttr == null)
                    {
                        persistedAttr = new PersistedExtensionAttribute();
                        persistedEP.ExtensionAttributes.Add(persistedAttr);
                    }

                    persistedAttr.AttributeKey = attr.Key;
                    persistedAttr.AttributeValue = JsonConvert.SerializeObject(attr.Value, SerializerSettings);
                }
            }

            return persistable;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        public static PersistedExecutionError ToPersistable(this ExecutionError instance)
        {
            return new PersistedExecutionError
            {
                ErrorTime = instance.ErrorTime,
                Message = instance.Message,
                ExecutionPointerId = instance.ExecutionPointerId,
                WorkflowId = instance.WorkflowId
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        public static PersistedSubscription ToPersistable(this EventSubscription instance)
        {
            return new PersistedSubscription(new Guid(instance.Id))
            {
                EventKey = instance.EventKey,
                EventName = instance.EventName,
                StepId = instance.StepId,
                ExecutionPointerId = instance.ExecutionPointerId,
                WorkflowId = instance.WorkflowId,
                SubscribeAsOf = DateTime.SpecifyKind(instance.SubscribeAsOf, DateTimeKind.Utc),
                SubscriptionData = JsonConvert.SerializeObject(instance.SubscriptionData, SerializerSettings),
                ExternalToken = instance.ExternalToken,
                ExternalTokenExpiry = instance.ExternalTokenExpiry,
                ExternalWorkerId = instance.ExternalWorkerId
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        public static PersistedEvent ToPersistable(this Event instance)
        {
            return new PersistedEvent(new Guid(instance.Id))
            {
                EventKey = instance.EventKey,
                EventName = instance.EventName,
                EventTime = DateTime.SpecifyKind(instance.EventTime, DateTimeKind.Utc),
                IsProcessed = instance.IsProcessed,
                EventData = JsonConvert.SerializeObject(instance.EventData, SerializerSettings)
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        public static WorkflowInstance ToWorkflowInstance(this PersistedWorkflow instance)
        {
            if (instance == null) return null;

            WorkflowInstance result = new WorkflowInstance
            {
                Data = JsonConvert.DeserializeObject(instance.Data, SerializerSettings),
                Description = instance.Description,
                Reference = instance.Reference,
                Id = instance.Id,
                NextExecution = instance.NextExecution,
                Version = instance.Version,
                WorkflowDefinitionId = instance.WorkflowDefinitionId,
                Status = instance.Status,
                CreateTime = DateTime.SpecifyKind(instance.CreationTime, DateTimeKind.Utc)
            };
            if (instance.CompleteTime.HasValue)
                result.CompleteTime = DateTime.SpecifyKind(instance.CompleteTime.Value, DateTimeKind.Utc);

            result.ExecutionPointers = new ExecutionPointerCollection(instance.ExecutionPointers.Count + 8);

            foreach (var ep in instance.ExecutionPointers)
            {
                var pointer = new ExecutionPointer
                {
                    Id = ep.Id,
                    StepId = ep.StepId,
                    Active = ep.Active
                };

                if (ep.SleepUntil.HasValue)
                    pointer.SleepUntil = DateTime.SpecifyKind(ep.SleepUntil.Value, DateTimeKind.Utc);

                pointer.PersistenceData = JsonConvert.DeserializeObject(ep.PersistenceData ?? string.Empty, SerializerSettings);

                if (ep.StartTime.HasValue)
                    pointer.StartTime = DateTime.SpecifyKind(ep.StartTime.Value, DateTimeKind.Utc);

                if (ep.EndTime.HasValue)
                    pointer.EndTime = DateTime.SpecifyKind(ep.EndTime.Value, DateTimeKind.Utc);

                pointer.StepName = ep.StepName;

                pointer.RetryCount = ep.RetryCount;
                pointer.PredecessorId = ep.PredecessorId;
                pointer.ContextItem = JsonConvert.DeserializeObject(ep.ContextItem ?? string.Empty, SerializerSettings);

                if (!string.IsNullOrEmpty(ep.Children))
                    pointer.Children = ep.Children.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries).ToList();

                pointer.EventName = ep.EventName;
                pointer.EventKey = ep.EventKey;
                pointer.EventPublished = ep.EventPublished;
                pointer.EventData = JsonConvert.DeserializeObject(ep.EventData ?? string.Empty, SerializerSettings);
                pointer.Outcome = JsonConvert.DeserializeObject(ep.Outcome ?? string.Empty, SerializerSettings);
                pointer.Status = ep.Status;

                if (!string.IsNullOrEmpty(ep.Scope))
                    pointer.Scope = new List<string>(ep.Scope.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries));

                foreach (var attr in ep.ExtensionAttributes)
                {
                    pointer.ExtensionAttributes[attr.AttributeKey] = JsonConvert.DeserializeObject(attr.AttributeValue, SerializerSettings);
                }

                result.ExecutionPointers.Add(pointer);
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        public static EventSubscription ToEventSubscription(this PersistedSubscription instance)
        {
            if (instance == null) return null;

            return new EventSubscription
            {
                Id = instance.Id.ToString(),
                EventKey = instance.EventKey,
                EventName = instance.EventName,
                StepId = instance.StepId,
                ExecutionPointerId = instance.ExecutionPointerId,
                WorkflowId = instance.WorkflowId,
                SubscribeAsOf = DateTime.SpecifyKind(instance.SubscribeAsOf, DateTimeKind.Utc),
                SubscriptionData = JsonConvert.DeserializeObject(instance.SubscriptionData, SerializerSettings),
                ExternalToken = instance.ExternalToken,
                ExternalTokenExpiry = instance.ExternalTokenExpiry,
                ExternalWorkerId = instance.ExternalWorkerId
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        public static Event ToEvent(this PersistedEvent instance)
        {
            if (instance == null) return null;

            return new Event
            {
                Id = instance.Id.ToString(),
                EventKey = instance.EventKey,
                EventName = instance.EventName,
                EventTime = DateTime.SpecifyKind(instance.EventTime, DateTimeKind.Utc),
                IsProcessed = instance.IsProcessed,
                EventData = JsonConvert.DeserializeObject(instance.EventData, SerializerSettings)
            };
        }
    }
}
