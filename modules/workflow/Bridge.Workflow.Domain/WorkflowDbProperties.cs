﻿namespace Bridge.Workflow
{
    public static class WorkflowDbProperties
    {
        public static string DbTablePrefix { get; set; } = "Workflow";

        public static string DbSchema { get; set; } = null;

        public const string ConnectionStringName = "Default";

        public const string DefaultPassPhrase = "gsKxGZ012HLL3MI5";
    }
}
