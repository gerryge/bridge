﻿using System;

namespace Bridge.Workflow.Domain.Workflows
{
    /// <summary>
    /// 
    /// </summary>
    public class AbpWorkflowStepBody
    {
        public string Name { get; set; }
        public string DisplayName { get; set; }

        public WorkflowParamDictionary Inputs { get; set; } = new WorkflowParamDictionary();

        public Type StepBodyType { get; set; }
    }

    public class AbpWorkflowStepBody_two
    {
        public string Name { get; set; }
        public string DisplayName { get; set; }

        public WorkflowParamDictionary Inputs { get; set; } = new WorkflowParamDictionary();

    }
}
