﻿using System;
using System.Collections.Generic;

namespace Bridge.Workflow.Domain.Workflows
{
    /// <summary>
    /// 
    /// </summary>
    public class WorkflowParamDictionary : Dictionary<string, WorkflowParam>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        public void Add(WorkflowParam param)
        {
            if (this.ContainsKey(param.Name))
            {
                throw new Exception($"'{param.Name}' has Contain!");
            }

            this[param.Name] = param;
        }
    }
}
