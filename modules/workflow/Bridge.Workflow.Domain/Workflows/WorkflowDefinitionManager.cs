﻿using System;
using Volo.Abp.DependencyInjection;

namespace Bridge.Workflow.Domain.Workflows
{
    /// <summary>
    /// 
    /// </summary>
    public class WorkflowDefinitionManager : AbpStepBodyDefinitionContextBase, ISingletonDependency
    {
        private readonly IAbpStepBodyConfiguration _baseCodeTypeConfiguration;
        //private readonly IIocManager _iocManager;

        public WorkflowDefinitionManager(IAbpStepBodyConfiguration baseCodeTypeConfiguration)
        {
            _baseCodeTypeConfiguration = baseCodeTypeConfiguration;
        }

        /// <summary>
        /// 初始化
        /// </summary>
        internal void Initialize(IServiceProvider service)
        {
            foreach (var providerType in _baseCodeTypeConfiguration.Providers)
            {
                var provider = service.GetService(providerType) as AbpStepBodyProvider;
                if (provider == null) continue;
                provider.Build(this);
            }
        }
    }
}
