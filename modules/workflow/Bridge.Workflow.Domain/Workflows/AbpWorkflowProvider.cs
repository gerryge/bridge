﻿using Volo.Abp.DependencyInjection;
using WorkflowCore.Interface;

namespace Bridge.Workflow.Domain
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class AbpWorkflowProvider : ITransientDependency
    {
        /// <summary>
        /// 设置码表类型
        /// </summary>
        /// <param name="context"></param>
        public abstract void Builds(IWorkflowBuilder context);
    }
}
