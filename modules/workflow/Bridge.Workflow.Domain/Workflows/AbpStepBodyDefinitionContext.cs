﻿using System.Collections.Generic;
using Volo.Abp;

namespace Bridge.Workflow.Domain.Workflows
{
    /// <summary>
    /// 
    /// </summary>
    public interface IAbpStepBodyDefinitionContext
    {
        void Create(AbpWorkflowStepBody entity);
    }

    /// <summary>
    /// 
    /// </summary>
    public abstract class AbpStepBodyDefinitionContextBase : IAbpStepBodyDefinitionContext
    {
        protected readonly Dictionary<string, AbpWorkflowStepBody> AbpStepBodys;
        public AbpStepBodyDefinitionContextBase()
        {

            AbpStepBodys = new Dictionary<string, AbpWorkflowStepBody>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        public void Create(AbpWorkflowStepBody entity)
        {
            if (AbpStepBodys.ContainsKey(entity.Name))
            {
                throw new AbpException("There is already a AbpStepBody with name: " + entity.Name);
            }

            AbpStepBodys[entity.Name] = entity;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IReadOnlyCollection<AbpWorkflowStepBody> GetAllStepBodys()
        {
            return AbpStepBodys.Values;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public AbpWorkflowStepBody GetStepBodyOrNull(string name)
        {
            return AbpStepBodys.GetOrDefault(name);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        public void RemoveStepBody(string name)
        {
            AbpStepBodys.Remove(name);
        }
    }
}
