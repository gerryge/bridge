﻿using System.Collections.Generic;
using Volo.Abp.Validation.StringValues;

namespace Bridge.Workflow.Domain.Workflows
{
    public interface IInputType
    {
        string Name
        {
            get;
        }

        object this[string key]
        {
            get;
            set;
        }

        IDictionary<string, object> Attributes
        {
            get;
        }

        IValueValidator Validator
        {
            get;
            set;
        }
    }


    /// <summary>
    /// 
    /// </summary>
    public class WorkflowParam
    {
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public IInputType InputType { get; set; }
        public object Value { get; set; }
    }
}
