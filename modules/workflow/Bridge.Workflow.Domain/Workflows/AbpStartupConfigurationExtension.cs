﻿using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using Volo.Abp;

namespace Bridge.Workflow.Domain.Workflows
{
    /// <summary>
    /// 
    /// </summary>
    public static class AbpStartupConfigurationExtension
    {
        /// <summary>
        /// 获取码表类型配置
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public static IAbpStepBodyConfiguration GetWorkflowConfiguration(this ApplicationInitializationContext context)
        {
            //if (!config.IocManager.IsRegistered<IAbpStepBodyConfiguration>())
            //{
            //    config.IocManager.Register<IAbpStepBodyConfiguration, AbpStepBodyConfiguration>(DependencyLifeStyle.Singleton);
            //}
            //return config.IocManager.Resolve<IAbpStepBodyConfiguration>();

            return context.ServiceProvider.GetService<IAbpStepBodyConfiguration>();
        }



        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="cameCase"></param>
        /// <param name="indented"></param>
        /// <returns></returns>
        public static string ToJsonString<T>(this IEnumerable<T> source, bool cameCase = false, bool indented = false)
        {
            if (source == null) return string.Empty;
            if (source.Count() < 1) return string.Empty;

            return JsonConvert.SerializeObject(source);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string ToJsonString<T>(this T obj) where T : class
        {
            if (obj == null) return string.Empty;

            return JsonConvert.SerializeObject(obj);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="str"></param>
        /// <returns></returns>
        public static T FromJsonString<T>(this string str)
        {
            if (string.IsNullOrWhiteSpace(str)) return default(T);

            return JsonConvert.DeserializeObject<T>(str);
        }
    }
}
