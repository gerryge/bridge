﻿using Volo.Abp.Domain.Entities;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Bridge.Workflow.Domain
{
    /// <summary>
    /// 流程订阅实体
    /// </summary>
    [Table("WorkflowSubscriptions")]
    public class PersistedSubscription : Entity<Guid>
    {
        public PersistedSubscription() { }

        public PersistedSubscription(Guid key) : base(key) { }

        [MaxLength(36)]
        public string WorkflowId { get; set; }

        public int StepId { get; set; }

        [MaxLength(36)]
        public string ExecutionPointerId { get; set; }

        [MaxLength(256)]
        public string EventName { get; set; }

        [MaxLength(256)]
        public string EventKey { get; set; }

        public DateTime SubscribeAsOf { get; set; }

        [MaxLength(8000)]
        public string SubscriptionData { get; set; }

        [MaxLength(256)]
        public string ExternalToken { get; set; }

        [MaxLength(36)]
        public string ExternalWorkerId { get; set; }

        public DateTime? ExternalTokenExpiry { get; set; }
    }
}
