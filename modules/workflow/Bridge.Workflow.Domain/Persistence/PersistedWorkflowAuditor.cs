﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Volo.Abp.Domain.Entities.Auditing;
using Volo.Abp.MultiTenancy;

namespace Bridge.Workflow.Domain
{
    /// <summary>
    /// 工作流审批记录实体
    /// </summary>
    [Table("WorkflowAuditors")]
    public class PersistedWorkflowAuditor : FullAuditedAggregateRoot<string>, IMultiTenant
    {
        public PersistedWorkflowAuditor() { }

        public PersistedWorkflowAuditor(string key) : base(key) { }

        [MaxLength(36)]
        public string WorkflowId { get; set; }

        [ForeignKey("WorkflowId")]
        public PersistedWorkflow Workflow { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [MaxLength(36)]
        public string ExecutionPointerId { get; set; }

        [ForeignKey("ExecutionPointerId")]
        public PersistedExecutionPointer ExecutionPointer { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public EnumAuditStatus Status { get; set; }
        /// <summary>
        /// 审核时间
        /// </summary>
        public DateTime? AuditTime { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 审核人Id
        /// </summary>
        public Guid UserId { get; set; }
        /// <summary>
        /// 审核人
        /// </summary>
        [MaxLength(256)]
        public string UserIdentityName { get; set; }
        /// <summary>
        /// 头像
        /// </summary>
        [MaxLength(256)]
        public string UserHeadPhoto { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Guid? TenantId { get; set; }
    }
}
