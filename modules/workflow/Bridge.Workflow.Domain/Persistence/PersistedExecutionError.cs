﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Volo.Abp.Domain.Entities;

namespace Bridge.Workflow.Domain
{
    /// <summary>
    /// 工作流执行错误记录实体
    /// </summary>
    [Table("WorkflowExecutionErrors")]
    public class PersistedExecutionError : Entity<Guid>
    {
        public PersistedExecutionError() { }

        public PersistedExecutionError(Guid key) : base(key) { }

        [MaxLength(36)]
        public string WorkflowId { get; set; }

        [MaxLength(36)]
        public string ExecutionPointerId { get; set; }

        public DateTime ErrorTime { get; set; }

        [MaxLength(8000)]
        public string Message { get; set; }
    }
}
