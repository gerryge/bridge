﻿using Volo.Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using WorkflowCore.Models;

namespace Bridge.Workflow.Domain
{
    /// <summary>
    /// 流程节点实体
    /// </summary>
    [Table("WorkflowExecutionPointers")]
    public class PersistedExecutionPointer : Entity<string>
    {
        public PersistedExecutionPointer() { }

        public PersistedExecutionPointer(string key) : base(key) { }

        [MaxLength(36)]
        public string WorkflowId { get; set; }

        [ForeignKey("WorkflowId")]
        public PersistedWorkflow Workflow { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int StepId { get; set; }

        public bool Active { get; set; }

        public DateTime? SleepUntil { get; set; }

        public string PersistenceData { get; set; }

        public DateTime? StartTime { get; set; }

        public DateTime? EndTime { get; set; }

        [MaxLength(256)]
        public string EventName { get; set; }

        [MaxLength(256)]
        public string EventKey { get; set; }

        /// <summary>
        /// 来源节点
        /// </summary>
        [MaxLength(200)]
        public string SourceKey { get; set; }
        /// <summary>
        /// 来源key
        /// </summary>
        [MaxLength(200)]
        public string SourceName { get; set; }

        public bool EventPublished { get; set; }

        [MaxLength(2000)]
        public string EventData { get; set; }

        [MaxLength(256)]
        public string StepName { get; set; }

        public List<PersistedExtensionAttribute> ExtensionAttributes { get; set; } = new List<PersistedExtensionAttribute>();

        public int RetryCount { get; set; }

        public string Children { get; set; }

        public string ContextItem { get; set; }

        [MaxLength(36)]
        public string PredecessorId { get; set; }

        [MaxLength(256)]
        public string Outcome { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public PointerStatus Status { get; set; } 

        [MaxLength(256)]
        public string Scope { get; set; }
    }
}
