﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Volo.Abp.Domain.Entities;

namespace Bridge.Workflow.Domain
{
    /// <summary>
    /// 流程扩展属性实体
    /// </summary>
    [Table("WorkflowExtensionAttributes")]
    public class PersistedExtensionAttribute : Entity<Guid>
    {
        public PersistedExtensionAttribute() { }

        public PersistedExtensionAttribute(Guid key) : base(key) { }

        [MaxLength(36)]
        public string ExecutionPointerId { get; set; }

        [ForeignKey("ExecutionPointerId")]
        public PersistedExecutionPointer ExecutionPointer { get; set; }

        [MaxLength(256)]
        public string AttributeKey { get; set; }

        [MaxLength(256)]
        public string AttributeValue { get; set; }
    }
}
