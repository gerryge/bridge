﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Volo.Abp.Domain.Entities.Auditing;
using Volo.Abp.MultiTenancy;
using WorkflowCore.Models;

namespace Bridge.Workflow.Domain
{
    /// <summary>
    /// 工作流实体
    /// </summary>
    [Table("Workflows")]
    public class PersistedWorkflow : FullAuditedAggregateRoot<string>, IMultiTenant
    {
        public PersistedWorkflow() { }

        public PersistedWorkflow(string key) : base(key)
        {
            //IsDeleted = false;
        }

        [MaxLength(36)]
        public string WorkflowDefinitionId { get; set; }

        [ForeignKey("WorkflowDefinitionId")]
        public PersistedWorkflowDefinition WorkflowDefinition { get; set; }

        public int Version { get; set; }

        [MaxLength(512)]
        public string Description { get; set; }

        [MaxLength(256)]
        public string Reference { get; set; }

        public virtual PersistedExecutionPointerCollection ExecutionPointers { get; set; } = new PersistedExecutionPointerCollection();

        public long? NextExecution { get; set; }

        public string Data { get; set; }

        public DateTime? CompleteTime { get; set; }

        /// <summary>
        /// Runnable=0, 运行
        /// Suspended=1, 挂起、暂停
        /// Complete=2, 完成
        /// Terminated=3 终止
        /// </summary>
        public WorkflowStatus Status { get; set; }

        public Guid? TenantId { get; set; }

        [MaxLength(256)]
        public string CreateUserIdentityName { get; set; }
    }
}
