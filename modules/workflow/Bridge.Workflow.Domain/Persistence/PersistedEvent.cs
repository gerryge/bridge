﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Volo.Abp.Domain.Entities.Auditing;
using Volo.Abp.MultiTenancy;

namespace Bridge.Workflow.Domain
{
    /// <summary>
    /// 工作流事件实体
    /// </summary>
    [Table("WorkflowEvents")]
    public class PersistedEvent : CreationAuditedEntity<Guid>, IMultiTenant
    {
        public PersistedEvent() { }

        public PersistedEvent(Guid key) : base(key) { }

        [MaxLength(256)]
        public string EventName { get; set; }

        [MaxLength(256)]
        public string EventKey { get; set; }

        public string EventData { get; set; }

        public DateTime EventTime { get; set; }

        public bool IsProcessed { get; set; }

        public Guid? TenantId { get; set; }
    }
}
