﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Volo.Abp.Domain.Entities.Auditing;
using Volo.Abp.MultiTenancy;

namespace Bridge.Workflow.Domain
{
    /// <summary>
    /// 流程定义表
    /// </summary>
    [Table("workflowdefinitions")]
    public class PersistedWorkflowDefinition : FullAuditedAggregateRoot<string>, IMultiTenant
    {
        public PersistedWorkflowDefinition() { }

        public PersistedWorkflowDefinition(string key) : base(key) { }

        public Guid? TenantId { get; set; }
        [MaxLength(256)]
        public string Title { get; set; }

        [Key]
        public int Version { get; set; }

        [MaxLength(4000)]
        public string Description { get; set; }

        [MaxLength(256)]
        public string Icon { get; set; }
        [MaxLength(256)]
        public string Color { get; set; }
        [MaxLength(256)]
        public string Group { get; set; }

        /// <summary>
        /// 输入
        /// </summary>
        public IEnumerable<IEnumerable<IEnumerable<WorkflowFormData>>> Inputs { get; set; }

        /// <summary>
        /// 流程节点
        /// </summary>
        public IEnumerable<WorkflowNode> Nodes { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="title"></param>
        /// <param name="version"></param>
        /// <param name="inputs"></param>
        /// <param name="nodes"></param>
        /// <param name="tenantId"></param>
        public PersistedWorkflowDefinition(string id,
            string title,
            int version,
            List<List<List<WorkflowFormData>>> inputs,
            List<WorkflowNode> nodes,
            Guid? tenantId = null)
        {
            this.Id = id;
            this.Title = title;
            this.Version = version;
            this.Inputs = inputs;
            this.Nodes = nodes;
            this.TenantId = tenantId;
            this.CreationTime = DateTime.Now;
            this.IsDeleted = false;
        }
    }
}
