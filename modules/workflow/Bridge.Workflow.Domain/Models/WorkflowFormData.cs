﻿using System;
using System.Collections.Generic;

namespace Bridge.Workflow
{
    public class WorkflowFormData
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Label { get; set; }
        public string Type { get; set; }
        public object Value { get; set; }

        public IEnumerable<object> Styles { get; set; }
        public int? MaxLength { get; set; }
        public int? MinLength { get; set; }
        /// <summary>
        /// 选项
        /// </summary>
        public IEnumerable<object> Items { get; set; }
        /// <summary>
        /// 验证
        /// </summary>
        public IEnumerable<object> Rules { get; set; } = new List<object>();
    }
}
