﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Bridge.Workflow.Domain.Workflows;
using Volo.Abp.Validation.StringValues;
using System.Linq;

namespace Bridge.Workflow.Domain
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public abstract class InputTypeBase : IInputType
    {
        public virtual string Name
        {
            get
            {
                TypeInfo typeInfo = GetType().GetTypeInfo();
                if (typeInfo.IsDefined(typeof(InputTypeAttribute)))
                {
                    var name = typeInfo?.CustomAttributes?.Last().ConstructorArguments?.First().Value?.ToString();
                    //var name = typeInfo.GetCustomAttributes(typeof(InputTypeAttribute)).Cast<InputTypeAttribute>().First()
                    //    .Name;
                    return string.IsNullOrEmpty(name) ? typeInfo?.Name : name;
                }

                return typeInfo.Name;
            }
        }

        //
        // 摘要:
        //     Gets/sets arbitrary objects related to this object. Gets null if given key does
        //     not exists.
        //
        // 参数:
        //   key:
        //     Key
        public object this[string key]
        {
            get
            {
                return Attributes.GetOrDefault(key);
            }
            set
            {
                Attributes[key] = value;
            }
        }

        //
        // 摘要:
        //     Arbitrary objects related to this object.
        public IDictionary<string, object> Attributes
        {
            get;
            private set;
        }

        public IValueValidator Validator
        {
            get;
            set;
        }

        protected InputTypeBase()
            : this(new AlwaysValidValueValidator())
        {
        }

        protected InputTypeBase(IValueValidator validator)
        {
            Attributes = new Dictionary<string, object>();
            Validator = validator;
        }

        public static string GetName<TInputType>() where TInputType : IInputType
        {
            return ((IInputType)Activator.CreateInstance(typeof(TInputType))).Name;
        }
    }
}
