﻿using System;
using Volo.Abp.Validation.StringValues;

namespace Bridge.Workflow.Domain
{
    [AttributeUsage(AttributeTargets.Class)]
    public class InputTypeAttribute : Attribute
    {
        public string Name
        {
            get;
            set;
        }

        public InputTypeAttribute(string name) { }
    }

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    [InputType("SELECT_USERS")]
    public class SelectUserInputType : InputTypeBase
    {
        public SelectUserInputType()
        {

        }

        public SelectUserInputType(IValueValidator validator)
            : base(validator)
        {

        }
    }

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    [InputType("SELECT_ROLES")]
    public class SelectRoleInputType: InputTypeBase
    {
        public SelectRoleInputType()
        {

        }

        public SelectRoleInputType(IValueValidator validator)
            : base(validator)
        {

        }
    }
}
