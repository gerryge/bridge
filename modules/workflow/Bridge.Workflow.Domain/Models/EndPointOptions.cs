﻿namespace Bridge.Workflow
{
    /// <summary>
    /// 
    /// </summary>
    public class EndPointOptions
    {
        public string Anchor { get; set; }
        public int MaxConnections { get; set; }
        public string Uuid { get; set; }
    }
}