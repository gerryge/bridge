﻿using System;
using System.Collections.Generic;
using Bridge.Workflow.Domain;

namespace Bridge.Workflow
{
    /// <summary>
    /// 工作流流程定义节点实体类
    /// </summary>
    public class WorkflowNode
    {
        /// <summary>
        /// 节点主键
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// 节点标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 图标
        /// </summary>
        public string Icon { get; set; }

        /// <summary>
        /// 分组
        /// </summary>
        public int Group { get; set; }

        /// <summary>
        /// 节点描述
        /// </summary>
        public IEnumerable<EndPointOptions> EndPointOptions { get; set; }

        /// <summary>
        /// 位置
        /// </summary>
        public int[] Position { get; set; }

        /// <summary>
        /// 类型[left,top]
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// 步骤实体
        /// </summary>
        public AbpStepBodyInput StepBody { get; set; }

        /// <summary>
        /// 上一节点
        /// </summary>
        public IEnumerable<string> ParentNodes { get; set; }

        /// <summary>
        /// 下一节点
        /// </summary>
        public IEnumerable<WorkflowConditionNode> NextNodes { get; set; }

        /// <summary>
        /// 节点操作
        /// </summary>
        public IEnumerable<NodeOperate> Operate { get; set; }

        /// <summary>
        /// 是否必要节点（true必要节点 不可删除，false 非必要节点 可删除）
        /// </summary>
        public bool Required { get; set; }
    }

    /// <summary>
    /// 步骤操作
    /// </summary>
    public class NodeOperate
    {
        /// <summary>
        /// 操作名称
        /// </summary>
        public string OperateName { get; set; }
        /// <summary>
        /// 操作标识
        /// </summary>
        public string OperateEvent { get; set; }
        /// <summary>
        /// 业务状态码
        /// </summary>
        public string BusinessStatus { get; set; }
        /// <summary>
        /// 跳转页面url
        /// </summary>
        public string PageUrl { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class AbpStepBodyInput
    {
        public string Name { get; set; } 
        public string DisplayName { get; set; }
        public Dictionary<string, WorkflowParamInput> Inputs { get; set; } = new Dictionary<string, WorkflowParamInput>();
        public string StepBodyType { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class WorkflowParamInput
    {
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        //public InputTypeBase InputType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public object Value { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class WorkflowConditionNode
    {
        public string Label { get; set; }
        public string NodeId { get; set; }
        public IEnumerable<WorkflowConditionCondition> Conditions { get; set; } = new List<WorkflowConditionCondition>();
    }

    /// <summary>
    /// 
    /// </summary>
    public class WorkflowConditionCondition
    {
        public string Field { get; set; }
        public string Operator { get; set; }
        public object Value { get; set; }
        public string ToNodeName { get; set; }
        public string ToNodeKey { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class WorkflowData
    {
        /// <summary>
        /// 业务表id
        /// </summary>
        public string BusinessId { get; set; }
        /// <summary>
        /// 业务单据号
        /// </summary>
        public string BusinessCode { get; set; }
        /// <summary>
        /// 机构id
        /// </summary>
        public string AgencyId { get; set; }

        /// <summary>
        /// 总金额
        /// </summary>
        public decimal SumPrice { get; set; }
        /// <summary>
        /// 产品大类名
        /// </summary>
        public string ProductClassName { get; set; }
        /// <summary>
        /// 控制通过和驳回
        /// </summary>
        public string IsApproved { get; set; }

    }

    public class EditBusinessDataInput: WorkflowData
    {

        /// <summary>
        /// 业务流程ID
        /// </summary>
        public string WorkflowId { get; set; }
        
    }
}
