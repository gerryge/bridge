﻿using System;
using System.ComponentModel.DataAnnotations;
using Volo.Abp.Auditing;

namespace Bridge.Workflow.Users.Request
{
    public class UserLoginInfoRequest
    {
        /// <summary>
        /// 
        /// </summary>
        [Required]
        public string UserNameOrEmailAddress { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        [Required]
        [StringLength(16)]
        [DataType(DataType.Password)]
        [DisableAuditing]
        public string Password { get; set; }

        public bool RememberMe { get; set; }
    }
}
