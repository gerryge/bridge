﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using Volo.Abp;
using Volo.Abp.Identity;

namespace Bridge.Workflow.Users
{
    /// <summary>
    /// 
    /// </summary>
    //[Serialization]
    public class User : IdentityUser
    {
        public User(Guid id, string userName, string email, Guid? tenantId = null) :
            base(id, userName, email, tenantId)
        {
            Check.NotNull<string>(userName, "userName");
            TenantId = tenantId;
            UserName = userName;
            NormalizedUserName = userName.ToUpperInvariant();
            if (!string.IsNullOrWhiteSpace(email))
            {
                Email = "";
                NormalizedEmail = "".ToUpperInvariant();
            }
            ConcurrencyStamp = Guid.NewGuid().ToString();
            SecurityStamp = Guid.NewGuid().ToString();
            Roles = new Collection<IdentityUserRole>();
            Claims = new Collection<IdentityUserClaim>();
            Logins = new Collection<IdentityUserLogin>();
            Tokens = new Collection<IdentityUserToken>();
            OrganizationUnits = new Collection<IdentityUserOrganizationUnit>();
        }

        [Required(AllowEmptyStrings = true)]    //允许空字符串
        public override string Email { get => base.Email; protected set => base.Email = value; }
    }
}
