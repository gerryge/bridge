﻿using System;
namespace Bridge.Workflow.Users.Response
{
    public class LoginResponse
    {
        public LoginResponse(LoginResultTypeEnum result)
        {
            this.Result = result;
        }

        public LoginResultTypeEnum Result { get; set; }

        public string Description { get; set; }

    }
}
