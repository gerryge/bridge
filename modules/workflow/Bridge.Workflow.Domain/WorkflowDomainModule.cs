﻿using Bridge.Workflow.Domain.Workflows;
using Microsoft.Extensions.DependencyInjection;
using Volo.Abp;
using Volo.Abp.Domain;
using Volo.Abp.Modularity;

namespace Bridge.Workflow
{
    [DependsOn(
        typeof(AbpDddDomainModule),
        typeof(WorkflowDomainSharedModule)
        //typeof(WorkflowCore)
    )]
    public class WorkflowDomainModule : AbpModule
    {
        public override void OnPostApplicationInitialization(ApplicationInitializationContext context)
        {
            base.OnPostApplicationInitialization(context);

            //初始化所有的流程定义
            var wfManager = context.ServiceProvider.GetService<WorkflowDefinitionManager>();

            wfManager.Initialize(context.ServiceProvider);
        }
    }
}
