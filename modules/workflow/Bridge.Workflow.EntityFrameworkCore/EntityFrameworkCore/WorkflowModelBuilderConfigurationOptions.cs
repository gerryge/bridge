﻿using JetBrains.Annotations;
using Volo.Abp.EntityFrameworkCore.Modeling;

namespace Bridge.Workflow.EntityFrameworkCore
{
    public class WorkflowModelBuilderConfigurationOptions : AbpModelBuilderConfigurationOptions
    {
        public WorkflowModelBuilderConfigurationOptions(
            [NotNull] string tablePrefix = "",
            [CanBeNull] string schema = null)
            : base(
                tablePrefix,
                schema)
        {
            
        }
    }
}