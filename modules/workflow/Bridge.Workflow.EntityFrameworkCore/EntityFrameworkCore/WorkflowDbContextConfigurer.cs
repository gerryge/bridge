﻿using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace Bridge.Workflow.EntityFrameworkCore
{
    /// <summary>
    /// 
    /// </summary>
    public static class WorkflowDbContextConfigurer
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="connectionString"></param>
        public static void Configure(DbContextOptionsBuilder<WorkflowDbContext> builder, string connectionString)
        {
            //builder.UseMySql(builder,connectionString);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="connection"></param>
        public static void Configure(DbContextOptionsBuilder<WorkflowDbContext> builder, DbConnection connection)
        {
            //builder.UseMySql(connection);
        }
    }
}
