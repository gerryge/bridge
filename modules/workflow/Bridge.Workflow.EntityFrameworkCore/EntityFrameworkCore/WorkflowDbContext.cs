﻿using Bridge.Workflow.Domain;
using Bridge.Workflow.Users;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Data;
using Volo.Abp.EntityFrameworkCore;

namespace Bridge.Workflow.EntityFrameworkCore
{
    /// <summary>
    /// 
    /// </summary>
    [ConnectionStringName(WorkflowDbProperties.ConnectionStringName)]
    public class WorkflowDbContext : AbpDbContext<WorkflowDbContext>, IWorkflowDbContext
    {
        /* Add DbSet for each Aggregate Root here. Example:
         * public DbSet<Question> Questions { get; set; }
         */

        //public DbSet<User> User { get; set; }
        public DbSet<PersistedEvent> PersistedEvents { get; set; }
        public DbSet<PersistedExecutionError> PersistedExecutionErrors { get; set; }
        public DbSet<PersistedExecutionPointer> PersistedExecutionPointers { get; set; }
        public DbSet<PersistedExtensionAttribute> PersistedExtensionAttributes { get; set; }
        public DbSet<PersistedSubscription> PersistedSubscriptions { get; set; }
        public DbSet<PersistedWorkflow> PersistedWorkflows { get; set; }
        public DbSet<PersistedWorkflowDefinition> PersistedWorkflowDefinitions { get; set; }
        public DbSet<PersistedWorkflowAuditor> PersistedWorkflowAuditors { get; set; }


        public WorkflowDbContext(DbContextOptions<WorkflowDbContext> options) 
            : base(options)
        {

        }

        

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.ConfigureWorkflow();
        }
    }
}