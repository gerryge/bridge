﻿using System;
using Bridge.Workflow.EntityFrameworkCore.Persistence;
using Microsoft.Extensions.DependencyInjection;
using Volo.Abp;
using Volo.Abp.EntityFrameworkCore;
using Volo.Abp.Modularity;
using WorkflowCore.Interface;
using WorkflowCore.Models;
using WorkflowCore.Models.LifeCycleEvents;

namespace Bridge.Workflow.EntityFrameworkCore
{
    [DependsOn(
        typeof(WorkflowDomainModule),
        typeof(AbpEntityFrameworkCoreModule)
    )]
    public class WorkflowEntityFrameworkCoreModule : AbpModule
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddAbpDbContext<WorkflowDbContext>(options =>
            {
                /* Add custom repositories here. Example:
                 * options.AddRepository<Question, EfCoreQuestionRepository>();
                 */
                options.AddDefaultRepositories(true);
            });

            //注入工作流持久化为单例模式
            context.Services.AddSingleton<IAbpPersistenceProvider, AbpPersistenceProvider>();
            context.Services.AddWorkflow(cfg =>
            {
                //var connectionString = context.Services.GetConfiguration()["ConnectionStrings:Default"];
                //cfg.UseMySQL(connectionString, true, true);
                cfg.UsePersistence(sp => sp.GetService<IAbpPersistenceProvider>());
                //cfg.UseElasticsearch(new ConnectionSettings(new Uri("http://elastic:9200")), "workflows");
            });

            context.Services.AddWorkflowDSL();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public override void OnPostApplicationInitialization(ApplicationInitializationContext context)
        {
            base.OnPostApplicationInitialization(context);

            //启动工作流引擎
            //var host = context.ServiceProvider.GetService<IWorkflowHost>();
            //host.OnLifeCycleEvent += Host_OnLifeCycleEvent;
            //host.OnStepError += Host_OnStepError;
            //host?.Start();
        }

        /// <summary>
        /// 全局异常错误处理
        /// </summary>
        /// <param name="workflow"></param>
        /// <param name="step"></param>
        /// <param name="exception"></param>
        private void Host_OnStepError(WorkflowInstance workflow, WorkflowStep step, Exception exception)
        {
            //throw new System.NotImplementedException();

            
        }

        /// <summary>
        /// 事件回收机制
        /// </summary>
        /// <param name="evt"></param>
        private void Host_OnLifeCycleEvent(LifeCycleEvent evt)
        {
            //throw new System.NotImplementedException();
        }

        /// <summary>
        /// 应用关闭时停止工作流引擎
        /// </summary>
        /// <param name="context"></param>
        public override void OnApplicationShutdown(ApplicationShutdownContext context)
        {
            base.OnApplicationShutdown(context);
            //停止工作流引擎
            var host = context.ServiceProvider.GetService<IWorkflowHost>();
            host?.Stop();
        }
    }
}