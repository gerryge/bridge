﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Bridge.Workflow.Domain;
using WorkflowCore.Interface;

namespace Bridge.Workflow.EntityFrameworkCore.Persistence
{
    /// <summary>
    /// 
    /// </summary>
    public interface IAbpPersistenceProvider : IPersistenceProvider
    {
        Task<PersistedWorkflow> GetPersistedWorkflow(string id);
        Task<IEnumerable<PersistedWorkflow>> GetAllRunnablePersistedWorkflow(string definitionId, int version);
        Task<PersistedExecutionPointer> GetPersistedExecutionPointer(string id);
        Task<PersistedWorkflowDefinition> GetPersistedWorkflowDefinition(string id, int version);
        /// <summary>
        /// 写入审核记录
        /// </summary>
        /// <param name="insertModel"></param>
        /// <returns></returns>
        Task<PersistedWorkflowAuditor> InsertAuditor(PersistedWorkflowAuditor insertModel);
        Task<bool> AuditorAnyAsync(string id, Guid userId, EnumAuditStatus auditStatus);
        Task<int> AuditorCountAsync(string id, EnumAuditStatus auditStatus);
    }
}
