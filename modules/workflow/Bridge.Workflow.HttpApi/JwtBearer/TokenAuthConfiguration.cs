﻿using System;
using Microsoft.IdentityModel.Tokens;

namespace Bridge.Workflow.JwtBearer
{
    public class TokenAuthConfiguration
    {
        public string SecurityKey { get; set; }

        public string Issuer { get; set; }

        public string Audience { get; set; }

        public SigningCredentials SigningCredentials { get; set; }

        public TimeSpan Expiration { get; set; }
    }
}
