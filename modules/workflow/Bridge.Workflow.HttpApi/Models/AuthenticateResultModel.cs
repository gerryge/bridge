﻿using System;
using Newtonsoft.Json;

namespace Bridge.Workflow.Models
{
    public class AuthenticateResultModel
    {
        [JsonProperty("accessToken")]
        public string AccessToken { get; set; }

        [JsonProperty("encryptedAccessToken")]
        public string EncryptedAccessToken { get; set; }

        [JsonProperty("expireInSeconds")]
        public int ExpireInSeconds { get; set; }

        [JsonProperty("userId")]
        public string UserId { get; set; }
    }
}
