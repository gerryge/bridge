﻿using Bridge.Workflow.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridge.Workflow.Models
{
    //如果好用，请收藏地址，帮忙分享。
    public class Sides
    {
        /// <summary>
        /// 
        /// </summary>
        public int host { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int tenant { get; set; }
    }

    public class MultiTenancy
    {
        /// <summary>
        /// 
        /// </summary>
        public string isEnabled { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ignoreFeatureCheckForHostUsers { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Sides sides { get; set; }
    }

    public class Session
    {
        /// <summary>
        /// 
        /// </summary>
        public string userId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string tenantId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string impersonatorUserId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string impersonatorTenantId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int multiTenancySide { get; set; }
    }

    public class CurrentCulture
    {
        /// <summary>
        /// 
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// 英语
        /// </summary>
        public string displayName { get; set; }
    }

    public class LanguagesItem
    {
        /// <summary>
        /// 
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string displayName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string icon { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string isDefault { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string isDisabled { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string isRightToLeft { get; set; }
    }

    public class CurrentLanguage
    {
        /// <summary>
        /// 
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string displayName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string icon { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string isDefault { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string isDisabled { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string isRightToLeft { get; set; }
    }

    public class SourcesItem
    {
        /// <summary>
        /// 
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string type { get; set; }
    }

    public class Abp
    {
        /// <summary>
        /// 
        /// </summary>
        public string AllOfTheseFeaturesMustBeEnabled { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string AllOfThesePermissionsMustBeGranted { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string AtLeastOneOfTheseFeaturesMustBeEnabled { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string AtLeastOneOfThesePermissionsMustBeGranted { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string CurrentUserDidNotLoginToTheApplication { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string DefaultFromSenderDisplayName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string DefaultFromSenderEmailAddress { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string DefaultLanguage { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string DomainName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string FeatureIsNotEnabled { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string MainMenu { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Password { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ReceiveNotifications { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string SmtpHost { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string SmtpPort { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string TimeZone { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string UseDefaultCredentials { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Username { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string UseSSL { get; set; }
    }

    public class AbpWeb
    {
        /// <summary>
        /// 
        /// </summary>
        public string AreYouSure { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Cancel { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string DefaultError { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string DefaultError401 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string DefaultError403 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string DefaultError404 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string DefaultErrorDetail { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string DefaultErrorDetail401 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string DefaultErrorDetail403 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string DefaultErrorDetail404 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string EntityNotFound { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string InternalServerError { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ValidationError { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ValidationNarrativeTitle { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Yes { get; set; }
    }

    public class WorkflowDemo
    {
        /// <summary>
        /// 
        /// </summary>
        public string About { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Actions { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string AdminEmailAddress { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Administration { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string AdminPassword { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string All { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string AreYouSureWantToDelete { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Back { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string CanBeEmptyToLoginAsHost { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Cancel { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Change { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ChangeTenant { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Clear { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ClearAll { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ClearOthers { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ConfirmNewPassword { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ConfirmPassword { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string CouldNotCompleteLoginOperation { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string CouldNotValidateExternalUser { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Create { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string CreateNewRole { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string CreateNewTenant { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string CreateNewUser { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string CurrentPassword { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string CurrentTenant { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string DatabaseConnectionString { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string DefaultPasswordIs { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Delete { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string DisplayName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Edit { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string EditRole { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string EditTenant { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string EditUser { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string EmailAddress { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Filter { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string FormIsNotValidMessage { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string FullName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string HomePage { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string InvalidEmailAddress { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string InvalidPattern { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string InvalidUserNameOrPassword { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string IsActive { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string LabelOptions { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string LeaveEmptyToSwitchToHost { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string LogIn { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string LoginFailed { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Logout { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string MultiLevelMenu { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string NameSurname { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string NewPassword { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string No { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string NotSelected { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Off { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string On { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Optional { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string OrLoginWith { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string PairsDoNotMatch { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Password { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string PasswordsDoNotMatch { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string PasswordsMustBeAtLeast8CharactersContainLowercaseUppercaseNumber { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Permissions { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string PleaseEnterAtLeastNCharacter { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string PleaseEnterLoginInformation { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string PleaseEnterNoMoreThanNCharacter { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string PleaseWait { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Refresh { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Register { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string RegisterFormUserNameInvalidMessage { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string RememberMe { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ResetPassword { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ResetPasswordStepOneInfo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ResetPasswordStepTwoInfo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string RoleDeleteWarningMessage { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string RoleDescription { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string RoleName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Roles { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Save { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string SavedSuccessfully { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Search { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string SearchWithThreeDot { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Settings { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Skins { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string StartTyping { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string SuccessfullyDeleted { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string SuccessfullyRegistered { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Surname { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string TenancyName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string TenantDeleteWarningMessage { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string TenantIdIsNotActive { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string TenantIsNotActive { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string TenantName_Regex_Description { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string TenantNameCanNotBeEmpty { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Tenants { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string TenantSelection { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string TenantSelection_Detail { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ThereIsNoTenantDefinedWithName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ThisFieldIsRequired { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string TotalRecordsCount { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string UnknownTenantId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string UpdatePassword { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string UserDeleteWarningMessage { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string UserDetails { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string UserEmailIsNotConfirmedAndCanNotLogin { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string UserIsNotActiveAndCanNotLogin { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string UserLockedOutMessage { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string UserNameOrEmail { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string UserRoles { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Users { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Version { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string WaitingForActivationMessage { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string WaitingForEmailActivation { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string WelcomeMessage { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Yes { get; set; }
    }

    public class Values
    {
        /// <summary>
        /// 
        /// </summary>
        public Abp Abp { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public AbpWeb AbpWeb { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public WorkflowDemo WorkflowDemo { get; set; }
    }

    public class Localization
    {
        /// <summary>
        /// 
        /// </summary>
        public CurrentCulture currentCulture { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<LanguagesItem> languages { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public CurrentLanguage currentLanguage { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<SourcesItem> sources { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Values values { get; set; }
    }

    public class AllFeatures
    {
    }

    public class Features
    {
        /// <summary>
        /// 
        /// </summary>
        public AllFeatures allFeatures { get; set; }
    }

    public class GrantedPermissions
    {
    }

    public class Auth
    {
        /// <summary>
        /// 
        /// </summary>
        public GrantedPermissions grantedPermissions { get; set; }
    }

    public class MainMenu
    {
        /// <summary>
        /// 
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string displayName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string customData { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<string> items { get; set; }
    }

    public class Menus
    {
        /// <summary>
        /// 
        /// </summary>
        public MainMenu MainMenu { get; set; }
    }

    public class Nav
    {
        /// <summary>
        /// 
        /// </summary>
        public Menus menus { get; set; }
    }

    public class Setting
    {
        /// <summary>
        /// 
        /// </summary>
        public Values values { get; set; }
    }

    public class Clock
    {
        /// <summary>
        /// 
        /// </summary>
        public string provider { get; set; }
    }

    public class Windows
    {
        /// <summary>
        /// 
        /// </summary>
        public string timeZoneId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int baseUtcOffsetInMilliseconds { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int currentUtcOffsetInMilliseconds { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string isDaylightSavingTimeNow { get; set; }
    }

    public class Iana
    {
        /// <summary>
        /// 
        /// </summary>
        public string timeZoneId { get; set; }
    }

    public class TimeZoneInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public Windows windows { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Iana iana { get; set; }
    }

    public class Timing
    {
        /// <summary>
        /// 
        /// </summary>
        public TimeZoneInfo timeZoneInfo { get; set; }
    }

    public class AntiForgery
    {
        /// <summary>
        /// 
        /// </summary>
        public string tokenCookieName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string tokenHeaderName { get; set; }
    }

    public class Security
    {
        /// <summary>
        /// 
        /// </summary>
        public AntiForgery antiForgery { get; set; }
    }

    public class Custom
    {
    }

    public class Result
    {
        /// <summary>
        /// 
        /// </summary>
        public MultiTenancy multiTenancy { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Session session { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Localization localization { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Features features { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Auth auth { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Nav nav { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Setting setting { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Clock clock { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Timing timing { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Security security { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Custom custom { get; set; }
    }

    public class AbpConfigurationModel
    {
        /// <summary>
        /// 
        /// </summary>
        public Result result { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string targetUrl { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string success { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string error { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string unAuthorizedRequest { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string __abp { get; set; }
    }
}
