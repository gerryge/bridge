﻿using System;
using System.ComponentModel.DataAnnotations;
using Volo.Abp.Auditing;

namespace Bridge.Workflow.Models
{
    public class AuthenticateModel
    {
        [Required]
        //[StringLength(AbpUserBase.MaxEmailAddressLength)]
        public string UserNameOrEmailAddress { get; set; }

        [Required]
        //[StringLength(AbpUserBase.MaxPlainPasswordLength)]
        [DisableAuditing]
        public string Password { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool RememberClient { get; set; }
    }
}
