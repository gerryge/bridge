﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using System.Threading.Tasks;
using Volo.Abp.AspNetCore.Mvc;

namespace Bridge.Workflow.Extensions
{
    /// <summary>
    /// 全局异常捕获
    /// </summary>
    public class ExcptionExtension : IExceptionFilter
    {
        public ExcptionExtension()
        {

        }
        public void OnException(ExceptionContext context)
        {
            var logger = context.GetService<ILogger<ExcptionExtension>>(NullLogger<ExcptionExtension>.Instance);

            var ex = context.Exception;
            context.Result = new JsonResult(new { code = 0, message = ex.Message });
            context.ExceptionHandled = true;
            //todo 持久化日志
            logger.LogError(ex.ToString());
        }
    }
}
