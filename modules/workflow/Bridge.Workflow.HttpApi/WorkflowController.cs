﻿using Bridge.Workflow.Localization;
using Volo.Abp.AspNetCore.Mvc;

namespace Bridge.Workflow
{
    public abstract class WorkflowController : AbpController
    {
        protected WorkflowController()
        {
            LocalizationResource = typeof(WorkflowResource);
        }
    }
}
