﻿using Bridge.Workflow.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Volo.Abp.AspNetCore.Mvc;
using SystemIo = System.IO;

namespace Bridge.Workflow.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/v1/Workflow/AbpUserConfiguration/[action]")]
    public class AbpUserConfiguration : AbpController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public AbpConfigurationModel GetAll()
        {

            var text = SystemIo.File.ReadAllText("wwwroot/localization.txt");

            return JsonConvert.DeserializeObject<AbpConfigurationModel>(text);
        }
    }
}
