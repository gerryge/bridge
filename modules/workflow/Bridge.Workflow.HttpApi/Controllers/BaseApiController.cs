﻿using Newtonsoft.Json;

namespace Bridge.Workflow.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Response<T>
    {
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("result")]
        public T Result { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("TargetUrl")]
        public string TargetUrl { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("Success")]
        public bool success { get; set; } = true;

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("error")]
        public string Error { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("unAuthorizedRequest")]
        public bool UnAuthorizedRequest { get; set; } = false;

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("__abp")]
        public bool Abp { get; set; } = true;

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <returns></returns>
        public static Response<T> Success(T data)
        {
            return new Response<T>
            {
                Result = data,
                success = true,
                UnAuthorizedRequest = false
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static Response<T> Fail(string message)
        {
            return new Response<T>
            {
                success = false,
                UnAuthorizedRequest = false,
                Error = message
            };
        }
    }


    /// <summary>
    /// 
    /// </summary>
    //public class BaseApiController : AbpController
    //{
    //    /// <summary>
    //    /// 
    //    /// </summary>
    //    /// <param name="context"></param>
    //    public override void OnActionExecuted(ActionExecutedContext context)
    //    {
    //        if (context.Exception == null)
    //        {   //执行成功 取得由 API 返回的资料
    //            ObjectResult result = context.Result as ObjectResult;
    //            if (result != null)
    //            {   // 重新封装回传格式
    //                Response<object> robj = new Response<object>
    //                {
    //                    Result = result.Value
    //                };
    //                ObjectResult objectResult = new ObjectResult(robj);
    //                context.Result = objectResult;
    //            }
    //        }
    //        base.OnActionExecuted(context);
    //    }
    //}
}
