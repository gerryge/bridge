﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Bridge.Workflow.Application;
using Bridge.Workflow.Domain.Workflows;
using Bridge.Workflow.Workflows.Dtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NUglify.Helpers;
using Volo.Abp.Application.Dtos;
using Volo.Abp.AspNetCore.Mvc;

namespace Bridge.Workflow.Controllers
{
    /// <summary>
    /// 工作流流程定义管理
    /// </summary>
    [Authorize]
    [Area("workflowdefinition")]
    [Route("api/v1/Workflow/workflowdefinition/[action]")]
    public class WorkflowDefinitionController : AbpController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly WorkflowDesignService _designService;

        /// <summary>
        /// 
        /// </summary>
        public WorkflowDefinitionController(WorkflowDesignService workflowDesignService)
        {
            _designService = workflowDesignService;
        }

        /// <summary>
        /// 创建流程定义
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<Response<string>> CreateAsync([FromBody] WorkflowDesignInfo input)
        {
            var result = await _designService.CreateAsync(input);
            if (result == null)
                return Response<string>.Success(string.Empty);
            else
                return Response<string>.Fail("创建流程失败！");
        }

        /// <summary>
        /// 更新流程定义
        /// </summary>
        /// <param name="id"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<Response<WorkflowDesignInfo>> UpdateAsync(string id, [FromBody] WorkflowDesignInfo input)
        {
            var result = await _designService.UpdateAsync(id, input);

            return Response<WorkflowDesignInfo>.Success(result);
        }

        /// <summary>
        /// 删除流程定义
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<Response<string>> DeleteAsync(string id)
        {
            await _designService.DeleteAsync(id);
            return Response<string>.Success(string.Empty);
        }

        /// <summary>
        /// 获取流程定义详情
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<Response<WorkflowDesignInfo>> GetAsync(string id)
        {
            var result = await _designService.GetAsync(id);

            return Response<WorkflowDesignInfo>.Success(result);
        }

        /// <summary>
        /// 获取所有分组的流程
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<Response<IEnumerable<string>>> GetAllGroupAsync()
        {
            var result = await _designService.GetAllGroupAsync();

            return Response<IEnumerable<string>>.Success(result);
        }

        /// <summary>
        /// 获取所有流程步骤
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public Response<List<AbpWorkflowStepBody_two>> GetAllStepBodys()
        {
            var result = _designService.GetAllStepBodys();
            List<AbpWorkflowStepBody_two> stepBody = new();
            result.ForEach(s =>
            {
                stepBody.Add(new AbpWorkflowStepBody_two
                {
                    DisplayName = s.DisplayName,
                    Inputs = s.Inputs,
                    Name = s.Name
                });
            });
            var data = Response<List<AbpWorkflowStepBody_two>>.Success(stepBody);
            return data;
        }

        /// <summary>
        /// 获取分组流程定义
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<Response<Dictionary<string, IEnumerable<WorkflowDesignInfo>>>> GetAllWithGroupAsync(WorkflowListInput input)
        {
            var result = await _designService.GetAllWithGroupAsync(input);

            return Response<Dictionary<string, IEnumerable<WorkflowDesignInfo>>>.Success(result);
        }

        /// <summary>
        /// 获取所有流程定义详情
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<Response<PagedResultDto<WorkflowDesignInfo>>> GetListAsync(WorkflowListInput input)
        {
            var result = await _designService.GetListAsync(input);

            return Response<PagedResultDto<WorkflowDesignInfo>>.Success(result);
        }
    }
}
