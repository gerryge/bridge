﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bridge.Shared;
using Bridge.Shared.Models;
using Bridge.Workflow.Workflow.Dto;
using Bridge.Workflow.Workflows.Dtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp.Application.Dtos;
using Volo.Abp.AspNetCore.Mvc;

namespace Bridge.Workflow.Controllers
{
    /// <summary>
    /// 工作流审批相关
    /// </summary>
    [Authorize]
    [Area("workflowaudit")]
    [Route("api/v1/Workflow/workflowaudit/[action]")]
    public class WorkflowAuditController : AbpController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IWorkflowAuditService _workflowAuditAppService;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="workflowAuditAppService"></param>
        public WorkflowAuditController(IWorkflowAuditService workflowAuditAppService)
        {
            this._workflowAuditAppService = workflowAuditAppService;
        }

        /// <summary>
        /// 修改业务数据
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseResult> EditBusinessDataAsync(EditBusinessDataInput input)
        {
            return await _workflowAuditAppService.EditBusinessDataAsync(input);
        }
        /// <summary>
        /// 审批流程
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseResult> AuditAsync([FromBody] WorkflowAuditInput input)
        {
            return await _workflowAuditAppService.AuditAsync(input);
        }

        /// <summary>
        /// 终止流程
        /// </summary>
        /// <param name="workflowId"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseResult> TerminateAsync([FromBody] TerminateWorkflow input)
        {
            return await _workflowAuditAppService.TerminateAsync(input);
        }

        /// <summary>
        /// 审批流程
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseResult> AuditByBusinessIdAsync([FromBody] WorkflowAuditByBusinessIdInput input)
        {
            return await _workflowAuditAppService.AuditByBusinessIdAsync(input);
        }

        /// <summary>
        /// 批量审批流程
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseResult> BatchAuditByBusinessIdAsync([FromBody] List<WorkflowAuditByBusinessIdInput> input)
        {
            if (input.Any())
            {
                foreach (var s in input)
                {
                    await _workflowAuditAppService.AuditByBusinessIdAsync(s);
                }
            }
            return ResponseResult.Success(string.Empty);
        }

        /// <summary>
        /// 获取所有流程
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseResult<PagedResultDto<GetMyAuditPageListOutput>>> GetAllAsync(GetMyAuditPageListInput input)
        {
            return await _workflowAuditAppService.GetAllAsync(input);
        }

        /// <summary>
        /// 在途
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseResult<PagedResultDto<GetMyAuditPageListOutput>>> GetProcessingAsync(GetMyAuditPageListInput input)
        {
            return await _workflowAuditAppService.GetProcessingAsync(input);
        }

        /// <summary>
        /// 审批轨迹
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponseResult<WorkflowAuditRecordOutput>> GetAuditRecordsAsync(string id)
        {
            return await _workflowAuditAppService.GetAuditRecordsAsync(id);
        }

        /// <summary>
        /// 获取单据待审核节点信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseResult<GetMyAuditPageListOutput>> GetAuditInfoAsync([FromBody] GetAuditInfoInput input)
        {
            return await _workflowAuditAppService.GetAuditInfoAsync(input);
        }
    }
}
