﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bridge.Workflow.Application;
using Bridge.Workflow.Workflows.Dtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp.Application.Dtos;
using Volo.Abp.AspNetCore.Mvc;

namespace Bridge.Workflow.Controllers
{
    /// <summary>
    /// 工作流管理
    /// </summary>
    [Authorize]
    [Area("workflow")]
    [Route("api/v1/Workflow/workflow/[action]")]
    public class WorkflowController : AbpController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly WorkflowService _workflowAppService;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="workflowAppService"></param>
        public WorkflowController(WorkflowService workflowAppService)
        {
            this._workflowAppService = workflowAppService;
        }


   


        /// <summary>
        /// 发起工作流
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<Response<string>> StartAsync([FromBody] StartWorkflowInput input)
        {
            await _workflowAppService.StartAsync(input);

            return Response<string>.Success(string.Empty);
        }
        /// <summary>
        /// 批量发起发起工作流
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<Response<string>> StartBatchAsync([FromBody] List<StartWorkflowInput> input)
        {
            if (input.Any())
            {
                foreach (var s in input)
                {
                    await _workflowAppService.StartAsync(s);
                }
            }
            return Response<string>.Success(string.Empty);
        }


        /// <summary>
        /// 获取工作流详情
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<Response<WorkflowDto>> GetDetailsAsync(string id)
        {
            var result = await _workflowAppService.GetDetailsAsync(id);

            return Response<WorkflowDto>.Success(result);
        }

        /// <summary>
        /// 获取我的工作流列表
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<Response<PagedResultDto<MyWorkflowListOutput>>> GetPageListAsync(WorkflowListInput input)
        {
            var result = await _workflowAppService.GetPageListAsync(input);

            return Response<PagedResultDto<MyWorkflowListOutput>>.Success(result);
        }

        /// <summary>
        /// 在途
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<Response<PagedResultDto<MyWorkflowListOutput>>> GetProcessingPageListAsync(WorkflowListInput input)
        {
            input.Statuses = new List<int>() { 0 };
            var result = await _workflowAppService.GetPageListAsync(input);

            return Response<PagedResultDto<MyWorkflowListOutput>>.Success(result);
        }


        /// <summary>
        /// 在途
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<Response<PagedResultDto<MyWorkflowListOutput>>> GetProcessingPageListViewAsync(WorkflowListInput input)
        {
            input.Statuses = new List<int>() { 0 };
            var result = await _workflowAppService.GetProcessingPageListViewAsync(input);

            return Response<PagedResultDto<MyWorkflowListOutput>>.Success(result);
        }


        /// <summary>
        /// 已完成（包括终止）
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<Response<PagedResultDto<MyWorkflowListOutput>>> GetCompletePageListAsync(WorkflowListInput input)
        {
            input.Statuses = new List<int>() { 2, 3 };
            var result = await _workflowAppService.GetPageListAsync(input);

            return Response<PagedResultDto<MyWorkflowListOutput>>.Success(result);
        }

        /// <summary>
        /// 发布订阅
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        //[HttpPost]
        //public async Task<Response<string>> PublicEvent([FromBody] PublishEventInput input)
        //{
        //    await _workflowAppService.PublicEvent(input);

        //    return Response<string>.Success(string.Empty);
        //}
    }
}
