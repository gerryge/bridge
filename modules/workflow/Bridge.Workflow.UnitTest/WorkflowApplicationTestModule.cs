﻿using System;
using Volo.Abp.Modularity;

namespace Jzt.Oms.Workflow.UnitTest
{
    [DependsOn(
     typeof(WorkflowApplicationModule),
     typeof(WorkflowDomainModule)
     )]
    public class WorkflowApplicationTestModule : AbpModule
    {

    }
}
