﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Jzt.Oms.Workflow.Application.Contracts;
using Jzt.Oms.Workflow.Domain.Workflows;
using Jzt.Oms.Workflow.Workflows.Dtos;
using Volo.Abp.Application.Dtos;
using WorkflowCore.Testing;
using Xunit;

namespace Jzt.Oms.Workflow.UnitTest
{
    /// <summary>
    /// 
    /// </summary>
    public class WorkflowxUnitTest : WorkflowApplicationTestBase
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IWorkflowService _workflowAppService;

        /// <summary>
        /// 
        /// </summary>
        public WorkflowxUnitTest()
        {
            _workflowAppService = GetRequiredService<IWorkflowService>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Fact(DisplayName = "测试启动工作流")]
        public async Task StartAsync()
        {
            var input = new StartWorkflowInput
            {
                Id = string.Empty,
                Version = 1,
                Inputs = new Dictionary<string, object>
                {
                    { "Type","1"},
                    { "Acmount","1000"},
                    { "dw","RMB"}
                }
            };

            await _workflowAppService.StartAsync(input);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Fact(DisplayName = "测试获取工作流详情")]
        public async Task GetAsync()
        {
            //var id = Guid.NewGuid().ToString();
            //var result = await _workflowAppService.GetDetailsAsync(id);

            //Console.WriteLine(result.ToJsonString());
        }
    }
}
