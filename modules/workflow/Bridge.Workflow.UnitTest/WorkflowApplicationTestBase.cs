﻿using System;

namespace Jzt.Oms.Workflow.UnitTest
{
    /* Inherit from this class for your application layer tests.
  * See SampleAppService_Tests for example.
  */
    public abstract class WorkflowApplicationTestBase : WorkflowTestBase<WorkflowApplicationTestModule>
    {

    }
}
