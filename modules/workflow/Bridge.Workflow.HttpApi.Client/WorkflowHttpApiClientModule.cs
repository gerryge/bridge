﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Http.Client;
using Volo.Abp.Modularity;

namespace Bridge.Workflow
{
    [DependsOn(
        typeof(SystemApplicationContractsModule),
        typeof(AbpHttpClientModule))]
    public class WorkflowHttpApiClientModule : AbpModule
    {
        public const string RemoteServiceName = "AccountService";

        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddHttpClientProxies(
                typeof(SystemApplicationContractsModule).Assembly,
                RemoteServiceName
            );
        }
    }
}
