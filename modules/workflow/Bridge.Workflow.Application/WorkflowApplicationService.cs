﻿using Bridge.Workflow.Localization;
using Volo.Abp.Application.Services;

namespace Bridge.Workflow
{
    public abstract class WorkflowApplicationService : ApplicationService
    {
        protected WorkflowApplicationService()
        {
            LocalizationResource = typeof(WorkflowResource);
            ObjectMapperContext = typeof(WorkflowApplicationModule);
        }
    }
}
