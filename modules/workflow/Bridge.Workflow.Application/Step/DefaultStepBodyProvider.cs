﻿using System;
using Bridge.Workflow.Domain;
using Bridge.Workflow.Domain.Workflows;

namespace Bridge.Workflow.Application
{
    /// <summary>
    /// 
    /// </summary>
    public class DefaultStepBodyProvider : AbpStepBodyProvider
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public override void Build(IAbpStepBodyDefinitionContext context)
        {
            var step1 = new AbpWorkflowStepBody
            {
                Name = "FixedUserAudit",
                DisplayName = "指定用户审核",
                StepBodyType = typeof(GeneralAuditingStepBodyService)
            };
            step1.Inputs.Add(new WorkflowParam()
            {
                InputType = new SelectUserInputType(),
                Name = "UserId",
                DisplayName = "审核人"
            });
            context.Create(step1);

            var step2 = new AbpWorkflowStepBody
            {
                Name = "FixedRoleAudit",
                DisplayName = "指定角色审核",
                StepBodyType = typeof(RoleAuditingStepBodyService)
            };
            step2.Inputs.Add(new WorkflowParam()
            {
                InputType = new SelectRoleInputType(),
                Name = "RoleName",
                DisplayName = "审核角色名"
            });
            context.Create(step2);

            var step3 = new AbpWorkflowStepBody
            {
                Name = "FixedAutoAudit",
                DisplayName = "自动审核通过",
                StepBodyType = typeof(NullStepBodyService)
            };
            //step3.Inputs.Add(new WorkflowParam()
            //{
            //    InputType = new SelectRoleInputType(),
            //    Name = "WorkflowDefinitionId",
            //    DisplayName = "审核节点ID"
            //});
            context.Create(step3);


            var step4 = new AbpWorkflowStepBody
            {
                Name = "FixedCreateUserAudit",
                DisplayName = "指定发起人",
                StepBodyType = typeof(CreateUserStepBody)
            };
           
            context.Create(step4);

           

        }
    }
}
