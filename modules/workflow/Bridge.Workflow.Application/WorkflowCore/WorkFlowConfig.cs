﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridge.Workflow.WorkflowCore
{
    /// <summary>
    /// 工作流配置，如新增业务单据配置请新建类并继承类：BaseApprovalOption
    /// </summary>
    public class WorkFlowConfig
    {
        public static  IConfiguration _config { get; set; }
        /// <summary>
        /// 订单工作流配置
        /// </summary>
        public static OrderApprovalOption OrderOption
        {
            get
            {
                return _config.GetSection("WorkFlowConfig:OrderApprovalOption").Get<OrderApprovalOption>()??new OrderApprovalOption();
            }
        }

        public static BaseApprovalOption LoadConfig(string title)
        {
            BaseApprovalOption option = null;
            switch (title)
            {
                case "订单申请审批工作流":
                    option = OrderOption;
                    break;
                default:
                    option = new BaseApprovalOption();
                    break;
                     
            }

            return option;
        }
    }


    public class OrderApprovalOption:BaseApprovalOption
    {
        
    }


    public class BaseApprovalOption
    {
        /// <summary>
        /// 是否开启同一节点多人都审
        /// </summary>
        public bool IsMultipleApproval { get; set; }

        /// <summary>
        /// 是否开启条件选择节点
        /// </summary>
        public bool IsSelectCondition { get; set; }
    }
}
