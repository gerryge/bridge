﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridge.Workflow.WorkflowCore
{
   public interface IConditionProcess
    {
        public string ExecuteResult(WorkflowData BusinessData);
    }
}
