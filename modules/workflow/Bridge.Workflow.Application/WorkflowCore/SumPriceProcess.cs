﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridge.Workflow.WorkflowCore
{
    public class SumPriceProcess : IConditionProcess
    {
       public WorkflowConditionCondition _condition { get; set; } 

        
        public SumPriceProcess(WorkflowConditionCondition condition)
        {
            this._condition = condition;
        }
        public string ExecuteResult(WorkflowData BusinessData)
        {
            string selectConditionNodeKey = string.Empty;
            decimal sumPrice = BusinessData.SumPrice;

            decimal conditionsSumPrice = 0;
            decimal.TryParse(_condition.Value?.ToString(), out conditionsSumPrice);
            if (_condition.Operator == "==" && sumPrice == conditionsSumPrice)
            {
                selectConditionNodeKey = _condition.ToNodeKey;
               
            }
            if (_condition.Operator == ">=" && sumPrice >= conditionsSumPrice)
            {
                selectConditionNodeKey = _condition.ToNodeKey;
                
            }
            if (_condition.Operator == "<=" && sumPrice <= conditionsSumPrice)
            {
                selectConditionNodeKey = _condition.ToNodeKey;
                
            }
            if (_condition.Operator == "<" && sumPrice < conditionsSumPrice)
            {
                selectConditionNodeKey = _condition.ToNodeKey;
                
            }
            if (_condition.Operator == ">" && sumPrice > conditionsSumPrice)
            {
                selectConditionNodeKey = _condition.ToNodeKey;

            }

            return selectConditionNodeKey;
        }
    }
}
