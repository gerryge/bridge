﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridge.Workflow.WorkflowCore
{
    public class ProductClassProcess : IConditionProcess
    {
        public WorkflowConditionCondition _condition { get; set; }


        public ProductClassProcess(WorkflowConditionCondition condition)
        {
            this._condition = condition;
        }
        public string ExecuteResult(WorkflowData BusinessData)
        {
            string selectConditionNodeKey = string.Empty;
            if (_condition.Operator == "==" && _condition.Value?.ToString() == BusinessData.ProductClassName)
            {
                selectConditionNodeKey = _condition.ToNodeKey;
             
            }
            if (_condition.Operator == "!=" && _condition.Value?.ToString() == BusinessData.ProductClassName)
            {
                selectConditionNodeKey = _condition.ToNodeKey;

            }

            return selectConditionNodeKey;
        }
    }
}
