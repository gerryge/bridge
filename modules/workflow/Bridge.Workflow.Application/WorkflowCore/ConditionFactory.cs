﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridge.Workflow.WorkflowCore
{
    /// <summary>
    /// 条件处理者工厂类，扩展其它的条件选择
    /// </summary>
  public  class ConditionFactory
    {
        public static IConditionProcess CreateProcess(WorkflowConditionCondition condition)
        {
            string Field = condition.Field;
            IConditionProcess process = null;
            switch (Field)
            {
                case "SumPrice":
                    process = new SumPriceProcess(condition);
                    break;
                case "ProductClass":
                    process = new ProductClassProcess(condition);
                    break;
                default:
                    break;

            }
            return process;
        }
    }
}
