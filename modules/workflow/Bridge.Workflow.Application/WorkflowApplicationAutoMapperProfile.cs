﻿using AutoMapper;
using Bridge.Workflow.Domain;
using Bridge.Workflow.Workflows.Dtos;

namespace Bridge.Workflow
{
    public class WorkflowApplicationAutoMapperProfile : Profile
    {
        public WorkflowApplicationAutoMapperProfile()
        {
            CreateMap<PersistedExecutionPointer, ExecutionPointerOutPut>();
            /* You can configure your AutoMapper mapping configuration here.
             * Alternatively, you can split your mapping configurations
             * into multiple profile classes for a better organization. */
        }
    }
}