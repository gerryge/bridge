﻿using Bridge.Workflow.Application;
using Bridge.Workflow.Application.Contracts;
using Bridge.Workflow.Domain.Workflows;
using Microsoft.Extensions.DependencyInjection;
using Volo.Abp;
using Volo.Abp.Application;
using Volo.Abp.AutoMapper;
using Volo.Abp.Modularity;

namespace Bridge.Workflow
{
    [DependsOn(
        typeof(WorkflowDomainModule),
        typeof(WorkflowApplicationContractsModule),
        typeof(AbpDddApplicationModule),
        typeof(AbpAutoMapperModule)
        )]
    public class WorkflowApplicationModule : AbpModule
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddAutoMapperObjectMapper<WorkflowApplicationModule>();
            Configure<AbpAutoMapperOptions>(options =>
            {
                options.AddMaps<WorkflowApplicationModule>(validate: true);
            });

            context.Services.AddTransient<JointlySignSetpBodyService, JointlySignSetpBodyService>();
            context.Services.AddTransient<NullStepBodyService, NullStepBodyService>();
            context.Services.AddTransient<GeneralAuditingStepBodyService, GeneralAuditingStepBodyService>();
            context.Services.AddTransient<RoleAuditingStepBodyService, RoleAuditingStepBodyService>();
            context.Services.AddTransient<CreateUserStepBody, CreateUserStepBody>();
            context.Services.AddSingleton<IAbpStepBodyConfiguration, AbpStepBodyConfiguration>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public override void OnApplicationInitialization(ApplicationInitializationContext context)
        {
            //初始化所有的在途流程
            var wfManager = context.ServiceProvider.GetService<IAbpWorkflowManager>();
            wfManager.Initialize().ConfigureAwait(false);

            context.GetWorkflowConfiguration().Providers.Add<DefaultStepBodyProvider>();
        }
    }
}
