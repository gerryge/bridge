﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Volo.Abp.Application.Dtos;

namespace Bridge.Workflow.Roles.Dto
{
    /// <summary>
    /// 
    /// </summary>
    public class RoleDto : EntityDto<string>
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string DisplayName { get; set; }

        public string NormalizedName { get; set; }

        public string Description { get; set; }

        public List<string> GrantedPermissions { get; set; }
    }
}
