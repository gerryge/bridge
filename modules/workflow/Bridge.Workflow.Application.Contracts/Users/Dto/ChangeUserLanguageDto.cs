using System.ComponentModel.DataAnnotations;

namespace Bridge.Workflow.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}