﻿using System;
using Volo.Abp.Application.Dtos;

namespace Bridge.Workflow.Users.Dto
{
    public class SearchUserOutput : EntityDto<Guid>
    {
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string HeadImage { get; set; }
    }
}
