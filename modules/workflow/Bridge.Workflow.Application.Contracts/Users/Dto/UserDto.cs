using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Identity;

namespace Bridge.Workflow.Users.Dto
{
    //[AutoMapFrom(typeof(User))]
    public class UserDto : EntityDto<Guid>
    {
        [Required]
        //[StringLength(AbpUserBase.MaxUserNameLength)]
        public string UserName { get; set; }

        [Required]
        //[StringLength(AbpUserBase.MaxNameLength)]
        public string Name { get; set; }

        [Required]
        //[StringLength(AbpUserBase.MaxSurnameLength)]
        public string Surname { get; set; }

        [Required]
        [EmailAddress]
        //[StringLength(AbpUserBase.MaxEmailAddressLength)]
        public string EmailAddress { get; set; }

        public bool IsActive { get; set; }

        public DateTime? LastLoginTime { get; set; }

        public DateTime CreationTime { get; set; }

        public List<string> RoleNames { get; set; }

        [JsonIgnore]
        public string Token { get; set; }
    }
}
