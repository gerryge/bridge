﻿using System.Collections.Generic;
using Bridge.Shared.Models;

namespace Bridge.Workflow.Users.Dto
{
    /// <summary>
    /// 
    /// </summary>
    public class SearchUserInput : BasePageInput
    {
        public string Keyword { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class SearchUserListInput : SearchUserInput
    {
        public int MaxCount { get; set; } = 100;
        public IEnumerable<long> UserIds { get; set; } = new List<long>();
    }
}
