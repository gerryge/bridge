﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;

namespace Bridge.Workflow.Users.Dto
{
   public class TenantLoginInfoDto:EntityDto
    {
        public string TenancyName { get; set; }
        public string Name { get; set; }
    }
}
