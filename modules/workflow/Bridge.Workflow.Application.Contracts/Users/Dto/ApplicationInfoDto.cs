﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridge.Workflow.Users.Dto
{
   public class ApplicationInfoDto
    {
        public string Version { get; set; }
        public DateTime ReleaseDate { get; set; }
        public Dictionary<string,bool> Features { get; set; }
    }
}
