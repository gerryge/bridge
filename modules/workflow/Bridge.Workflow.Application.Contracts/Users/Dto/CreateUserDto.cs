using System.ComponentModel.DataAnnotations;
using Volo.Abp.Auditing;

namespace Bridge.Workflow.Users.Dto
{
    //[AutoMapTo(typeof(User))]
    public class CreateUserDto
    {
        [Required]
        //[StringLength(AbpUserBase.MaxUserNameLength)]
        public string UserName { get; set; }

        [Required]
        //[StringLength(AbpUserBase.MaxNameLength)]
        public string Name { get; set; }

        [Required]
        //[StringLength(AbpUserBase.MaxSurnameLength)]
        public string Surname { get; set; }

        [Required]
        [EmailAddress]
        //[StringLength(AbpUserBase.MaxEmailAddressLength)]
        public string EmailAddress { get; set; }

        public bool IsActive { get; set; }

        public string[] RoleNames { get; set; }

        [Required]
        //[StringLength(AbpUserBase.MaxPlainPasswordLength)]
        [DisableAuditing]
        public string Password { get; set; }

        public void Normalize()
        {
            if (RoleNames == null)
            {
                RoleNames = new string[0];
            }
        }
    }
}
