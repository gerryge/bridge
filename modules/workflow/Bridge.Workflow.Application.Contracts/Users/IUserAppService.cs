﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Bridge.Shared;
using Bridge.Shared.Models;
using Bridge.Workflow.Domain;
using Bridge.Workflow.Roles.Dto;
using Bridge.Workflow.Users.Dto;
using Bridge.Workflow.Users.Request;
using Volo.Abp.Application.Services;
using Volo.Abp.Identity;

namespace Bridge.Workflow
{
    public interface IUserAppService : IApplicationService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        Task<string> Authenticate(UserLoginInfoRequest request);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<ResponseResult<UserDto>> GetAsync(string userId);

        ResponseResult<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        //Task<PageResponseResult<UserDto>> GetUsers(SearchUserInput input);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<ListResponseResult<RoleDto>> GetRoles(SearchUserInput input);

        Task<ListResponseResult<UserDto>> GetUser(SearchUserInput input);
        Task<List<IdentityUserDto>> GetApprovalUserList(WorkflowNode nextNode,PersistedWorkflow workflow);
    }
}
