﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Bridge.Workflow.Domain;
using Bridge.Workflow.Domain.Workflows;
using Bridge.Workflow.Workflows.Dtos;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace Bridge.Workflow
{
    public interface IWorkflowAppService : IApplicationService
    {
        Task<WorkflowDesignInfo> CreateAsync(WorkflowDesignInfo input);
        //Task DeleteAsync(string id);
       // Task<IEnumerable<string>> GetAllGroupAsync();
        //IEnumerable<AbpWorkflowStepBody> GetAllStepBodys();
        //Task<Dictionary<string, IEnumerable<WorkflowDesignInfo>>> GetAllWithGroupAsync(WorkflowListInput input);
        Task<WorkflowDesignInfo> GetAsync(string id);
       // Task<WorkflowDto> GetDetailsAsync(EntityDto<Guid> input);
        Task<PagedResultDto<WorkflowDesignInfo>> GetListAsync(WorkflowListInput input);
       // Task<PagedResultDto<MyWorkflowListOutput>> GetMyWorkflowAsync(WorkflowListInput input);
        //Task PublicEvent(PublishEventInput input);
        //Task StartAsync(StartWorkflowInput input);
       // Task<WorkflowDesignInfo> UpdateAsync(string id, WorkflowDesignInfo input);
    }
}