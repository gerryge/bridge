﻿using System;
using System.Threading.Tasks;
using Bridge.Shared;
using Bridge.Shared.Models;
using Bridge.Workflow.Workflow.Dto;
using Bridge.Workflow.Workflows.Dtos;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace Bridge.Workflow
{
    /// <summary>
    /// 
    /// </summary>
    public interface IWorkflowAuditService : IApplicationService
    {
        /// <summary>
        /// 修改业务数据
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<ResponseResult> EditBusinessDataAsync(EditBusinessDataInput input);
        Task<ResponseResult> AuditAsync(WorkflowAuditInput input);
        Task<ResponseResult> TerminateAsync(TerminateWorkflow input);

        /// <summary>
        /// 根据业务id进行审核
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<ResponseResult> AuditByBusinessIdAsync(WorkflowAuditByBusinessIdInput input);
        Task<ResponseResult<PagedResultDto<GetMyAuditPageListOutput>>> GetAllAsync(GetMyAuditPageListInput input);
        Task<ResponseResult<WorkflowAuditDto>> GetAuditRecordsAsync_old(string id);
        Task<PageResponseResult<WorkflowAuditRecordOutput>> GetAuditRecordsAsync(string id);
        /// <summary>
        /// 获取我发起的在途流程
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<ResponseResult<PagedResultDto<GetMyAuditPageListOutput>>> GetProcessingAsync(GetMyAuditPageListInput input);



        /// <summary>
        /// 获取单据待审核节点信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<ResponseResult<GetMyAuditPageListOutput>> GetAuditInfoAsync(GetAuditInfoInput input);
    }
}