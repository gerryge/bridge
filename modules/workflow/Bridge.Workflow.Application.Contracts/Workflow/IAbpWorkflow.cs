﻿using Bridge.Workflow.Domain;
using Bridge.Workflow.Domain.Workflows;
using Volo.Abp.Collections;
using Volo.Abp.DependencyInjection;
using WorkflowCore.Interface;

namespace Bridge.Workflow
{
    /// <summary>
    /// 
    /// </summary>
    public interface IAbpWorkflow : IWorkflow<WorkflowParamDictionary> { }

    /// <summary>
    /// 
    /// </summary>
    public interface IWorkflowConfiguration
    {
        /// <summary>
        /// 
        /// </summary>
        ITypeList<AbpWorkflowProvider> Providers { get; }
    }

    /// <summary>
    /// 
    /// </summary>
    internal class WorkflowConfiguration : IWorkflowConfiguration, ISingletonDependency
    {
        public ITypeList<AbpWorkflowProvider> Providers { get; }

        public WorkflowConfiguration()
        {
            Providers = new TypeList<AbpWorkflowProvider>();
        }
    }
}
