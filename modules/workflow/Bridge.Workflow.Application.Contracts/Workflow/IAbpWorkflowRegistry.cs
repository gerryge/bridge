﻿using System;
using Bridge.Workflow.Domain.Workflows;
using Volo.Abp;
using Volo.Abp.DependencyInjection;
using WorkflowCore.Interface;

namespace Bridge.Workflow
{
    public interface IAbpWorkflowRegistry
    {
        void RegisterWorkflow(Type type);
    }

    /// <summary>
    /// 
    /// </summary>
    public class AbpWorkflowRegistry : IAbpWorkflowRegistry, ISingletonDependency
    {
        private readonly IWorkflowRegistry _workflowRegistry;
        private readonly IServiceProvider _iocManager;

        public AbpWorkflowRegistry(IWorkflowRegistry workflowRegistry, IServiceProvider iocManager)
        {
            this._workflowRegistry = workflowRegistry;
            this._iocManager = iocManager;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>

        public void RegisterWorkflow(Type type)
        {
            var workflow = _iocManager.GetService(type);
            if (!(workflow is IAbpWorkflow))
            {
                throw new AbpException("RegistType must implement from AbpWorkflow!");
            }

            _workflowRegistry.RegisterWorkflow(workflow as IWorkflow<WorkflowParamDictionary>);
        }
    }
}
