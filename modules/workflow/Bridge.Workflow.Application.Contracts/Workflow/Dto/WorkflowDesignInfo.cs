﻿using System;
using System.Collections.Generic;
using Bridge.Workflow.Domain;
using Volo.Abp.Application.Dtos;

namespace Bridge.Workflow.Workflows.Dtos
{
    //[AutoMapFrom(typeof(PersistedWorkflowDefinition))]
    //[AutoMap(typeof(PersistedWorkflowDefinition))]
    public class WorkflowDesignInfo : EntityDto<string>
    {
        public string Color { get; set; }
        public string Group { get; set; }
        public string Icon { get; set; }
        public string Title { get; set; }
        public int Version { get; set; }
        public string Description { get; set; }
        public IEnumerable<IEnumerable<IEnumerable<WorkflowFormData>>> Inputs { get; set; }
        public IEnumerable<WorkflowNode> Nodes { get; set; }


        public WorkflowDesignInfo()
        {
            Version = 1;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        public static implicit operator PersistedWorkflowDefinition(WorkflowDesignInfo input)
        {
            if (input == null) return null;

            return new PersistedWorkflowDefinition(input.Id)
            {
                Color = input.Color,
                Title = input.Title,
                Version = input.Version,
                Description = input.Description,
                Icon = input.Icon,
                Group = input.Group,
                Inputs = input.Inputs,
                Nodes = input.Nodes,
                CreationTime = DateTime.Now,
                //IsDeleted = false,
                LastModificationTime = DateTime.Now
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        public static implicit operator WorkflowDesignInfo(PersistedWorkflowDefinition input)
        {
            if (input == null) return null;

            return new WorkflowDesignInfo
            {
                Id = input.Id,
                Color = input.Color,
                Title = input.Title,
                Version = input.Version,
                Description = input.Description,
                Icon = input.Icon,
                Group = input.Group,
                Inputs = input.Inputs,
                Nodes = input.Nodes
            };
        }
    }

    public class WorkflowDesignInfoUpdate : EntityDto<string>
    {
        public string Color { get; set; }
        public string Group { get; set; }
        public string Icon { get; set; }
        public string Title { get; set; }
        public int Version { get; set; }
        public string Description { get; set; }
        public IEnumerable<IEnumerable<IEnumerable<WorkflowFormData>>> Inputs { get; set; }
        public IEnumerable<WorkflowNode> Nodes { get; set; }
        public WorkflowDesignInfoUpdate()
        {
            Version = 1;
        }
    }
}
