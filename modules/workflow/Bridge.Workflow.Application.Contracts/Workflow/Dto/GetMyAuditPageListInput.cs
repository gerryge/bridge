﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bridge.Workflow.BaseDto;
using Volo.Abp.Application.Dtos;
using WorkflowCore.Models;

namespace Bridge.Workflow.Workflows.Dtos
{
    /// <summary>
    /// 
    /// </summary>
    public class GetMyAuditPageListInput : PagedInputDto
    {
        /// <summary>
        /// 
        /// </summary>
        public bool? AuditedMark { get; set; }

        public Guid UserId { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 流程名
        /// </summary>
        public string CurrentStepName { get; set; }

        /// <summary>
        /// 发起时间-开始
        /// </summary>
        public string CreationTimeStart { get; set; }

        /// <summary>
        /// 发起时间-结束
        /// </summary>
        public string CreationTimeEnd { get; set; }

        /// <summary>
        /// 提交人
        /// </summary>
        public string CreatorName { get; set; }
        /// <summary>
        /// 是否查全部
        /// </summary>
        public bool? IsAll { get; set; }
    }
    public class ExecutionPointerOutPut
    {
        public ExecutionPointerOutPut() { }

        public string WorkflowId { get; set; }

        public string Id { get; set; }

        public int StepId { get; set; }

        public bool Active { get; set; }

        public DateTime? SleepUntil { get; set; }

        public string PersistenceData { get; set; }

        public DateTime? StartTime { get; set; }

        public DateTime? EndTime { get; set; }

        public string EventName { get; set; }

        public string EventKey { get; set; }

        public bool EventPublished { get; set; }

        public string EventData { get; set; }

        public string StepName { get; set; }

        

        public int RetryCount { get; set; }

        public string Children { get; set; }

        public string ContextItem { get; set; }

        public string PredecessorId { get; set; }

        public string Outcome { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public PointerStatus Status { get; set; } = PointerStatus.Legacy;

        public string Scope { get; set; }
    }
    public class GetMyAuditPageListOutput : EntityDto<string>
    {
        public string WorkflowDefinitionId { get; set; }
        public string WorkflowId { get; set; }
        public int Version { get; set; }

        public int StepId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ExecutionPointerId { get
            {
                return ExecutionPointer.Id;
            } }
        public ExecutionPointerOutPut ExecutionPointer { get;set;}
        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 流程名
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 提交时间
        /// </summary>
        public DateTime CreationTime { get; set; }
        /// <summary>
        /// 提交人
        /// </summary>
        public Guid CreatorId { get; set; }
        /// <summary>
        /// 提交人
        /// </summary>
        public string CreatorName { get; set; }
        /// <summary>
        /// 当前步骤
        /// </summary>
        public string CurrentStepName { get { return ExecutionPointer.StepName; } }

        public EnumAuditStatus Status { get; set; }

        public DateTime? AuditTime { get; set; }

        /// <summary>
        /// 业务数据
        /// </summary>
        public WorkflowData BusinessData { get; set; }

        /// <summary>
        /// 是否可以审核
        /// </summary>
        public bool IsApproval { get; set; }
    
        public IEnumerable<WorkflowNode> Nodes { get; set; }
        /// <summary>
        /// 步骤可操作信息
        /// </summary>
        public IEnumerable<NodeOperate> NodeOperates => Nodes?.Where(s => s.Key == ExecutionPointer.EventKey).FirstOrDefault()?.Operate;
    }
}
