﻿using Bridge.Workflow.BaseDto;
using System.Collections.Generic;

namespace Bridge.Workflow.Workflows.Dtos
{
    /// <summary>
    /// 
    /// </summary>
    public class WorkflowListInput : PagedInputDto
    {
        public string Title { get; set; }

        /// <summary>
        /// 流程名
        /// </summary>
        public string CurrentStepName { get; set; }

        /// <summary>
        /// 发起时间-开始
        /// </summary>
        public string CreationTimeStart { get; set; }

        /// <summary>
        /// 发起时间-结束
        /// </summary>
        public string CreationTimeEnd { get; set; }

        /// <summary>
        /// 提交人
        /// </summary>
        public string CreatorName { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public List<int> Statuses { get; set; }
    }
}
