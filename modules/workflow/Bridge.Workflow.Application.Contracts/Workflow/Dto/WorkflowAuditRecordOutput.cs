﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridge.Workflow.Workflow.Dto
{
  public  class WorkflowAuditRecordOutput
    {
        public string Title { get; set; }
        public DateTime? AuditTime { get; set; }

        public string Status { get; set; }
        public string Remark { get; set; }

        public string UserIdentityName { get; set; }
    }
}
