﻿using System.Collections.Generic;

namespace Bridge.Workflow.Workflows.Dtos
{
    /// <summary>
    /// 发起工作流输入实体类
    /// </summary>
    public class StartWorkflowInput
    {
        /// <summary>
        /// 流程模版ID
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 流程模版版本号
        /// </summary>
        public int Version { get; set; }

        /// <summary>
        /// 自定义业务数据
        /// </summary>
        public Dictionary<string, object> Inputs { get; set; } = new Dictionary<string, object>();
    }
}
