﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using Volo.Abp.Application.Dtos;
using WorkflowCore.Models;

namespace Bridge.Workflow.Workflows.Dtos
{
    /// <summary>
    /// 
    /// </summary>
    public class MyWorkflowListOutput : EntityDto<string>
    {
        public string Title { get; set; }
        public int Version { get; set; }
        public DateTime CreationTime { get; set; }
        public DateTime? CompleteTime { get; set; }
        public WorkflowStatus Status { get; set; }
        public string CurrentStepName { get; set; }
        public string CurrentStepTitle
        {
            get
            {
                return Nodes.FirstOrDefault(i => i.Key == CurrentStepName)?.Title;
            }
        }
        [JsonIgnore]
        public IEnumerable<WorkflowNode> Nodes { get; set; }


        /// <summary>
        /// 提交人
        /// </summary>
        public Guid CreatorId { get; set; }
        /// <summary>
        /// 提交人
        /// </summary>
        public string CreatorName { get; set; }

        /// <summary>
        /// 业务数据
        /// </summary>
        public WorkflowData BusinessData { get; set; }

    }
}
