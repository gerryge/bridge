﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Bridge.Workflow.Workflows.Dtos
{
    public class WorkflowAuditInput
    {
      
        public string ExecutionPointerId { get; set; }
        //是否通过
        public bool Pass { get; set; }
        //意见
        public string Remark { get; set; }
        //操作名
        public string OperateName { get; set; }

        public Guid UserId { get; set; }
    }
    public class WorkflowAuditByBusinessIdInput
    {
        public string WfdefId { get; set; }
        
        public string BusinessId { get; set; }

        public bool Pass { get; set; }
        [MaxLength(500)]
        public string Remark { get; set; }


        public Guid UserId { get; set; }
    }

    public class GetAuditInfoInput
    {
        public string WfdefId { get; set; }

        public string BusinessId { get; set; }
    }


    public class TerminateWorkflow
    {
        public string WorkflowId { get; set; }

        public string Remark { get; set; }
    }
}
