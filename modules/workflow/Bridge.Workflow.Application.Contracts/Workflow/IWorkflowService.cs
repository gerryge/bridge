﻿using System.Threading.Tasks;
using Bridge.Workflow.Workflows.Dtos;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace Bridge.Workflow.Application.Contracts
{
    /// <summary>
    /// 
    /// </summary>
    public interface IWorkflowService : IApplicationService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<WorkflowDto> GetDetailsAsync(string id);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<PagedResultDto<MyWorkflowListOutput>> GetPageListAsync(WorkflowListInput input);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task PublicEvent(PublishEventInput input);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task StartAsync(StartWorkflowInput input);
    }
}