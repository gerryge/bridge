﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Bridge.Workflow.Domain;
using Bridge.Workflow.Domain.Workflows;
using Bridge.Workflow.Workflows.Dtos;
using Volo.Abp.Domain.Services;
using WorkflowCore.Models;

namespace Bridge.Workflow.Application.Contracts
{
    /// <summary>
    /// 
    /// </summary>
    public interface IAbpWorkflowManager : IDomainService
    {
        Task Initialize();
        Task<PersistedExecutionPointer> GetPersistedExecutionPointer(string id);
        Task<bool> TerminateWorkflow(string workflowId);
        IEnumerable<AbpWorkflowStepBody> GetAllStepBodys();
        Task PublishEventAsync(string eventName, string eventKey, object eventData);
        Task<PersistedWorkflowDefinition> CreateAsync(PersistedWorkflowDefinition entity);
        Task DeleteAsync(string id);
        Task<PersistedWorkflowDefinition> UpdateAsync(PersistedWorkflowDefinition entity);
        Task StartWorlflow(string id, int version, Dictionary<string, object> inputs);
        WorkflowDefinition LoadDefinition(WorkflowDesignInfo input);
    }
}
