﻿using Volo.Abp.Application.Dtos;

namespace Bridge.Workflow.BaseDto
{
    public class PagedAndSortedInputDto : PagedInputDto, ISortedResultRequest
    {
        public string Sorting { get; set; }

		
		//// custom codes 
		
        //// custom codes end

        public PagedAndSortedInputDto()
        {
            MaxResultCount = AppLtmConsts.DefaultPageSize;
        }
    }
}