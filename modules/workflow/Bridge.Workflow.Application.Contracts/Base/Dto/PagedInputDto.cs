﻿using System.ComponentModel.DataAnnotations;
using Volo.Abp.Application.Dtos;

namespace Bridge.Workflow.BaseDto
{
    /// <summary>
    /// 
    /// </summary>
    public class PagedInputDto : IPagedResultRequest
    {
        [Range(1, AppLtmConsts.MaxPageSize)]
        public int MaxResultCount { get; set; }

        [Range(0, int.MaxValue)]
        public int SkipCount { get; set; }


		
		//// custom codes 
		
        //// custom codes end


        public PagedInputDto()
        {
            MaxResultCount = AppLtmConsts.DefaultPageSize;
        }
    }
}