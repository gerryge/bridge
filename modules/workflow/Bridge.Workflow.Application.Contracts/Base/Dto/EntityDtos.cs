﻿using System.Collections.Generic;

namespace Bridge.Workflow.BaseDto
{
    public class AppLtmConsts
    {
        // TODO: 52ABP代码生成器默认生成的一些常量类 ，不影响业务逻辑点

        public const int DefaultPageSize = 10;
        public const int MaxPageSize = 1000;



        //// custom codes 

        //// custom codes end
    }

    public class EntityDtos<T>
    {
        public IEnumerable<T> Ids { get; set; }
    }
}
