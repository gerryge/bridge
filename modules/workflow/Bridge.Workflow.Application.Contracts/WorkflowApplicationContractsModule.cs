﻿using Volo.Abp.Application;
using Volo.Abp.Modularity;
using Volo.Abp.Authorization;

namespace Bridge.Workflow
{
    [DependsOn(
        typeof(WorkflowDomainSharedModule),
        typeof(AbpDddApplicationContractsModule),
        typeof(AbpAuthorizationModule)
        )]
    public class WorkflowApplicationContractsModule : AbpModule
    {

    }
}
