﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace BridgeWebSystemLib.Core.DataEntity
{
    [Serializable]
    public class BaseEntity : ICloneable
    {
        public BaseEntity()
        {
            _dataEntityState = DataEntityState.Add;
        }
        [Extended]
        public DataEntityState DataEntityState
        {
            get { return _dataEntityState; }
            set { _dataEntityState = value; }
        }
        protected DataEntityState _dataEntityState;
        [Extended]
        public string UserDataString1 { get; set; }
        [Extended]
        public string UserDataString2 { get; set; }
        [Extended]
        public object UserDataObject1 { get; set; }
        [Extended]
        public object UserDataObject2 { get; set; }

        public object Clone()
        {
            MemoryStream ms = new MemoryStream();
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(ms, this);
            ms.Seek(0, SeekOrigin.Begin);//设置当前流中的位置为指定值
            object des = bf.Deserialize(ms);
            ms.Close();
            return des;
        }
    }

    public enum DataEntityState
    {
        Add,
        Modify,
        Delete
    }
}
