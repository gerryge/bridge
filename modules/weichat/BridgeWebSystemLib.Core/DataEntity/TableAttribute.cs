﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BridgeWebSystemLib.Core.DataEntity
{
    [AttributeUsage(AttributeTargets.Class)]
    public class TableAttribute : Attribute
    {
        public TableAttribute(string tableName)
        {
            Name = tableName;
        }
        public string Name { get; set; }
    }

    [AttributeUsage(AttributeTargets.Property)]
    public class PrimaryKeyAttribute : Attribute
    {
    }

    [AttributeUsage(AttributeTargets.Property)]
    public class ComputedAttribute : Attribute
    {
    }

    [AttributeUsage(AttributeTargets.Property)]
    public class ExtendedAttribute : Attribute { }
}
