﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;

namespace BridgeWebSystemLib.Core.Util
{
    public static class DBUtil
    {
        /// <summary> 
        /// 表示无效16位整数
        /// </summary>
        public static short INVALID_INT16 = Int16.MinValue;
        /// <summary>
        /// 表示无效32位整数
        /// </summary>
        public static int INVALID_INT = Int32.MinValue;
        /// <summary>
        /// 表示无效64位整数
        /// </summary>
        public static long INVALID_INT64 = Int64.MinValue;
        /// <summary>
        /// 表示无效单精度浮点数
        /// </summary>
        public static float INVALID_FLOAT = Single.MinValue;
        /// <summary>
        /// 表示无效十进制数
        /// </summary>
        public static decimal INVALID_DECIMAL = Decimal.MinValue;
        ///<summary>
        ///表示无效时间
        ///</summary>
        public static DateTime INVALID_DATE = DateTime.MinValue;

        /// <summary>
        /// 前匹配字段
        /// </summary>
        /// <param name="sValue"></param>
        /// <returns></returns>
        public static string ToFrontLikeString(string sValue)
        {
            if (string.IsNullOrWhiteSpace(sValue))
                return sValue;
            return sValue + "%";
        }

        /// <summary>
        /// 后匹配字段
        /// </summary>
        /// <param name="sValue"></param>
        /// <returns></returns>
        public static string ToBackLikeString(string sValue)
        {
            if (string.IsNullOrWhiteSpace(sValue))
                return sValue;
            return "%" + sValue;
        }

        /// <summary>
        /// 全匹配字段
        /// </summary>
        /// <param name="sValue"></param>
        /// <returns></returns>
        public static string ToFullLikeString(string sValue)
        {
            if (string.IsNullOrWhiteSpace(sValue))
                return sValue;
            return "%" + sValue + "%";
        }

        /// <summary>
        /// 数组转换成SQL In格式字符串，例如:(1,2,3)或者('A','B','C')
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="aryObj"></param>
        /// <returns></returns>
        public static string ArrayToSQLIn<T>(T [] aryObj)
        {
            if (aryObj.Length == 0)
                return "-1";
            StringBuilder strBuf = new StringBuilder();
            foreach (object obj in aryObj)
            {
                strBuf.Append(",");
                if (obj is string)
                {
                    strBuf.Append("'").Append(obj.ToString()).Append("'");
                }
                else
                {
                    strBuf.Append(obj.ToString());
                }
            }
            return "(" + strBuf.ToString(1, strBuf.Length - 1) + ")";
        }
    }
}
