﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace BridgeWebSystemLib.Core.Util
{
    public class CryptoUtil
    {
        public static string SHA256Hash(string str)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(str);
            byte[] hash = SHA256.Create().ComputeHash(bytes);

            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                builder.Append(hash[i].ToString("x2"));
            }
            return builder.ToString();
        }
    }
}
