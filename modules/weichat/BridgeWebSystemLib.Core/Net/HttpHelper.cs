﻿using BridgeWebSystemLib.Core.Util;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace BridgeWebSystemLib.Core.Net
{
    public static class HttpHelper
    {
        internal static IHttpAccess HttpAccess
        {
            get
            {
                if (_httpAccess == null)
                    _httpAccess = new HttpClientAccess();
                return _httpAccess;
            }
        }private static IHttpAccess _httpAccess = null;

        public static string Get(string url)
        {
            return HttpAccess.Get(url);
        }

        public static T Get<T>(string url, IDictionary<string,string> dict = null)
        {
            string json = HttpAccess.Get(url, dict); 
            var obj = JsonConvert.DeserializeObject<T>(json);
            return obj;
        }

        public static byte[] GetByte(string url, IDictionary<string, string> dict = null)
        {
            return HttpAccess.GetByte(url, dict);
        }

        public static string GetByteCompression(string url, IDictionary<string, string> dict = null)
        {
            return HttpAccess.GetByteCompression(url);
        }

        public static string PostJson(string url,string content, AuthenticationHeaderValue headerValue = null)
        {
            return HttpAccess.PostJson(url, content, headerValue);
        }

        public static Task<string> PostAsyncJson(string url, string content)
        {
            return HttpAccess.PostJsonAsync(url, content);
        }
    }
}
