﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;

namespace BridgeWebSystemLib.Core.Net
{
    public class RestSharpAccess
    {
        public static string Post(string url, object jsonBody, Dictionary<string, string> headerDict = null)
        {
            var client = new RestClient(url);
            RestRequest request = new RestRequest(Method.POST);
            request.AddJsonBody(jsonBody, "application/json; charset=utf-8");
            if (headerDict != null)
            {
                foreach (KeyValuePair<string, string> pair in headerDict)
                {
                    request.AddHeader(pair.Key, pair.Value);
                }
            }
            var response = client.Execute(request);
            return response.Content;
        }
    }
}
