﻿using System.Collections.Generic;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace BridgeWebSystemLib.Core.Net
{
    public interface IHttpAccess
    {
        string Get(string url, IDictionary<string, string> paramArray = null);
        byte[] GetByte(string url, IDictionary<string, string> paramArray = null);
        string GetByteCompression(string url, IDictionary<string, string> paramArray = null);

        string PostJson(string url, string content,AuthenticationHeaderValue headerValue = null);
        Task<string> PostJsonAsync(string url, string content, AuthenticationHeaderValue headerValue = null);
    }
}
