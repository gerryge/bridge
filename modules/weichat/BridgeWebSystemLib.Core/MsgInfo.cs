﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BridgeWebSystemLib.Core
{
    public class MsgInfo
    {
        public static MsgInfo CreateMsgInfo()
        {
            MsgInfo info = new MsgInfo();
            info.State = true;
            return info;
        }

        public bool State { get; set; }

        public string Message { get; set; }

        public object Data { get; set; }

        public Exception Exception { get; set; }
    }
}
