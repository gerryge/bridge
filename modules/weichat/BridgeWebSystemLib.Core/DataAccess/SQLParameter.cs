﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BridgeWebSystemLib.Core.DataAccess
{
    /// <summary>
    /// SQL参数
    /// </summary>
    public class SQLParameter
    {
        public SQLParameter() { }

        /// <summary>
        /// SQL参数构造函数
        /// </summary>
        /// <param name="column">字段名</param>
        /// <param name="val">值</param>
        /// <param name="oper">运算符</param>
        /// <param name="paramName">参数名，可选，默认为字段名</param>
        public SQLParameter(string column, object val, 运算符 oper = 运算符.等于, string paramName = null)
        {
            ColumnName = column;
            Value = val;
            Operator = oper;
            if (paramName == null)
                ParamName = column;
            else
                ParamName = paramName;
        }

        public static SQLParameter PrepareParameter(string column, object val)
        {
            SQLParameter parameter = new SQLParameter();
            parameter.ColumnName = column;
            parameter.Operator = 运算符.等于;
            parameter.Value = val;
            parameter.ParamName = column;
            return parameter;
        }

        public static SQLParameter PrepareParameter(string column, object val, 运算符 oper, string paramName = null)
        {
            SQLParameter parameter = new SQLParameter();
            parameter.ColumnName = column;
            parameter.Operator = oper;
            parameter.Value = val;
            parameter.ParamName = paramName;
            return parameter;
        }

        /// <summary>
        /// 字段名
        /// </summary>
        public string ColumnName { get; set; }
        /// <summary>
        /// 参数名
        /// </summary>
        public string ParamName { get; set; }
        /// <summary>
        /// 运算符
        /// </summary>
        public 运算符 Operator { get; set; }
        /// <summary>
        /// 值
        /// </summary>
        public object Value { get; set; }

        public static string ConvertOperator(运算符 oper)
        {
            if (oper == 运算符.等于)
                return "=";
            else if (oper == 运算符.大于)
                return ">";
            else if (oper == 运算符.大于等于)
                return ">=";
            else if (oper == 运算符.小于)
                return "<";
            else if (oper == 运算符.小于等于)
                return "<=";
            else if (oper == 运算符.模糊)
                return "Like";
            return "=";
        }
    }

    public enum 运算符
    {
        等于 = 0,
        大于 = 1,
        大于等于 = 2,
        小于 = 3,
        小于等于 = 4,
        模糊 = 5
    }
}
