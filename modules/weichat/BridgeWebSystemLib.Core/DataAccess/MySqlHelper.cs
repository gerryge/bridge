﻿using BridgeWebSystemLib.Core.Util;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;

namespace BridgeWebSystemLib.Core.DataAccess
{
    public static class MySqlHelper
    {
        #region ExecuteNonQuery
        public static int ExecuteNonQuery(this MySqlConnection connection, string commandText, MySqlParameter[] commandParameters, MySqlTransaction transaction = null)
        {
            return ExecuteNonQuery(connection, commandText, CommandType.Text, commandParameters, transaction);
        }

        public static int ExecuteNonQuery(this MySqlConnection connection, string commandText, CommandType commandType, MySqlParameter[] commandParameters, MySqlTransaction transaction = null)
        {
            MySqlCommand command = null;
            if (transaction == null)
            {
                command = connection.CreateCommand();
                PrepareCommand(command, connection, null, commandText, commandType, commandParameters);
            }
            else
            {
                command = transaction.Connection.CreateCommand();
                PrepareCommand(command, transaction.Connection, transaction, commandText, commandType, commandParameters);
            }
            MySqlServerSqlLog(commandText, commandParameters);
            int result = command.ExecuteNonQuery();
            command.Dispose();
            return result;
        }
        #endregion

        #region ExecuteDataTable
        public static DataTable ExecuteDataTable(this MySqlConnection connection, string commandText, params MySqlParameter[] commandParameters)
        {
            return ExecuteDataTable(connection, commandText, CommandType.Text, commandParameters);
        }

        public static DataTable ExecuteDataTable(this MySqlConnection connection, string commandText, CommandType commandType, params MySqlParameter[] commandParameters)
        {
            DataTable dataTable = null;
            using (MySqlCommand command = connection.CreateCommand())
            {
                PrepareCommand(command, connection, null, commandText, commandType, commandParameters);
                MySqlServerSqlLog(commandText, commandParameters);
                using (var reader = command.ExecuteReader())
                {
                    dataTable = new DataTable();
                    dataTable.Load(reader);
                }
            }
            return dataTable;
        }
        #endregion

        #region ExecuteScalar
        public static object ExecuteScalar(this MySqlConnection connection, string commandText, params MySqlParameter[] commandParameters)
        {
            return ExecuteScalar(connection, commandText, null, CommandType.Text, commandParameters);
        }

        public static object ExecuteScalar(this MySqlConnection connection, string commandText, MySqlTransaction transaction, params MySqlParameter[] commandParameters)
        {
            return ExecuteScalar(connection, commandText, transaction, CommandType.Text, commandParameters);
        }

        public static object ExecuteScalar(this MySqlConnection connection, string commandText, MySqlTransaction transaction, CommandType commandType, params MySqlParameter[] commandParameters)
        {
            object result = null;
            using (MySqlCommand command = connection.CreateCommand())
            {
                PrepareCommand(command, connection, transaction, commandText, commandType, commandParameters);
                MySqlServerSqlLog(commandText, commandParameters);
                result = command.ExecuteScalar();
            }
            return result;
        }
        #endregion

        #region ExecuteReader
        public static IDataReader ExecuteReader(this MySqlConnection connection, string commandText, MySqlTransaction transaction, params MySqlParameter[] commandParameters)
        {
            return ExecuteReader(connection, commandText, CommandType.Text, transaction, commandParameters);
        }

        public static IDataReader ExecuteReader(this MySqlConnection connection, string commandText, CommandType commandType, MySqlTransaction transaction, params MySqlParameter[] commandParameters)
        {
            IDataReader reader = null;
            using (MySqlCommand command = connection.CreateCommand())
            {
                PrepareCommand(command, connection, transaction, commandText, commandType, commandParameters);
                MySqlServerSqlLog(commandText, commandParameters);
                reader = command.ExecuteReader();
            }
            return reader;
        }
        #endregion

        #region PrivateMethod
        private static void PrepareCommand(MySqlCommand command, MySqlConnection connection, MySqlTransaction transaction, string commandText, CommandType commandType, params MySqlParameter[] commandParameters)
        {
            if (connection.State != ConnectionState.Open)
            {
                connection.Open();
            }
            command.Connection = connection;
            command.CommandText = commandText;
            command.CommandType = commandType;
            command.Transaction = transaction;
            if (commandParameters != null)
            {
                AttachParameters(command, commandParameters);
            }
        }

        private static void AttachParameters(MySqlCommand command, MySqlParameter[] commandParameters)
        {
            foreach (MySqlParameter parameter in commandParameters)
            {
                if (parameter.Direction == ParameterDirection.Input)
                {
                    if (parameter.MySqlDbType == MySqlDbType.Int32 || parameter.MySqlDbType == MySqlDbType.Int16 || parameter.MySqlDbType == MySqlDbType.Int64)
                    {
                        if (parameter.Value == null || (int)parameter.Value == DBUtil.INVALID_INT)
                        {
                            parameter.Value = DBNull.Value;
                        }
                    }
                    else if (parameter.MySqlDbType == MySqlDbType.VarChar || parameter.MySqlDbType == MySqlDbType.VarString)
                    {
                        if (parameter.Value == null || parameter.Value.ToString() == "")
                        {
                            parameter.Value = DBNull.Value;
                        }
                    }
                    else if (parameter.MySqlDbType == MySqlDbType.Decimal)
                    {
                        if (parameter.Value == null || (decimal)parameter.Value == DBUtil.INVALID_DECIMAL)
                        {
                            parameter.Value = DBNull.Value;
                        }
                    }
                    else if (parameter.MySqlDbType == MySqlDbType.Date || parameter.MySqlDbType == MySqlDbType.DateTime)
                    {
                        if (parameter.Value == null || (DateTime)parameter.Value == DBUtil.INVALID_DATE)
                        {
                            parameter.Value = DBNull.Value;
                        }
                    }
                }
                command.Parameters.Add(parameter);
            }
        }

        private static void MySqlServerSqlLog(string sSql, MySqlParameter[] commandParameters)
        {
            string sSqllog = sSql;
            if (commandParameters != null)
            {
                foreach (var param in commandParameters)
                {
                    string value = param.Value.ToString();
                    if (param.Value != DBNull.Value)
                    {
                        if (param.MySqlDbType == MySqlDbType.VarChar)
                        {
                            value = "'" + param.Value + "'";
                        }
                        else if (param.MySqlDbType == MySqlDbType.Date)
                        {
                            value = ((DateTime)param.Value).ToString("yyyy-MM-dd HH:mm:ss");
                        }
                        else if (param.MySqlDbType == MySqlDbType.Int32)
                        {
                            value = ((int)param.Value).ToString();
                        }
                        else if (param.MySqlDbType == MySqlDbType.Decimal)
                        {
                            value = ((decimal)param.Value).ToString();
                        }
                    }
                    else
                    {
                        value = "''";
                    }
                    //Regex r = new Regex(param.ParameterName);
                    //sSqllog = r.Replace(sSqllog, value, 1);
                    sSqllog = sSqllog.Replace(param.ParameterName, value);
                }
            }
            LogUtil.WriteLog(AppDomain.CurrentDomain.BaseDirectory + "\\MySQLlog", DateTime.Now.ToString("yyyy-MM-dd"), DateTime.Now.ToString("HH:mm:ss") + "  " + sSqllog);
        }
        #endregion
    }
}
