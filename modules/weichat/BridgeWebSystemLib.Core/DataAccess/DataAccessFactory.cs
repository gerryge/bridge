﻿using BridgeWebSystemLib.Core.Util;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace BridgeWebSystemLib.Core.DataAccess
{
    public class DataAccessFactory
    {
        public static IDataAccess CreateDataAccess(string connectionKey, string connectionString)
        {
            if (connectionKey == "Oracle")
                return CreateOracleDataAccess(connectionString);
            else if(connectionKey == "SQLServer")
                return CreateSQLServerDataAccess(connectionString);
            return null;
        }

        public static IDataAccess CreateOracleDataAccess(string connectionString)
        {
            return null;
        }

        public static IDataAccess CreateSQLServerDataAccess(string connectionString)
        {
            return new SQLServerInstance(connectionString);
        }

        public static IDataAccess CreateMySqlDataAccess(string connectionString)
        {
            return new MySqlInstance(connectionString);
        }

    }
}
