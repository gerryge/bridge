﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace BridgeWebSystemLib.Core.DataAccess
{
    public interface IDataAccess : IDisposable
    {
        IDbConnection GetConnection();
        void CloseConnection();
        DateTime GetServerDateTime();
        string ConnectionString { get; }

        int GetSequences<T>(IDbTransaction transaction = null) where T : class;
        /// <summary>
        /// 获取某一个值
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        dynamic GetScalar(string sql, object param = null, IDbTransaction transaction = null);

        #region SELECT
        /// <summary>
        /// 获取数据对象
        /// </summary>
        /// <typeparam name="T">实体类型</typeparam>
        /// <param name="id">主键</param>
        /// <returns></returns>
        T Get<T>(dynamic id, IDbTransaction transaction = null) where T : class;
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="param"></param>
        /// <returns></returns>
        T GetByParam<T>(object param, IDbTransaction transaction = null) where T : class;
        /// <summary>
        /// 获取数据对象
        /// </summary>
        /// <typeparam name="T">实体类型</typeparam>
        /// <param name="sql">SQL语句</param>
        /// <param name="param">条件参数</param>
        /// <returns></returns>
        T Get<T>(string sql, object param = null, IDbTransaction transaction = null) where T : class;
        /// <summary>
        /// 获取数据列表
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        List<T> Query<T>(IDbTransaction transaction = null) where T : class;
        /// <summary>
        /// 获取数据列表
        /// </summary>
        /// <typeparam name="T">实体类型</typeparam>
        /// <param name="param">条件参数</param>
        /// <returns></returns>
        List<T> QueryByParam<T>(object param, IDbTransaction transaction = null) where T : class;
        /// <summary>
        /// 获取数据列表
        /// </summary>
        /// <typeparam name="T">实体类型</typeparam>
        /// <param name="sql">SQL语句</param>
        /// <param name="param">条件参数</param>
        /// <returns></returns>
        List<T> Query<T>(string sql, object param = null, IDbTransaction transaction = null) where T : class;
        /// <summary>
        /// 获取DataTable
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        DataTable Query(string sql, object param = null, IDbTransaction transaction = null);
        #endregion

        /// <summary>
        /// 数据写入
        /// </summary>
        /// <typeparam name="T">实体类型</typeparam>
        /// <param name="entity">实体对象</param>
        /// <param name="transaction">数据库事务</param>
        /// <returns></returns>
        int Insert<T>(T entity, IDbTransaction transaction = null) where T : class;

        /// <summary>
        /// 数据修改
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entity"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        int Update<T>(T entity, IDbTransaction transaction = null) where T : class;
        /// <summary>
        /// 数据删除
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entity"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        int Delete<T>(T entity, IDbTransaction transaction = null) where T : class;
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        int Delete<T>(dynamic id, IDbTransaction transaction = null) where T : class;
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entity"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        int DeleteByParam<T>(object param, IDbTransaction transaction = null) where T : class;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="param"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        int Execute(string sql, object param = null, IDbTransaction transaction = null);
    }
}
