﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BridgeWeChat.Model
{
    public class BaseResponse
    {
        public string ResultCode { get; set; }
        public string ResultMessage { get; set; }
        public BaseResponse()
        {
            ResultCode = "0";
            ResultMessage = "";
        }
    }
}
