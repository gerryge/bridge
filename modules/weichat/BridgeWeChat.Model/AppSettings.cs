﻿using System;

namespace BridgeWeChat.Model
{
    public class AppSettings
    {
        public ConnectionStrings ConnectionStrings { get; set; }
        //public ApiAuthentication ApiAuthentication { get; set; }
        public WeChatOAConfig WeChatOAConfig { get; set; }
        public AliYunSMS AliYunSMS { get; set; }
        public GeTui GeTui { get; set; }
    }

    public class ConnectionStrings
    {
        public string JZTHISConnection { get; set; }
    }

    public class ApiAuthentication
    {
        public string Issuer { get; set; }
        public string Audience { get; set; }
        public string SigningKey { get; set; }
        public int ExpiresSeconds { get; set; }
    }

    public class WeChatOAConfig
    {
        public string AppID { get; set; }
        public string AppSecret { get; set; }
        public string Token { get; set; }
        public string EncodingAESKey { get; set; }
        //关注模板ID
        public string GZTemplateId { get; set; }
        //扫码模板ID
        public string SMTemplateId { get; set; }
        //证照到期提醒模板ID
        public string ZZDQTemplateId { get; set; }
        //订单生成通知模板ID
        public string DDSCTemplateId { get; set; }
        //订单确认通知
        public string DDQRTemplateId { get; set; }
        //单据审核结果通知
        public string DDSHTemplateId { get; set; }
        //订单配送提醒
        public string DDPSTemplateId { get; set; }
    }

    public class AliYunSMS
    {
        public string AccessKeyID { get; set; }
        public string AccessKeySecret { get; set; }
    }

    public class GeTui
    {
        public string appId { get; set; }
        public string appKey { get; set; }
        public string appSecret { get; set; }
        public string masterSecret { get; set; }
    }
}
