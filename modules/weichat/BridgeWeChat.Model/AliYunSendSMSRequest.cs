﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BridgeWeChat.Model
{
    public class AliYunSendSMSRequest
    {
        [NotNull]
        public string OrganId { get; set; }
        [NotNull]
        public string PhoneNumbers { get; set; }
        [NotNull]
        public string SignName { get; set; }
        [NotNull]
        public string TemplateCode { get; set; }
        [NotNull]
        public string TemplateParam { get; set; }
    }
}
