﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BridgeWeChat.Model
{
    public class WeChatTemplateMessageRequest
    {
        [NotNull]
        public string OrganId { get; set; }
        public string UserId { get; set; }
        public string OpenId { get; set; }
        public string CallbackUrl { get; set; }
        [NotNull]
        public int TemplateType { get; set; }
        [NotNull]
        public string MessageId { get; set; }

        /// <summary>
        /// 模板id,优化匹配此字段，没有值在匹配TemplateType
        /// </summary>
        public string TemplateId { get; set; }
        public string First { get; set; }
        public string Keyword1 { get; set; }
        public string Keyword2 { get; set; }
        public string Keyword3 { get; set; }
        public string Keyword4 { get; set; }

        public string Keyword5 { get; set; }
        public string Remark { get; set; }

    }
}
