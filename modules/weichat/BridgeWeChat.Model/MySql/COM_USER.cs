﻿using BridgeWebSystemLib.Core.DataEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BridgeWeChat.Model
{
    public class COM_USER : BaseEntity
    {
		[Key]
		public int REC_ID
		{
			get { return _REC_ID; }
			set { _REC_ID = value; }
		}
		protected int _REC_ID = int.MinValue;
		public string ORGAN_ID
		{
			get { return _ORGAN_ID; }
			set { _ORGAN_ID = value; }
		}
		protected string _ORGAN_ID = null;
		public string USER_ID
		{
			get { return _USER_ID; }
			set { _USER_ID = value; }
		}
		protected string _USER_ID = null;
		public string OPEN_ID
		{
			get { return _OPEN_ID; }
			set { _OPEN_ID = value; }
		}
		protected string _OPEN_ID = null;
		public int PM_TYPE
		{
			get { return _PM_TYPE; }
			set { _PM_TYPE = value; }
		}
		protected int _PM_TYPE = int.MinValue;
		public DateTime OPER_DATE
		{
			get { return _OPER_DATE; }
			set { _OPER_DATE = value; }
		}
		protected DateTime _OPER_DATE = DateTime.MinValue;
	}
}
