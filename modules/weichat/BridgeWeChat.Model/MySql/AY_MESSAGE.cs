﻿using BridgeWebSystemLib.Core.DataEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BridgeWeChat.Model
{
    public class AY_MESSAGE : BaseEntity
    {
		[Key]
		public int REC_ID { get; set; }
		public string ORGAN_ID { get; set; }
		public string PHONE_NUMBERS { get; set; }
		public string SIGN_NAME { get; set; }
		public string TEMPLATE_CODE { get; set; }
		public string TEMPLATE_MESSAGE { get; set; }
		public string CODE { get; set; }
		public string REQUEST_ID { get; set; }
		public string BIZ_ID { get; set; }
		public DateTime OPER_DATE { get; set; }

	}
}
