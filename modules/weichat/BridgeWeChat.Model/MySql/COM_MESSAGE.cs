﻿using BridgeWebSystemLib.Core.DataEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BridgeWeChat.Model
{
    public class COM_MESSAGE : BaseEntity
    {
		[Key]
		public int REC_ID
		{
			get { return _REC_ID; }
			set { _REC_ID = value; }
		}
		protected int _REC_ID = int.MinValue;
		public int MSG_TYPE
		{
			get { return _MSG_TYPE; }
			set { _MSG_TYPE = value; }
		}
		protected int _MSG_TYPE = int.MinValue;
		public string ORGAN_ID
		{
			get { return _ORGAN_ID; }
			set { _ORGAN_ID = value; }
		}
		protected string _ORGAN_ID = null;
		public string USER_ID
		{
			get { return _USER_ID; }
			set { _USER_ID = value; }
		}
		protected string _USER_ID = null;
		public string OPEN_ID
		{
			get { return _OPEN_ID; }
			set { _OPEN_ID = value; }
		}
		protected string _OPEN_ID = null;
		public int TEMPLATE_TYPE
		{
			get { return _TEMPLATE_TYPE; }
			set { _TEMPLATE_TYPE = value; }
		}
		protected int _TEMPLATE_TYPE = int.MinValue;
		public string CALLBACK_URL
		{
			get { return _CALLBACK_URL; }
			set { _CALLBACK_URL = value; }
		}
		protected string _CALLBACK_URL = null;
		public string HPD_MSG_ID
		{
			get { return _HPD_MSG_ID; }
			set { _HPD_MSG_ID = value; }
		}
		protected string _HPD_MSG_ID = null;
		public string WX_MSG_ID
		{
			get { return _WX_MSG_ID; }
			set { _WX_MSG_ID = value; }
		}
		protected string _WX_MSG_ID = null;
		public string MSG_STATE
		{
			get { return _MSG_STATE; }
			set { _MSG_STATE = value; }
		}
		protected string _MSG_STATE = null;
		public DateTime OPER_DATE
		{
			get { return _OPER_DATE; }
			set { _OPER_DATE = value; }
		}
		protected DateTime _OPER_DATE = DateTime.MinValue;
	}
}
