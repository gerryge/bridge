﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BridgeWeChat.Model
{
    public class WeChatQRCodeRequest
    {
        [NotNull]
        public string OrganId { get; set; }
        [NotNull]
        public string UserId { get; set; }
        public string CallbackUrl { get; set; }
    }
}
