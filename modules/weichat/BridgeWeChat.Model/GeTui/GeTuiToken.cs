﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BridgeWeChat.Model
{
    public class GeTuiToken
    {
        public string sign { get; set; }
        public string timestamp { get; set; }
        public string appkey { get; set; }
    }
}
