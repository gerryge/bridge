﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BridgeWeChat.Model
{
    public class TokenResponse : PublicResponse
    {
        public Tokendata data { get; set; }
    }

    public class Tokendata
    {
        public string expire_time { get; set; }
        public string token { get; set; }
    }
}
