﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace BridgeWeChat.Model
{
    public class SinglePushResponse : PublicResponse
    {
        public SinglePushData data { set; get; }

        public SinglePushResponse()
        {
            data = new SinglePushData();
        }
    }

    public class SinglePushData
    {
        public string taskid { get; set; }
        public string state { get; set; }
    }
}
