﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BridgeWeChat.Model
{
    public class GeTuiTemplateMessageResponse : PublicResponse
    {
        public TemplateMessageData data { get; set; }
    }

    public class TemplateMessageData
    {
        public string taskid { get; set; }
    }
}
