﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BridgeWeChat.Model
{
    public class ListPushRequest
    {
        public Audience audience { get; set; }
        public string taskid { get; set; }
        public bool is_async { get; set; }

        public ListPushRequest()
        {
            audience = new Audience();
        }
    }
}
