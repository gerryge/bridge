﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BridgeWeChat.Model
{
    public class SinglePushRequest
    {
        public string request_id { get; set; }
        public Settings settings { get; set; }
        public Audience audience { get; set; }
        public Push_message push_message { get; set; }

        public SinglePushRequest()
        {
            settings = new Settings();
            audience = new Audience();
            push_message = new Push_message();
        }
    }

    public class Settings
    {
        public int ttl 
        {
            get { return _ttl; }
            set { _ttl = value; }
        }private int _ttl = 3600000;
    }

    public class Audience
    {
        public List<string> cid { get; set; }
    }

    public class Notification
    {
        public string title { get; set; }
        public string body { get; set; }
        public string click_type { get; set; }
        public string url { get; set; }
    }

    public class Push_message
    {
        public Notification notification { get; set; }
    }

}
