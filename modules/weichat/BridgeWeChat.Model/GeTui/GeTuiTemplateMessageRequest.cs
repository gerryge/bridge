﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BridgeWeChat.Model
{
    public class GeTuiTemplateMessageRequest
    {
        public string request_id { get; set; }
        public string group_name { get; set; }
        public Settings settings { get; set; }
        public Push_message push_message { get; set; }

        public GeTuiTemplateMessageRequest()
        {
            settings = new Settings();
            push_message = new Push_message();
        }
    }
}
