﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace BridgeWeChat.Model
{
    public class TemplateMessageModel
    {
        public string touser { get; set; }
        public string template_id { get; set; }
        public string url { get; set; }
        public Data data { get; set; }
    }

    public class Data
    {
        /// <summary>
        /// 
        /// </summary>
        public First first { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Keyword1 keyword1 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Keyword2 keyword2 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Keyword3 keyword3 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Keyword4 keyword4 { get; set; }

        public Keyword5 keyword5 { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public Remark remark { get; set; }
    }

    public class First
    {
        /// <summary>
        /// 
        /// </summary>
        public string value { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string color { get; set; }
    }

    public class Keyword1
    {
        /// <summary>
        /// 
        /// </summary>
        public string value { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string color { get; set; }
    }

    public class Keyword2
    {
        /// <summary>
        /// 
        /// </summary>
        public string value { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string color { get; set; }
    }

    public class Keyword3
    {
        /// <summary>
        /// 
        /// </summary>
        public string value { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string color { get; set; }
    }

    public class Keyword4
    {
        /// <summary>
        /// 
        /// </summary>
        public string value { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string color { get; set; }
    }
    public class Keyword5
    {
        /// <summary>
        /// 
        /// </summary>
        public string value { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string color { get; set; }
    }

    public class Remark
    {
        /// <summary>
        /// 
        /// </summary>
        public string value { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string color { get; set; }
    }
}
