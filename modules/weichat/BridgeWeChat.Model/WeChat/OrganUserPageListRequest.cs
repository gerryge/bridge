﻿namespace BridgeWeChat.Model.WeChat
{
    public class OrganUserPageListRequest
    {
        [NotNull]
        public string OrganId { get; set; }

        /// <summary>
        /// 每页显示个数
        /// </summary>
        public int PageSize { get; set; } = 100;

        /// <summary>
        /// 当前第几页
        /// </summary>
        public int PageIndex { get; set; } = 0;
    }
}
