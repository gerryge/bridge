﻿using System.Collections.Generic;

namespace BridgeWeChat.Model.WeChat
{
    public class OrganUserPageListRespone : BaseResponse
    {
       
        public List<COM_USER> Users { get; set; } = new List<COM_USER>();

        /// <summary>
        /// 每页显示个数
        /// </summary>
        public int PageSize { get; set; } = 0;

        /// <summary>
        /// 当前第几页
        /// </summary>
        public int PageIndex { get; set; } = 0;
        /// <summary>
        /// 当前页条数
        /// </summary>
        public int PageCount { get; set; } = 0;
        /// <summary>
        /// 总条数
        /// </summary>
        public int TotalCount { get; set; } = 0;
    }

}
