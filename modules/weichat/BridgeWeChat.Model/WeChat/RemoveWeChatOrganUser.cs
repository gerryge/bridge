﻿namespace BridgeWeChat.Model.WeChat
{
    public class RemoveWeChatOrganUser
    {
        [NotNull]
        public string OrganId { get; set; }

        [NotNull]
        public string UserId { get; set; }
    }
}
