﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BridgeWeChat.Model
{
    public class TemplateMessageResponse
    {
        public string errcode { get; set; }
        public string errmsg { get; set; }
        public string msgid { get; set; }
    }
}
