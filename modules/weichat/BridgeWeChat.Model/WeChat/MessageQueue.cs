﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BridgeWeChat.Model
{
    public class MessageQueue
    {
        public string MsgId;
        public string FromUser;
        public DateTime CreateTime;
    }
}
