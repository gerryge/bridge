﻿using BridgeWebSystemLib.Core.DataEntity;
using System;
using System.Collections.Generic;
using System.Text;

namespace BridgeWeChat.Model.WeChat
{
    public class OrganUserCountRespone : BaseResponse
    {
        public List<OrganUserCount> OrganUsers { get; set; }
    }

    public class OrganUserCount : BaseEntity
    {

        public string OrganId { get; set; } 

        public int Count { get; set; }
    }
}
