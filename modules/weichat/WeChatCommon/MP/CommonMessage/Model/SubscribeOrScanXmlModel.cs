﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace WeChatCommon.MP.CommonMessage
{
    [XmlRoot(ElementName = "xml")]
    public class SubscribeOrScanXmlModel : BaseRequestXml
    {
        public string Event { get; set; }
        public string EventKey { get; set; }
        public string Ticket { get; set; }
    }
}
