﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace WeChatCommon.MP.CommonMessage
{
    public class BaseRequestXml
    {
        public string ToUserName { get; set; }
        public string FromUserName { get; set; } 
        public string CreateTime { get; set; }
        public string MsgType { get; set; }
    }
}
