﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace WeChatCommon.MP.CommonMessage
{
    [XmlRoot(ElementName = "xml")]
    public class TemplateSendResultXmlModel : BaseRequestXml
    {
        public string Event { get; set; }
        public string MsgID { get; set; }
        public string Status { get; set; }
    }
}
