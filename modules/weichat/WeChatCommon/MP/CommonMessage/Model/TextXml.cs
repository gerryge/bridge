﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace WeChatCommon.MP.CommonMessage
{
    public class TextXml : BaseResponseXml
    {
        public string Content { get; set; }

        public override string ToXml()
        {
            return "<xml><ToUserName><![CDATA["+this.ToUserName+"]]></ToUserName><FromUserName><![CDATA["+this.FromUserName+"]]></FromUserName><CreateTime>"+this.CreateTime+"</CreateTime><MsgType><![CDATA[text]]></MsgType><Content><![CDATA["+Content+"]]></Content></xml>";
        }
    }
}
