﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace WeChatCommon.MP.CommonMessage
{
    [XmlRoot(ElementName = "xml")]
    public abstract class BaseResponseXml
    {
        public string ToUserName { get; set; }
        public string FromUserName { get; set; }
        public long CreateTime { get; set; }
        public string MsgType { get; set; }

        public abstract string ToXml();
    }
}
