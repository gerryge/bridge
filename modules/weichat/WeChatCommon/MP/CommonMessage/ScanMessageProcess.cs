﻿using BridgeWeChat.Constant;
using BridgeWeChat.Model;
using BridgeWebSystemLib.Core.DataAccess;
using BridgeWebSystemLib.Core.Util;
using System;
using System.Collections.Generic;
using System.Text;
using WeChatCommon.BizLogic;

namespace WeChatCommon.MP.CommonMessage
{
    public class ScanMessageProcess : ICommonMessageProcess
    {
        private SubscribeOrScanXmlModel _scanXmlModel;

        public ScanMessageProcess(string xmlString)
        {
            _scanXmlModel = XmlUtil.XmlDeserialize<SubscribeOrScanXmlModel>(xmlString);
        }

        public string ExecuteResult()
        {
            var qrCodeParam = JsonUtil.JsonDeserialize<WeChatQRCodeRequest>(_scanXmlModel.EventKey);

            string content = "";
            var msgInfo = ComUserUtil.SaveUserInfo(_scanXmlModel, qrCodeParam);
            if (msgInfo.State)
            {
                //content = "扫码成功！场景值：" + _scanXmlModel.EventKey + "，OPEN_ID:" + _scanXmlModel.FromUserName;
                content = "扫码成功！我们将及时为您推送订单，证照等消息。";
            }
            else
            {
                content = "扫码失败！场景值：" + _scanXmlModel.EventKey + "  " + msgInfo.Message;
            }

            TextXml xml = new TextXml();
            xml.FromUserName = _scanXmlModel.ToUserName;
            xml.ToUserName = _scanXmlModel.FromUserName;
            xml.CreateTime = DateTime.Now.Ticks;
            xml.Content = content;
            string xmlString = xml.ToXml();
            return xmlString;
        }
    }
}
