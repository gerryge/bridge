﻿using BridgeWeChat.Constant;
using BridgeWeChat.Model;
using BridgeWebSystemLib.Core.DataAccess;
using BridgeWebSystemLib.Core.Util;
using System;
using System.Collections.Generic;
using System.Text;
using WeChatCommon.BizLogic;

namespace WeChatCommon.MP.CommonMessage
{
    /// <summary>
    /// 用户扫码关注消息处理
    /// </summary>
    public class SubscribeMessageProcess : ICommonMessageProcess
    {
        private SubscribeOrScanXmlModel _scanXmlModel;

        public SubscribeMessageProcess(string xmlString)
        {
            _scanXmlModel = XmlUtil.XmlDeserialize<SubscribeOrScanXmlModel>(xmlString);
        }

        public string ExecuteResult()
        {
            if (_scanXmlModel.EventKey.StartsWith("qrscene_"))
            {
                _scanXmlModel.EventKey = _scanXmlModel.EventKey.Substring(8);
            }
            var qrCodeParam = JsonUtil.JsonDeserialize<WeChatQRCodeRequest>(_scanXmlModel.EventKey);
            string content = "";
            var msgInfo = ComUserUtil.SaveUserInfo(_scanXmlModel, qrCodeParam);
            if (msgInfo.State)
            {
                //content = "关注成功！场景值：" + _scanXmlModel.EventKey;

                content = "感谢您的关注！我们将及时为您推送订单，证照等消息。";
            }
            else
            {
                content = "关注失败！场景值：" + _scanXmlModel.EventKey + "  " + msgInfo.Message;
            }
            TextXml xml = new TextXml();
            xml.FromUserName = _scanXmlModel.ToUserName;
            xml.ToUserName = _scanXmlModel.FromUserName;
            xml.CreateTime = DateTime.Now.Ticks;
            xml.Content = content;
            string xmlString = xml.ToXml();
            return xmlString;
        }
    }
}
