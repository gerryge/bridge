﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WeChatCommon.MP.CommonMessage
{
    public interface ICommonMessageProcess
    {
        string ExecuteResult();
    }
}
