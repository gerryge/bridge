﻿using BridgeWebSystemLib.Core.DataAccess;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace WeChatCommon.MP.CommonMessage
{
    public class CommonMessageFactory
    {
        public static ICommonMessageProcess CreateMessageProcess(XmlDocument document)
        {
            if (document == null)
                return null;
            ICommonMessageProcess messageProcess = null;
            XmlElement rootElement = document.DocumentElement;
            string msgType = rootElement.SelectSingleNode("MsgType").InnerText;
            switch (msgType)
            {
                case "text":
                    messageProcess = new TextMessageProcess();
                    break;
                case "image":
                    break;
                case "voice":
                    break;
                case "video":
                    break;
                case "shortvideo":
                    break;
                case "location":
                    break;
                case "link":
                    break;
                case "event":
                    switch (rootElement.SelectSingleNode("Event").InnerText)
                    {
                        case "subscribe":
                            messageProcess = new SubscribeMessageProcess(document.InnerXml);
                            break;
                        case "unsubscribe":
                            break;
                        case "SCAN":
                            messageProcess = new ScanMessageProcess(document.InnerXml);
                            break;
                        case "LOCATION":
                            break;
                        case "CLICK":
                            break;
                        case "VIEW":
                            break;
                        case "TEMPLATESENDJOBFINISH":
                            messageProcess = new TemplateSendResultMessageProcess(document.InnerXml);
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    //result = "没有识别的类型消息：" + xmlElement.SelectSingleNode("MsgType").InnerText;
                    //WriteLog(result);
                    break;
             }
            return messageProcess;
        }
    }
}
