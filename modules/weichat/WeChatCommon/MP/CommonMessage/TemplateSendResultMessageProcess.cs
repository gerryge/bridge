﻿using BridgeWebSystemLib.Core.Util;
using System;
using System.Collections.Generic;
using System.Text;
using WeChatCommon.BizLogic;

namespace WeChatCommon.MP.CommonMessage
{
    public class TemplateSendResultMessageProcess : ICommonMessageProcess
    {
        TemplateSendResultXmlModel _xmlModel = null;
        public TemplateSendResultMessageProcess(string xmlString)
        {
            _xmlModel = XmlUtil.XmlDeserialize<TemplateSendResultXmlModel>(xmlString);
        }

        public string ExecuteResult()
        {
            var msgInfo = ComMessageUtil.UpdateWeChatMPMessageState(_xmlModel.MsgID, _xmlModel.Status);
            //调用通知接口

            if (_xmlModel.Status == "success")
            {
                return "OK";
            }
            else 
            {
                
            }
            return "NO";
        }
    }
}
