﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WeChatCommon.MP.TemplateMessage
{
    public interface ITemplateMessageProcess
    {
        string PostData();
        
    }
}
