﻿using BridgeWeChat.Constant;
using BridgeWeChat.Model;
using BridgeWebSystemLib.Core;
using BridgeWebSystemLib.Core.DataAccess;
using System;
using System.Collections.Generic;
using System.Text;
using WeChatCommon.MP.CommonMessage;

namespace WeChatCommon.BizLogic
{
    public class ComUserUtil
    {
        public static MsgInfo SaveUserInfo(SubscribeOrScanXmlModel xmlModel,WeChatQRCodeRequest qrCodeParam)
        {
            MsgInfo msgInfo = MsgInfo.CreateMsgInfo();
            try
            {
                using (IDataAccess dataAccess = BridgeHttpContext.DataAccess)
                {
                    var user = dataAccess.GetByParam<COM_USER>(new { ORGAN_ID = qrCodeParam.OrganId, USER_ID = qrCodeParam.UserId });
                    if (user == null)
                    {
                        user = new COM_USER();
                        user.ORGAN_ID = qrCodeParam.OrganId;
                        user.USER_ID = qrCodeParam.UserId;
                        user.OPEN_ID = xmlModel.FromUserName;
                        user.PM_TYPE = (int)ComEnum.平台类型.微信公众号;
                        user.OPER_DATE = DateTime.Now;
                        dataAccess.Insert(user);
                    }
                    else
                    {
                        user.OPEN_ID = xmlModel.FromUserName;
                        user.OPER_DATE = DateTime.Now;
                        dataAccess.Update(user);
                    }
                }
            }
            catch (Exception ex)
            {
                msgInfo.State = false;
                msgInfo.Message = ex.Message;
                msgInfo.Exception = ex;
            }
            return msgInfo;
        }
    }
}
