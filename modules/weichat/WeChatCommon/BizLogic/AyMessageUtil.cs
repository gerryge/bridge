﻿using BridgeWeChat.Model;
using BridgeWebSystemLib.Core;
using BridgeWebSystemLib.Core.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace WeChatCommon.BizLogic
{
    public class AyMessageUtil
    {
        public static MsgInfo SaveAliYunMessage(AliYunSendSMSRequest messageRequest, SendSMSResponse response)
        {
            MsgInfo msgInfo = MsgInfo.CreateMsgInfo();
            try
            {
                using (IDataAccess dataAccess = BridgeHttpContext.DataAccess)
                {
                    var message = new AY_MESSAGE();
                    message.ORGAN_ID = messageRequest.OrganId;
                    message.PHONE_NUMBERS = messageRequest.PhoneNumbers;
                    message.SIGN_NAME = messageRequest.SignName;
                    message.TEMPLATE_CODE = messageRequest.TemplateCode;
                    message.TEMPLATE_MESSAGE = messageRequest.TemplateParam;
                    message.CODE = response.Code;
                    message.REQUEST_ID = response.RequestId;
                    message.BIZ_ID = response.BizId;
                    message.OPER_DATE = DateTime.Now;
                    dataAccess.Insert(message);
                }
            }
            catch (Exception ex)
            {
                msgInfo.State = false;
                msgInfo.Message = ex.Message;
                msgInfo.Exception = ex;
            }
            return msgInfo;
        }

        public static MsgInfo BatchSaveAliYunMessage(List<AliYunSendSMSRequest> messageRequestList, SendSMSResponse response)
        {
            MsgInfo msgInfo = MsgInfo.CreateMsgInfo();
            
            using (IDataAccess dataAccess = BridgeHttpContext.DataAccess)
            {
                IDbConnection conn = dataAccess.GetConnection();
                IDbTransaction trans = conn.BeginTransaction();
                try
                {
                    foreach (var messageRequest in messageRequestList)
                    {
                        var message = new AY_MESSAGE();
                        message.ORGAN_ID = messageRequest.OrganId;
                        message.PHONE_NUMBERS = messageRequest.PhoneNumbers;
                        message.SIGN_NAME = messageRequest.SignName;
                        message.TEMPLATE_CODE = messageRequest.TemplateCode;
                        message.TEMPLATE_MESSAGE = messageRequest.TemplateParam;
                        message.CODE = response.Code;
                        message.REQUEST_ID = response.RequestId;
                        message.BIZ_ID = response.BizId;
                        message.OPER_DATE = DateTime.Now;
                        dataAccess.Insert(message, trans);
                    }
                    trans.Commit();
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    msgInfo.State = false;
                    msgInfo.Message = ex.Message;
                    msgInfo.Exception = ex;
                }
            }
            return msgInfo;
        }
    }
}
