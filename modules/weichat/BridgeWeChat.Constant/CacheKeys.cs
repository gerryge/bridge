﻿using System;

namespace BridgeWeChat.Constant
{
    public class CacheKeys
    {
        public static string WeChatAccessToken { get { return "_WeChatAccessToken"; } }
        public static string GeTuiToken { get { return "_GeTuiToken"; } }
    }
}
