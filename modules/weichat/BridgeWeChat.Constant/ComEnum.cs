﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BridgeWeChat.Constant
{
    public class ComEnum
    {
        public enum 平台类型
        {
            微信公众号 = 1,
            微信小程序 = 2
        }

        public enum 平台消息类型
        { 
            APP = 1,
            微信公众号 = 2,
            微信小程序 = 3,
            短信平台 = 4
        }

        public enum 模板消息类型
        {
            关注消息 = 1,
            扫码消息 = 2,
            证照到期提醒 = 3,
            订单生成通知 = 4,
            订单确认通知 = 5,
            单据审核结果通知 = 6,
            订单配送提醒 = 7
        }
    }
}
