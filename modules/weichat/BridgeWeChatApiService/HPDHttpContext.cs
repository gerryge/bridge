﻿using BridgeWeChat.Model;
using BridgeWebSystemLib.Core;
using BridgeWebSystemLib.Core.DataAccess;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BridgeWeChatApiService
{
    /// <summary>
    /// 
    /// </summary>
    public class HPDHttpContext : BridgeHttpContext
    {
        /// <summary>
        /// 
        /// </summary>
        public static IOptions<AppSettings> AppSettings
        {
            get
            {
                return GetService<IOptions<AppSettings>>();
            }
        }
    }

    public static class ServiceConfiguration
    {
        public static IServiceCollection ConfigServies(this IServiceCollection services, IConfiguration config)
        {
            ServiceCollectionServiceExtensions.AddSingleton<IHttpContextAccessor, HttpContextAccessor>(services);

            services.Configure<AppSettings>(config);

            string connectionString = config.GetSection("ConnectionStrings")["MySqlConnection"];
            ServiceCollectionServiceExtensions.AddTransient<IDataAccess>(services, d => DataAccessFactory.CreateMySqlDataAccess(connectionString));

            HPDHttpContext.Configure(services.BuildServiceProvider());
            return services;
        }
    }
}
