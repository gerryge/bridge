﻿using BridgeWeChat.Model;
using BridgeWebSystemLib.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BridgeWeChatApiService
{
    public class RequestHelper
    {
        /// <summary>
        /// 为空验证
        /// </summary>
        /// <typeparam name="TRequest"></typeparam>
        /// <param name="request"></param>
        /// <returns></returns>
        public static MsgInfo NotnullValidate<TRequest>(TRequest request)
        {
            var msgInfo = MsgInfo.CreateMsgInfo();
            StringBuilder strBud = new StringBuilder();
            var properties = typeof(TRequest).GetProperties();
            foreach (var prop in properties)
            {
                object[] objs = prop.GetCustomAttributes(typeof(NotNullAttribute), false);
                if (objs.Length == 1)
                {
                    var obj = prop.GetValue(request);
                    if (obj == null || obj.ToString() == "")
                    {
                        strBud.Append(prop.Name + "值不能为空！");
                        msgInfo.State = false;
                    }
                }
            }
            msgInfo.Message = strBud.ToString();
            return msgInfo;
        }
    }
}
