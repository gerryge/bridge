﻿public static class ScmConsts
{
    public const int MaxCodeLength = 32;

    public const int MaxNameLength = 256;

    public const int MaxImageNameLength = 128;
    public const string DefaultDbTablePrefix = "t_wms_";

    public const string DefaultDbSchema = null;
}

