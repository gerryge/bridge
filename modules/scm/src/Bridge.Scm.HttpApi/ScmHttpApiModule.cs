﻿using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.Modularity;
using Microsoft.Extensions.DependencyInjection;

[DependsOn(
    typeof(ScmApplicationContractsModule),
    typeof(AbpAspNetCoreMvcModule))]
public class ScmHttpApiModule : AbpModule
{
    public override void PreConfigureServices(ServiceConfigurationContext context)
    {
        PreConfigure<IMvcBuilder>(mvcBuilder =>
        {
            mvcBuilder.AddApplicationPartIfNotExists(typeof(ScmHttpApiModule).Assembly);
        });
    }

   
}

