﻿using Volo.Abp;


public class ScmCodeAlreadyExistsException : BusinessException
{
    public ScmCodeAlreadyExistsException(string productCode)
        : base("PM:000001", $"A product with code {productCode} has already exists!")
    {

    }
}
