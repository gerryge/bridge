﻿using Volo.Abp.AutoMapper;
using Volo.Abp.Modularity;


[DependsOn(
    typeof(ScmDomainModule),
    typeof(ScmApplicationContractsModule),
    typeof(AbpAutoMapperModule)
    )]
public class ScmApplicationModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        Configure<AbpAutoMapperOptions>(options =>
        {
            options.AddProfile<ScmApplicationAutoMapperProfile>(validate: true);
        });
    }
}

