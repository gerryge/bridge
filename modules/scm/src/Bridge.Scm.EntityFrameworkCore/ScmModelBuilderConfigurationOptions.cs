﻿using JetBrains.Annotations;
using Volo.Abp.EntityFrameworkCore.Modeling;


public class ScmModelBuilderConfigurationOptions : AbpModelBuilderConfigurationOptions
{
    public ScmModelBuilderConfigurationOptions(
        [NotNull] string tablePrefix = ScmConsts.DefaultDbTablePrefix,
        [CanBeNull] string schema = ScmConsts.DefaultDbSchema)
        : base(
            tablePrefix,
            schema)
    {

    }
}
