﻿using Microsoft.EntityFrameworkCore;
using Volo.Abp.Data;
using Volo.Abp.EntityFrameworkCore;


[ConnectionStringName("System")]
public class ScmDbContext : AbpDbContext<ScmDbContext>, IScmDbContext
{
    public static string TablePrefix { get; set; } = ScmConsts.DefaultDbTablePrefix;

    public static string Schema { get; set; } = ScmConsts.DefaultDbSchema;



    public ScmDbContext(DbContextOptions<ScmDbContext> options)
        : base(options)
    {

    }
    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);

        builder.ConfigureSystem(options =>
        {
            options.TablePrefix = TablePrefix;
            options.Schema = Schema;
        });
    }
}
