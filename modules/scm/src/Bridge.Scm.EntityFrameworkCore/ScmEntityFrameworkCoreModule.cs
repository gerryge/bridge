﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.EntityFrameworkCore;
using Volo.Abp.Modularity;


[DependsOn(
    typeof(ScmDomainModule),
    typeof(AbpEntityFrameworkCoreModule)
)]
public class ScmEntityFrameworkCoreModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        context.Services.AddAbpDbContext<ScmDbContext>(options =>
        {
            options.AddDefaultRepositories();
        });
    }
}
