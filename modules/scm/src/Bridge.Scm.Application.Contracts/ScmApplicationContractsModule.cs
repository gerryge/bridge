﻿
using Localization;
using Volo.Abp.Application;
using Volo.Abp.Authorization.Permissions;
using Volo.Abp.Localization;
using Volo.Abp.Modularity;
using Volo.Abp.Settings;
using Volo.Abp.VirtualFileSystem;


[DependsOn(
    typeof(ScmDomainSharedModule),
    typeof(AbpDddApplicationModule)
    )]
public class ScmApplicationContractsModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        Configure<AbpVirtualFileSystemOptions>(options =>
        {
            options.FileSets.AddEmbedded<ScmApplicationContractsModule>();
        });

        Configure<AbpLocalizationOptions>(options =>
        {
            options.Resources
                .Get<ScmResource>()
                .AddVirtualJson("/Scm/Localization/ApplicationContracts");
        });
    }
}

