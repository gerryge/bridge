﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Account.Input
{
    public class LoginInput
    {
        /// <summary>
        /// 手机号/用户名
        /// </summary>
        [Required(ErrorMessage = "不可为空")]

        public string Phone { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        [Required(ErrorMessage = "密码不可为空")]

        public string Password { get; set; }

        public bool RememberMe { get; set; }
    }
}
