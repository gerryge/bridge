﻿using Bridge.Shared.Enums.IdentityService;
using Bridge.Shared.Helper;
using System;
using System.Collections.Generic;
using System.Text;


namespace Account.Output
{
    public class LoginOutput
    {
        /// <summary>
        /// 登录结果:0登录成功，1密码错误5次
        /// </summary>
        public LoginResultEnum LoginResult { get; set; }

        /// <summary>
        /// 登录结果
        /// </summary>
        public string LoginResultStr
        {
            get
            {
                try
                {
                    return EnumHelper.GetDisplayAttributeName(typeof(LoginResultEnum), LoginResult);
                }
                catch (System.Exception)
                {

                    return "";
                }

            }
        }

        /// <summary>
        /// Token
        /// </summary>
        public string Token { get; set; }

        /// <summary>
        /// RefreshToken
        /// </summary>
        public string RefreshToken { get; set; }

        /// <summary>
        /// 是否首次登录
        /// </summary>
        public bool IsFirstLogin { get; set; }

        /// <summary>
        /// 是否长期未修改密码
        /// </summary>
        public bool IsNotChangePwd { get; set; }
        /// <summary>
        /// 长期未修改密码提示消息
        /// </summary>
        public string NotChangePwdErrorMessage { get; set; }


        /// <summary>
        /// 用户id
        /// </summary>
        public string UserName { get; set; }
    }
}
