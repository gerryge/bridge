﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Http.Client;
using Volo.Abp.Modularity;


[DependsOn(
    typeof(ScmApplicationContractsModule),
    typeof(AbpHttpClientModule))]
public class ScmHttpApiClientModule : AbpModule
{
    public const string RemoteServiceName = "Wms";

    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        context.Services.AddHttpClientProxies(
            typeof(ScmApplicationContractsModule).Assembly,
            RemoteServiceName
        );
    }
}

