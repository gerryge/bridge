﻿using JetBrains.Annotations;
using Volo.Abp.EntityFrameworkCore.Modeling;


public class SystemModelBuilderConfigurationOptions : AbpModelBuilderConfigurationOptions
{
    public SystemModelBuilderConfigurationOptions(
        [NotNull] string tablePrefix = SystemConsts.DefaultDbTablePrefix,
        [CanBeNull] string schema = SystemConsts.DefaultDbSchema)
        : base(
            tablePrefix,
            schema)
    {

    }
}
