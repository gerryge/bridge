﻿using System;
using Bridge.System.Domain;
using Microsoft.EntityFrameworkCore;
using Volo.Abp;
using Volo.Abp.EntityFrameworkCore.Modeling;


public static class SystemDbContextModelCreatingExtensions
{
    public static void ConfigureSystem(
        this ModelBuilder builder,
        Action<SystemModelBuilderConfigurationOptions> optionsAction = null)
    {
        Check.NotNull(builder, nameof(builder));

        var options = new SystemModelBuilderConfigurationOptions();

        optionsAction?.Invoke(options);
        builder.Entity<Menu>(s =>
        {
            s.ToTable("t_sys_menu");
            s.ConfigureByConvention();
        });

        builder.Entity<RoleMenu>(s =>
        {
            s.ToTable("t_sys_role_menu");
            s.ConfigureByConvention();
        });
        builder.Entity<UserExpand>(s =>
        {
            s.ToTable("t_sys_user_expand");
            s.ConfigureByConvention();
        });
        builder.Entity<DictData>(s =>
        {
            s.ToTable("t_sys_dictdata");
            s.ConfigureByConvention();
        });
        builder.Entity<Dept>(s =>
        {
            s.ToTable("t_sys_dept");
            s.ConfigureByConvention();
        });
        builder.Entity<Notice>(s =>
        {
            s.ToTable("t_sys_notice");
            s.ConfigureByConvention();
        });
        builder.Entity<GenTableColumn>(s =>
        {
            s.ToTable("t_sys_gentablecolumn");
            s.ConfigureByConvention();
        });
        builder.Entity<GenTable>(s =>
        {
            s.ToTable("t_sys_gentable");
            s.ConfigureByConvention();
        });
        //builder.Entity<Product>(b =>
        //{
        //    b.ToTable(options.TablePrefix + "Products", options.Schema);

        //    b.ConfigureConcurrencyStamp();
        //    b.ConfigureExtraProperties();
        //    b.ConfigureAudited();

        //    b.Property(x => x.Code).IsRequired().HasMaxLength(ProductConsts.MaxCodeLength);
        //    b.Property(x => x.Name).IsRequired().HasMaxLength(ProductConsts.MaxNameLength);
        //    b.Property(x => x.ImageName).HasMaxLength(ProductConsts.MaxImageNameLength);

        //    b.HasIndex(q => q.Code);
        //    b.HasIndex(q => q.Name);
        //}
        //);
    }
}
