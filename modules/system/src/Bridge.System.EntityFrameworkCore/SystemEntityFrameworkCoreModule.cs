﻿using Bridge.Shared.Extensions;
using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;
using Volo.Abp.Modularity;


[DependsOn(
    typeof(SystemDomainModule),
    typeof(AbpEntityFrameworkCoreModule)
)]
public class SystemEntityFrameworkCoreModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        context.Services.AddAbpDbContext<SystemDbContext>(options =>
        {
            options.AddDefaultRepositories();
        });
        //注入批量处理数据专用类
        context.Services.AddScoped<IEfCoreBulkOperationProvider, EfCoreBulkOperationProvider>();
    }
}
