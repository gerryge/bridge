﻿using Bridge.System.Domain;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Data;
using Volo.Abp.EntityFrameworkCore;


[ConnectionStringName("System")]
public class SystemDbContext : AbpDbContext<SystemDbContext>, ISystemDbContext
{
    public static string TablePrefix { get; set; } = SystemConsts.DefaultDbTablePrefix;

    public static string Schema { get; set; } = SystemConsts.DefaultDbSchema;

    //public DbSet<Product> Products { get; set; }

    public SystemDbContext(DbContextOptions<SystemDbContext> options)
        : base(options)
    {

    }
    public DbSet<Menu> Menus { get; set; }
    public DbSet<RoleMenu> RoleMenus { get; set; }
    public DbSet<UserExpand> UserExpands { get; set; }
    public DbSet<DictData> DictDatas { get; set; }
    public DbSet<Dept> Depts { get; set; }
    public DbSet<Notice> Notices { get; set; }
    public DbSet<GenTable> GenTables { get; set; }
    public DbSet<GenTableColumn> GenTableColumns { get; set; }
    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);

        builder.ConfigureSystem(options =>
        {
            options.TablePrefix = TablePrefix;
            options.Schema = Schema;
        });
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        //optionsBuilder.UseBatchEF_MySQLPomelo();
        base.OnConfiguring(optionsBuilder);
    }
}
