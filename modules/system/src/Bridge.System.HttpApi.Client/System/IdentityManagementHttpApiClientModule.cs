﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Http.Client;
using Volo.Abp.Modularity;

namespace Bridge.System
{
    [DependsOn(
        typeof(SystemApplicationContractsModule),
        typeof(AbpHttpClientModule))]
    public class SystemHttpApiClientModule : AbpModule
    {
        public const string RemoteServiceName = "System";

        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddHttpClientProxies(
                typeof(SystemApplicationContractsModule).Assembly,
                RemoteServiceName
            );
        }
    }
}
