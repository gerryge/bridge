﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Authoritys.Output
{
    public class AuthorityOutput
    {
        ///// <summary>
        ///// 页面代码
        ///// </summary>
        //public string PageCode { get; set; }

        ///// <summary>
        ///// 页面名
        ///// </summary>
        //public string PageName { get; set; }

        /// <summary>
        /// 菜单代码
        /// </summary>
        public string MenuCode { get; set; }

        /// <summary>
        /// 菜单名称
        /// </summary>
        public string MenuName { get; set; }

        /// <summary>
        /// 菜单Id
        /// </summary>
        public Guid MenuId { get; set; }

        /// <summary>
        /// add/del/update/disable等等操作
        /// </summary>
        public string Operate { get; set; }

        /// <summary>
        /// 新增/删除/修改/禁用等等
        /// </summary>
        public string OperateName { get; set; }
    }
}
