﻿using Authoritys.Input;
using Authoritys.Output;
using Bridge.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace Authoritys
{
    public interface IAuthorityService : IApplicationService
    {
        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<PageResponseResult<AuthorityOutput>> GetPageList(GetAuthorityInput input);
        /// <summary>
        /// 获取单个数据
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseResult<AuthorityOutput>> GetAuthority(Guid id);
        /// <summary>
        /// 新增数据
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<ResponseResult<AuthorityOutput>> Create(CreateAuthorityInput input);
        /// <summary>
        /// 修改数据
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<ResponseResult> Update(UpdateAuthorityInput input);
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        Task<ResponseResult> Delete(IdsInput<Guid> input);
        /// <summary>
        /// 获取某个用户权限数据
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<ResponseResult<List<AuthorityOutput>>> GetListByUserId(Guid userId);
    }
}
