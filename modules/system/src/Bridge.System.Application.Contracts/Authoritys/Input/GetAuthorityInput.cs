﻿using Bridge.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Authoritys.Input
{
    public class GetAuthorityInput:BasePageInput
    {
        /// <summary>
        /// 权限名称
        /// </summary>
        public string OperateName { get; set; }
    }
}
