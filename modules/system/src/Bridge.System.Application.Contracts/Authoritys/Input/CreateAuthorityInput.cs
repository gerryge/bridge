﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Authoritys.Input
{
    public class CreateAuthorityInput
    {
        public Guid MenuId { get; set; }
        /// <summary>
        /// add/del/update/disable等等操作
        /// </summary>
        public string Operate { get; set; }
        /// <summary>
        /// 新增/删除/修改/禁用等等
        /// </summary>
        public string OperateName { get; set; }
    }
}
