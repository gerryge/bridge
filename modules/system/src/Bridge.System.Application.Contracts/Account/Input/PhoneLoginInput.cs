﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Account.Input
{
    public class PhoneLoginInput
    {
        /// <summary>
        /// 手机号
        /// </summary>
        [Required(ErrorMessage = "手机号不可为空")]
        public string PhoneNumber { get; set; }

        /// <summary>
        /// 验证码
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 手机短信验证码
        /// </summary>
        [Required(ErrorMessage = "短信验证码不能为空")]
        public string PhoneCode { get; set; }

        public string Uuid { get; set; }

    }
}
