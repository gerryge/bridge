﻿using Account.Input;
using Account.Output;
using Bridge.Shared.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace Account
{
   public interface IUserAccountService : IApplicationService
    {
        Task<ResponseResult<LoginOutput>> Login(LoginInput login);
        Task<ResponseResult> LoginOut();
        Task<ResponseResult<LoginOutput>> SSOLogin(LoginInput input);

        Task<ResponseResult<LoginOutput>> PhoneLogin([FromBody] PhoneLoginInput login);
    }
}
