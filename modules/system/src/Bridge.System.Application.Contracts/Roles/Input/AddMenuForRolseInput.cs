﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roles.Input
{
    public class AddMenuForRolseInput
    {
        /// <summary>
        ///  角色Id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// 菜单Id集合
        /// </summary>
        public List<Guid> MenuIds { get; set; }
    }
}
