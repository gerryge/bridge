﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roles.Input
{
    public class UpdateRoleInput: RoleDtoBase
    {

        /// <summary>
        /// 角色ID
        /// </summary>
        public Guid Id { get; set; }
    }
}
