﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roles.Input
{
    public class AddAuthForRolseInput
    {
        /// <summary>
        /// 角色Id
        /// </summary>
        public Guid RoleId { get; set; }

        /// <summary>
        /// 权限Id集合
        /// </summary>
        public List<Guid> Auths { get; set; }
    }
}
