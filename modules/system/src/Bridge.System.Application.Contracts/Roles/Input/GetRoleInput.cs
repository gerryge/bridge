﻿using Bridge.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roles.Input
{
    public class GetRoleInput: BasePageInput
    {/// <summary>
     /// 角色名
     /// </summary>
        public string Name { get; set; }
     
        /// <summary>
        /// 角色状态 0:启用 1:禁用
        /// </summary>
        public int? RoleStatus { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreationTime { get; set; }
    }
}
