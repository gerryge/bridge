﻿using Bridge.Shared.Enums.IdentityService;
using Bridge.Shared.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roles.Output
{
    public class RoleOutput
    {
        public Guid Id { set; get; }
        public string Name { get; set; }
        public string NormalizedName { get; set; }

        /// <summary>
        /// 创建人
        /// </summary>
        public string CreateName { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreationTime { get; set; }

        /// <summary>
        /// 用户数
        /// </summary>
        public int UserNum { get; set; }

        /// <summary>
        /// 创建人
        /// </summary>
        public string UpdateName { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? UpdateTime { get; set; }

        /// <summary>
        /// 角色状态: 0. 启用 1.禁用
        /// </summary>
        public string RoleStatusStr
        {
            get
            {
                if (RoleStatus != null)
                {
                    return EnumHelper.GetDisplayAttributeName(typeof(RoleStatusEnum), RoleStatus);
                }
                return "";
            }
        }

        /// <summary>
        /// 状态类型
        /// </summary>
        public int? RoleStatus { get; set; }
    }
}
