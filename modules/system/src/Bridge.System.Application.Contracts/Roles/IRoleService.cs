﻿using Bridge.Shared.Models;
using Roles.Input;
using Roles.Output;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace Roles
{
    public interface IRoleService : IApplicationService
    {
        Task<ResponseResult<string>> CreateRole(CreateRoleInput input);

        Task<ResponseResult> UpdateRole(UpdateRoleInput input);
        Task<PageResponseResult<RoleOutput>> GetRoleList(GetRoleInput input);
        Task<ResponseResult> DeleteAsync(IdsInput<Guid> input);

        Task<ResponseResult> AddMenuForRolse(AddMenuForRolseInput Input);


        Task<ResponseResult<RoleOutput>> GetRoleInfo(Guid id);
    }
}
