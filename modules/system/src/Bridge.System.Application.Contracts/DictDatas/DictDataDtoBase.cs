﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DictDatas
{
    public class DictDataDtoBase
    {
        public Guid? Id { get; set; }
        public string DictName { get; set; }
        /// <summary>
        /// 字典值
        /// </summary>
        public int DictValue { get; set; }
        /// <summary>
        /// 字典类型
        /// </summary>
        public string DictType { get; set; }
        /// <summary>
        /// 状态（0正常 1停用）
        /// </summary>
        public int Status { get; set; }
        /// <summary>
        /// 字典排序
        /// </summary>
        public int DictSort { get; set; }

        public  DateTime CreationTime { get; set; }
    }
}
