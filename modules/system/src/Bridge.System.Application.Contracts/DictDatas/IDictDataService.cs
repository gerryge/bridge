﻿using Azure;
using Bridge.Shared.Models;
using Depts.Output;
using DictDatas.Input;
using DictDatas.Output;
using Roles.Input;
using Roles.Output;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace DictDatas
{
    public interface IDictDataService : IApplicationService
    {
       Task<ResponseResult<List<DictDataOutput>>> GetDictDataByDictType(string dictType);
        Task<ResponseResult<List<DictDataOutput>>> GetDictData(DictDataInput input);
        Task<ResponseResult> CreateOrUpdate(DictDataInput input);

        Task<PageResponseResult<DictDataOutput>> GetDictList(GetDictDataInput input);

        Task<ResponseResult> DeleteAsync(IdsInput<Guid> input);
        Task<ResponseResult<DictDataOutput>> GetDictInfo(Guid id);
    }
}
