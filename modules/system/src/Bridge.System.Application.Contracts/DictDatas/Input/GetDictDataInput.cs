﻿using Bridge.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DictDatas.Input
{
    public class GetDictDataInput:BasePageInput
    {
        public string DictName { get; set; }
        /// <summary>
        /// 字典类型
        /// </summary>
        public string DictType { get; set; }

        public int? Status { get; set; }

        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
    }
}
