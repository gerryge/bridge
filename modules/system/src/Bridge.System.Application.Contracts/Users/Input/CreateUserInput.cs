﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Identity;

namespace Users.Input
{
    public class CreateUserInput: IdentityUserCreateDto
    {
        /// <summary>
        /// 用户类型
        /// </summary>
        public int UserType { get; set; }
    }
}
