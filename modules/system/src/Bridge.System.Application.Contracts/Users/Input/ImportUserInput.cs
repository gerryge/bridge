﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Users.Input
{
    public class ImportUserInput
    {
        /// <summary>
        /// 用户名
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "用户名 不能为空！")]
        [Display(Name = "用户名")]
        public string UserName { get; set; }

        /// <summary>
        /// 真实姓名
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "真实姓名 不能为空！")]
        [Display(Name = "真实姓名")]
        public string RealName { get; set; }

        /// <summary>
        /// 邮箱
        /// </summary>
        [Display(Name = "邮箱")]
        public string Email { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        //[Required(AllowEmptyStrings = false, ErrorMessage = "手机号 不能为空！")]
        [Display(Name = "手机号")]
        public string Phone { get; set; }

        /// <summary>
        /// 用户类型（0平台，1供应商，2医院）
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "用户类型 不能为空！")]
        [Display(Name = "用户类型")]
        public string UserType { get; set; }

        /// <summary>
        /// 设置角色
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "设置角色 不能为空！")]
        [Display(Name = "设置角色")]
        public string RoleName { get; set; }


    }
}
