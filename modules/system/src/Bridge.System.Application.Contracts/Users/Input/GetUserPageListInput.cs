﻿using Bridge.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Users.Input
{
    public class GetUserPageListInput:BasePageInput
    {
        public string UserName { get; set; }

        public string PhoneNumber { get; set; }

        public bool? IsActive { get; set; }

        public DateTime? BeginTime { get; set; }

        public DateTime? EndTime { get; set; }
    }
}
