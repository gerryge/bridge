﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Users.Output
{
    public class ImportUserOutput
    {
        /// <summary>
        /// 导入成功数量
        /// </summary>
        public int SuccessCount { get; set; }

        /// <summary>
        /// 导入失败数量
        /// </summary>
        public int FailCount { get; set; }

        /// <summary>
        /// 错误消息
        /// </summary>
        public string ErrorMsg { get; set; }
    }
}
