﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Users.Output
{
    public class UserInfoOutput
    {
        public UserInfoOutput() { }
        public string UserName { get; set; }
        public Guid Id { get; set; }
        public string Avatar { get; set; }
        public List<string> Roles { get; set; }

        public string NormalizedUserName { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }    

        public string PhoneNumber { get; set; }

        public string Email { get; set; }

        public bool IsActive { get; set; }

        public DateTime? CreationTime { get; set; }

        public List<string> Permissions { get; set; }


    }
}
