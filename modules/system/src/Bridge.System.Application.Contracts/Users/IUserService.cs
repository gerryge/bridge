﻿using Bridge.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Users.Input;
using Users.Output;
using Volo.Abp.Application.Services;
using Volo.Abp.Identity;

namespace Users
{
    public interface IUserService : IApplicationService
    {

        Task<ResponseResult<CreateUserOutput>> CreateUser(CreateUserInput input);
        Task<ResponseResult> DeleteAsync(IdsInput<Guid> ids);
        Task<ResponseResult> UpdateAsync(UpdateUserInput input);
        Task<ResponseResult> ChangeIsActive(ChangeUseStatusInput input);
        Task AddRoleForUser(AddRoleForUserInput input);

        Task<ResponseResult<UserInfoOutput>> GetInfo();
        Task<ResponseResult<UserInfoOutput>> GetUserById(Guid Id);
        Task<PageResponseResult<UserInfoOutput>> GetPageListAsync(GetUserPageListInput input);

        Task<ImportUserOutput> ImportUserAsync(List<ImportUserInput> input);


        #region 工作流调用
        /// <summary>
        /// 通过角色Id,机构id获取用户
        /// </summary>
        /// <param name="roleId"></param>
        /// <param name="agencyId"></param>
        /// <returns></returns>
        Task<List<IdentityUserDto>> GetUserListByRoleName(Guid roleId);
        /// <summary>
        /// 获取用户信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<IdentityUserDto> WfGetAsyncById(Guid id);
        #endregion
    }
}
