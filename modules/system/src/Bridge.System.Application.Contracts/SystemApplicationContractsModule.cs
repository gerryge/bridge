﻿using Bridge.System;
using Bridge.System.Localization;
using Volo.Abp.Application;
using Volo.Abp.Authorization.Permissions;
using Volo.Abp.Localization;
using Volo.Abp.Modularity;
using Volo.Abp.Settings;
using Volo.Abp.VirtualFileSystem;


[DependsOn(
    typeof(SystemDomainSharedModule),
    typeof(AbpDddApplicationModule)
    )]
public class SystemApplicationContractsModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        Configure<AbpVirtualFileSystemOptions>(options =>
        {
            options.FileSets.AddEmbedded<SystemApplicationContractsModule>();
        });

        Configure<AbpLocalizationOptions>(options =>
        {
            options.Resources
                .Get<SystemResource>()
                .AddVirtualJson("/System/Localization/ApplicationContracts");
        });
    }
}

