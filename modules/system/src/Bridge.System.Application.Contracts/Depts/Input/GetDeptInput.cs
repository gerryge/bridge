﻿using Bridge.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Depts.Input
{
    public class GetDeptInput:BasePageInput
    {
        public string DeptName { get; set; }
    }
}
