﻿using Bridge.Shared.Models;
using Depts.Input;
using Depts.Output;
using Roles.Input;
using Roles.Output;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace Depts
{
    public interface IDeptService : IApplicationService
    {
        Task<ResponseResult<string>> CreateDept(CreateDeptInput input);

        Task<ResponseResult> UpdateDept(UpdateDeptInput input);
        Task<PageResponseResult<DeptTreeOutput>> GetDeptList(GetDeptInput input);
        Task<ResponseResult> DeleteAsync(IdsInput<Guid> input);
        Task<ResponseResult<DeptOutput>> GetDeptInfo(Guid id);
    }
}
