﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Depts
{
    public class DeptDtoBase
    {
        public Guid? Id { get; set; }
        /// <summary>
      /// 父部门标识
      /// </summary>
        public Guid? ParentId { get; set; }
        /// <summary>
        /// 部门名称
        /// </summary>
        public string DeptName { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int OrderNum { get; set; }

        /// <summary>
        /// 联系电话
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// 邮箱
        /// </summary>
        public string Email { get; set; }

        public int Status { get; set; }

        public DateTime CreationTime { get; set; }
        /// <summary>
        /// 负责人
        /// </summary>
        public string Leader { get; set; }
    }
}
