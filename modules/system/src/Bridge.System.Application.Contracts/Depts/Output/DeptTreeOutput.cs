﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Depts.Output
{
    public class DeptTreeOutput:DeptDtoBase
    {
        public List<DeptTreeOutput> Children { get; set; }
    }
}
