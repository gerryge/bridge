﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenTables
{
    public class GenTableDtoBase
    {
        public Guid? Id { get; set; }
        public string DbName { get; set; }

        public string TableName { get; set; }

        public string TableComment { get; set; }
        /// <summary>
        /// 关联父表的表名
        /// </summary>

        public string SubTableName { get; set; }
        /// <summary>
        /// 本表关联父表的外键名
        /// </summary>

        public string SubTableFkName { get; set; }
        /// <summary>
        /// csharp类名
        /// </summary>

        public string ClassName { get; set; }
        /// <summary>
        /// 使用的模板（crud单表操作 tree树表操作 sub主子表操作）
        /// </summary>

        public string TplCategory { get; set; }
        /// <summary>
        /// 基本命名空间前缀
        /// </summary>

        public string BaseNameSpace { get; set; }
        /// <summary>
        /// 生成模块名
        /// </summary>

        public string ModuleName { get; set; }
        /// <summary>
        /// 生成业务名
        /// </summary>

        public string BusinessName { get; set; }
        /// <summary>
        /// 生成功能名
        /// </summary>

        public string FunctionName { get; set; }
        /// <summary>
        /// 生成作者名
        /// </summary>

        public string FunctionAuthor { get; set; }
        /// <summary>
        /// 生成代码方式（0zip压缩包 1自定义路径）
        /// </summary>

        public string GenType { get; set; }
    }
}
