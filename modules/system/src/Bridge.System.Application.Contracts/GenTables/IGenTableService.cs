﻿using Bridge.Shared.Models;
using GenTables.Input;
using GenTables.Output;
using GenTables.Input;
using GenTables.Output;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace GenTables
{
    public interface IGenTableService : IApplicationService
    {

        Task<ResponseResult<List<GenTableOutput>>> GetGenTable(GenTableInput input);
        Task<ResponseResult> CreateOrUpdate(GenTableInput input);

        Task<PageResponseResult<GenTableOutput>> GetGenTableList(GetGenTableInput input);

        Task<ResponseResult> DeleteAsync(IdsInput<Guid> input);
        Task<ResponseResult<GenTableOutput>> GetGenTableInfo(Guid id);

        Task<ResponseResult<List<string>>> GetDbList();
    }
}
