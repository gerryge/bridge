﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Menus.Output
{
    public class MenuTreeSelectDto
    {
        public List<MenuTreeSelectOutput> Menus { get; set; }
        public List<Guid> CheckedKeys { get; set; }
    }
}
