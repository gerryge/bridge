﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Menus.Output
{
    public class MenuTreeOutput
    {
        public Guid Id {  get; set; }
        public string Name { get; set; }
        public int Sort { get; set; }
        public string Path { get; set; }
        public string Component { get; set; }
        public string MenuType { get; set; }
        public string Icon { get; set; }
        public string Perms { get; set; }
        public List<MenuTreeOutput> Children { get; set; }
    }
}
