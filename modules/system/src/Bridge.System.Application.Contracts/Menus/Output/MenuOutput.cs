﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Menus.Output
{
    public class MenuOutput
    {
        public Guid Id { get; set; }
        /// <summary>
        /// 菜单名
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 编码
        /// </summary>
        public int Code { get; set; }
        /// <summary>
        /// 父级菜单
        /// </summary>
        public Guid ParentId { get; set; }

        /// <summary>
        /// 父级菜单名称
        /// </summary>
        public string ParentName { get; set; }

        /// <summary>
        /// 层级
        /// </summary>
        public int Level { get; set; }
        /// <summary>
        /// 图标路径
        /// </summary>
        public string Icon { get; set; }
        /// <summary>
        /// 跳转路径
        /// </summary>
        public string Path { get; set; }


        //客户端类型 0 APP 1 web
        public int ClientType { get; set; }


        /// <summary>
        /// 类型（M目录 C菜单 F按钮 L链接）
        /// </summary>
        [MaxLength(1)]
        public string MenuType { get; set; } = string.Empty;
        /// <summary>
        /// MenuInOut
        /// </summary>
        public int MenuInOut { get; set; }
        /// <summary>
        ///排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 创建人
        /// </summary>
        public string CreatorName { get; set; }

        /// <summary>
        /// 组件
        /// </summary>
        public string Component { get; set; }
        /// <summary>
        /// 权限
        /// </summary>
        public string Perms { get; set; }
        /// <summary>
        /// 是否外连
        /// </summary>
        public string IsFrame { get; set; }
        /// <summary>
        /// 更新人
        /// </summary>
        public string LastModifierName { get; set; }

        public List<MenuOutput> Children { get; set; }
    }
}
