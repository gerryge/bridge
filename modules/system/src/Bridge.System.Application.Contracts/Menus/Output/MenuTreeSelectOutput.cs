﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Menus.Output
{
    public class MenuTreeSelectOutput
    {
        /// <summary>
        /// 菜单Id
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// 菜单名称
        /// </summary>
        public string Name { get; set; }
        public List<MenuTreeSelectOutput> Children { get; set; }
    }
}
