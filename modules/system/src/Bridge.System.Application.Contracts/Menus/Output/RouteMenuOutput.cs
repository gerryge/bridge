﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Menus.Output
{
    public class RouteMenuOutput:MenuDtoBase
    {
        public MetaOutput Meta { get; set; }

        public RouteMenuOutput() { }
        public List<RouteMenuOutput> Children {  get; set; }

        public bool Hidden { get; set; }
        public bool AlwaysShow { get; set; } 

        public string Component { get; set; }
    }


    public class MetaOutput
    {
        public MetaOutput() { }

        public string Title { get; set; }
        public string Icon { get; set; }

        public bool NoCache { get; set; }

        public string TitleKey { get; set; }

        public string Link { get; set; }
        public int IsNew { get; set; }
    }
}
