﻿using Bridge.Shared.Models;
using Menus.Input;
using Menus.Output;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace Menus
{
    public interface IMenuService : IApplicationService
    {
        /// <summary>
     /// 分页查询
     /// </summary>
     /// <param name="input"></param>
     /// <returns></returns>
        Task<PageResponseResult<MenuTreeOutput>> GetPageListAsync(GetMenuPageListInput input);
        /// <summary>
        /// 获取单个数据
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseResult<MenuOutput>> GetAsync(Guid id);
        /// <summary>
        /// 新增数据
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<ResponseResult<MenuOutput>> CreateAsync(CreateMenuInput input);
        /// <summary>
        /// 修改数据
        /// </summary>
        /// <param name="id"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<ResponseResult<MenuOutput>> UpdateAsync(UpdateMenuInput input);
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseResult> DeleteAsync(IdsInput<Guid> input);


        Task<ResponseResult<List<RouteMenuOutput>>> GetRouters();

        Task<ResponseResult<MenuTreeSelectDto>> GetMenuTreeSelect(Guid roleId);


    }
}
