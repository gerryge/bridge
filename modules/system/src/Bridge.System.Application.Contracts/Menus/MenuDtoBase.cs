﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Menus
{
    public class MenuDtoBase
    {
        /// <summary>
        /// 菜单名
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 权限字符串
        /// </summary>
        public string Perms { get; set; }
        /// <summary>
        /// 父级菜单
        /// </summary>
        public Guid? ParentId { get; set; }

        /// <summary>
        /// 图标路径
        /// </summary>
        public string Icon { get; set; }
        /// <summary>
        /// 跳转路径
        /// </summary>
        public string Path { get; set; }

        //客户端类型 0 APP 1 web
        public int ClientType { get; set; }

        public string Component { get; set; }
        /// <summary>
        /// 类型（M目录 C菜单 F按钮 L链接）
        /// </summary>
        public string MenuType { get; set; } = string.Empty;

        public string IsFrame { get; set; }
        /// <summary>
        ///排序
        /// </summary>
        public int Sort { get; set; }
    }
}
