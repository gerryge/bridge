﻿using Bridge.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Menus.Input
{
    public class GetMenuPageListInput:BasePageInput
    {
        /// <summary>
        /// 菜单名
        /// </summary>
        public string MenuName { get; set; }
    }
}
