﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Menus.Input
{
    public class UpdateMenuInput: MenuDtoBase
    {
        /// <summary>
        /// 菜单标识
        /// </summary>
        public Guid Id { get; set; }
       
    }
}
