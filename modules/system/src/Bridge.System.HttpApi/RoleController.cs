﻿using Bridge.Shared.Models;
using Microsoft.AspNetCore.Mvc;
using Roles;
using Roles.Input;
using Roles.Output;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.AspNetCore.Mvc;

namespace Bridge.System.HttpApi
{
    [Route("[controller]/[action]")]
    public class RoleController:AbpController
    {
        private readonly IRoleService _roleService;
        public RoleController(IRoleService roleService) {
            _roleService = roleService;
        }
        [HttpPost]
       public async Task<ResponseResult<string>> CreateRole([FromBody]CreateRoleInput input)
        {
            try
            {
                return await _roleService.CreateRole(input);
            }
            catch (Exception ex)
            {
                return ResponseResult<string>.Fail(ex.Message);
            }
        }
        [HttpPost]
       public async Task<ResponseResult> UpdateRole([FromBody] UpdateRoleInput input)
        {
            try
            {
                return await _roleService.UpdateRole(input);
            }
            catch (Exception ex)
            {
                return ResponseResult.Fail(ex.Message);
            }
        }
        [HttpPost]
       public async Task<PageResponseResult<RoleOutput>> GetRoleList([FromBody] GetRoleInput input)
        {
            try
            {
                return await _roleService.GetRoleList(input);
            }
            catch (Exception ex)
            {
                return PageResponseResult<RoleOutput>.Fail(ex.Message);
            }
        }
        [HttpPost]
       public async Task<ResponseResult> DeleteAsync([FromBody] IdsInput<Guid> input)
        {
            try
            {
                return await _roleService.DeleteAsync(input);
            }
            catch (Exception ex)
            {
                return ResponseResult.Fail(ex.Message);
            }
        }

        [HttpPost]
        public async Task<ResponseResult> AddMenuForRolse([FromBody]  AddMenuForRolseInput input)
        {
            try
            {
                return await _roleService.AddMenuForRolse(input);
            }
            catch (Exception ex)
            {
                return ResponseResult.Fail(ex.Message);
            }
        }
        [HttpGet("{id}")]
        public async Task<ResponseResult<RoleOutput>> GetRoleInfo(Guid id)
        {
            try
            {
                return await _roleService.GetRoleInfo(id);
            }
            catch (Exception ex)
            {
                return ResponseResult<RoleOutput>.Fail(ex.Message);
            }
        }
    }
}
