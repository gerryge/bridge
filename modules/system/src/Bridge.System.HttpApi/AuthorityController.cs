﻿using Authoritys;
using Authoritys.Input;
using Authoritys.Output;
using Bridge.Shared.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.AspNetCore.Mvc;

namespace Bridge.System.HttpApi
{
    [Route("[controller]/[action]")]
    public class AuthorityController:AbpController
    {
        private readonly IAuthorityService _authorityService;
        public AuthorityController(IAuthorityService authorityService) {
        
          _authorityService = authorityService;
        }

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
       public async Task<PageResponseResult<AuthorityOutput>> GetPageList(GetAuthorityInput input)
        {
            try
            {
                return await _authorityService.GetPageList(input);
            }
            catch (Exception ex)
            {
                return PageResponseResult<AuthorityOutput>.Fail(ex.Message);
            }
        }
        /// <summary>
        /// 获取单个数据
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
       public async Task<ResponseResult<AuthorityOutput>> GetAuthority(Guid id)
        {
            try
            {
                return await _authorityService.GetAuthority(id);
            }
            catch (Exception ex)
            {
                return ResponseResult<AuthorityOutput>.Fail(ex.Message);
            }
        }
        /// <summary>
        /// 新增数据
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
       public async Task<ResponseResult<AuthorityOutput>> Create(CreateAuthorityInput input)
        {
            try
            {
                return await _authorityService.Create(input);
            }
            catch (Exception ex)
            {
                return ResponseResult<AuthorityOutput>.Fail(ex.Message);
            }
        }
        /// <summary>
        /// 修改数据
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
       public async Task<ResponseResult> Update(UpdateAuthorityInput input)
        {
            try
            {
                return await _authorityService.Update(input);
            }
            catch (Exception ex)
            {
                return ResponseResult<AuthorityOutput>.Fail(ex.Message);
            }
        }
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        /// 
        [HttpPost]
        public async Task<ResponseResult> Delete(IdsInput<Guid> input)
        {
            try
            {
                return await _authorityService.Delete(input);
            }
            catch (Exception ex)
            {
                return ResponseResult<AuthorityOutput>.Fail(ex.Message);
            }
        }
        /// <summary>
        /// 获取某个用户权限数据
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>d
        [HttpGet]
       public async Task<ResponseResult<List<AuthorityOutput>>> GetListByUserId(Guid userId)
        {
            try
            {
                return await _authorityService.GetListByUserId(userId);
            }
            catch (Exception ex)
            {
                return ResponseResult<List<AuthorityOutput>>.Fail(ex.Message);
            }
        }
    }
}
