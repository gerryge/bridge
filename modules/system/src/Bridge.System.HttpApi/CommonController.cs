﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using NPOI.HPSF;
using System;
using System.Collections.Generic;
using FileIO=System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Volo.Abp.AspNetCore.Mvc;

namespace Bridge.System.HttpApi
{
    [Route("[controller]/[action]")]
    public class CommonController : AbpController
    {
        private readonly IWebHostEnvironment _webHostEnvironment;

        public CommonController(IWebHostEnvironment webHostEnvironment)
        {
            _webHostEnvironment = webHostEnvironment;
        }
        public CommonController() { }
        /// <summary>
        /// 用户导入模板下载
        /// </summary>
        /// <returns></returns>
        [HttpGet("{templateName}")]
        [AllowAnonymous]
        public async Task<IActionResult> DownTemplate(string templateName)
        {
            string sFileName = $"{templateName}.xlsx";
            string fullPath = FileIO.Path.Combine(_webHostEnvironment.WebRootPath, "template", sFileName);
            if (!FileIO.Path.Exists(fullPath))
            {
                throw new Exception(fullPath + "文件不存在");
            }
            var stream = FileIO.File.OpenRead(fullPath);  //创建文件流

            Response.Headers.Add("Access-Control-Expose-Headers", "Content-Disposition");
            return  File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", sFileName);
        }
    }
}
