﻿using Bridge.Shared.Models;
using Menus;
using Menus.Output;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Mvc;
using Notices;
using Notices.Input;
using Notices.Output;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.AspNetCore.Mvc;

namespace Bridge.System.HttpApi
{
    [Microsoft.AspNetCore.Mvc.Route("[controller]/[action]")]
    public class NoticeController : AbpController
    {
        private readonly INoticeService _noticeService;
        public NoticeController(INoticeService noticeServiceb)
        {
            _noticeService = noticeServiceb;
        }
        [HttpPost]
        public async Task<ResponseResult<NoticeOutput>> CreateAsync([FromBody]CreateNoticeInput input)
        {
            try
            {
                return await _noticeService.CreateAsync(input);
            }
            catch (Exception ex)
            {
                return ResponseResult<NoticeOutput>.Fail(ex.Message);
            }
        }
        [HttpPost]
        public async Task<ResponseResult> DeleteAsync([FromBody]IdsInput<Guid> input)
        {
            try
            {
                return await _noticeService.DeleteAsync(input);
            }
            catch (Exception ex)
            {
                return ResponseResult.Fail(ex.Message);
            }
        }
        [HttpGet("{id}")]
        public async Task<ResponseResult<NoticeOutput>> GetAsync(Guid id)
        {
            try
            {
                return await _noticeService.GetAsync(id);
            }
            catch (Exception ex)
            {
                return ResponseResult<NoticeOutput>.Fail(ex.Message);
            }
        }
        [HttpPost]
        public async Task<PageResponseResult<NoticeOutput>> GetPageListAsync([FromBody]NoticePageListInput input)
        {
            try
            {
                return await _noticeService.GetPageListAsync(input);
            }
            catch (Exception ex)
            {
                return PageResponseResult<NoticeOutput>.Fail(ex.Message);
            }
        }
        [HttpPost]
        public async Task<ResponseResult<NoticeOutput>> UpdateAsync([FromBody]UpdateNoticeInput input)
        {
            try
            {
                return await _noticeService.UpdateAsync(input);
            }
            catch (Exception ex)
            {
                return ResponseResult<NoticeOutput>.Fail(ex.Message);
            }
        }

        [HttpPut("{id}")]
        public async Task<ResponseResult> SendNotice(Guid id)
        {
            try
            {
                return await _noticeService.SendNotice(id);
            }
            catch (Exception ex)
            {
                return ResponseResult.Fail(ex.Message);
            }
        }
    }
}
