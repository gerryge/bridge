﻿using Bridge.Shared.Models;
using GenTables;
using GenTables.Input;
using GenTables.Output;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.AspNetCore.Mvc;

namespace Bridge.System.HttpApi
{
    [Route("[controller]/[action]")]
    public class GenTableController : AbpController
    {
        private readonly IGenTableService _genTableService;
        public GenTableController(IGenTableService _genTableService) { 
          this._genTableService = _genTableService;
        }

        [HttpPost]
        public async Task<PageResponseResult<GenTableOutput>> GetGenTableList([FromBody]GetGenTableInput input)
        {
            try
            {
                return await _genTableService.GetGenTableList(input);
            }
            catch (Exception ex)
            {
                return PageResponseResult<GenTableOutput>.Fail(ex.Message);
            }
        }

        [HttpGet]
       public async  Task<ResponseResult<List<string>>> GetDbList()
        {
            try
            {
                return await _genTableService.GetDbList();
            }
            catch (Exception ex)
            {
                return ResponseResult<List<string>>.Fail(ex.Message);
            }
        }
    }
}
