﻿using Bridge.Shared.Models;
using Menus;
using Menus.Input;
using Menus.Output;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.AspNetCore.Mvc;

namespace Bridge.System.HttpApi
{
    [Route("[controller]/[action]")]
    public class MenuController : AbpController
    {

        private readonly IMenuService _menuService;
        public MenuController(IMenuService menuServiceb)
        {
            _menuService = menuServiceb;
        }

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponseResult<MenuTreeOutput>> GetPageListAsync([FromBody] GetMenuPageListInput input)
        {
            try
            {
                return await _menuService.GetPageListAsync(input);
            }
            catch (Exception ex)
            {
                return PageResponseResult<MenuTreeOutput>.Fail(ex.Message);
            }
        }
        /// <summary>
        /// 获取单个数据
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ResponseResult<MenuOutput>> GetAsync(Guid id)
        {
            try
            {
                return await _menuService.GetAsync(id);
            }
            catch (Exception ex)
            {
                return ResponseResult<MenuOutput>.Fail(ex.Message);
            }
        }
        /// <summary>
        /// 新增数据
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseResult<MenuOutput>> CreateAsync([FromBody] CreateMenuInput input)
        {
            try
            {
                return await _menuService.CreateAsync(input);
            }
            catch (Exception ex)
            {
                return ResponseResult<MenuOutput>.Fail(ex.Message);
            }
        }
        /// <summary>
        /// 修改数据
        /// </summary>
        /// <param name="id"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseResult<MenuOutput>> UpdateAsync([FromBody] UpdateMenuInput input)
        {
            try
            {
                return await _menuService.UpdateAsync(input);
            }
            catch (Exception ex)
            {
                return ResponseResult<MenuOutput>.Fail(ex.Message);
            }
        }
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseResult> DeleteAsync([FromBody] IdsInput<Guid> input)
        {
            try
            {
                return await _menuService.DeleteAsync(input);
            }
            catch (Exception ex)
            {
                return ResponseResult<MenuOutput>.Fail(ex.Message);
            }
        }

        [HttpGet]
        public async Task<ResponseResult<List<RouteMenuOutput>>> GetRouters()
        {
            try
            {
                return await _menuService.GetRouters();
            }
            catch (Exception ex)
            {
                return ResponseResult<List<RouteMenuOutput>>.Fail(ex.Message);
            }
        }

        [HttpGet("{roleId}")]
        public async Task<ResponseResult<MenuTreeSelectDto>> GetMenuTreeSelect(Guid roleId)
        {
            try
            {
                return await _menuService.GetMenuTreeSelect(roleId);
            }
            catch (Exception ex)
            {
                return ResponseResult<MenuTreeSelectDto>.Fail(ex.Message);
            }
        }
    }
}
