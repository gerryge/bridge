﻿using Bridge.Shared.Models;
using DictDatas;
using DictDatas.Input;
using DictDatas.Output;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridge.System.HttpApi
{
    [Route("[controller]/[action]")]
    public class DictDataController
    {
        private  IDictDataService _dictDataService { get; set; }

        public DictDataController(IDictDataService dictDataService)
        {
            _dictDataService = dictDataService;
        }
        [HttpGet("{dictType}")]
        public async Task<ResponseResult<List<DictDataOutput>>> GetDictDataByDictType(string dictType)
        {
            try
            {
                return await _dictDataService.GetDictDataByDictType(dictType);
            }
            catch (Exception ex)
            {
                return ResponseResult<List<DictDataOutput>>.Fail(ex.Message);
            }
            
        }
        [HttpPost]
        public async Task<ResponseResult<List<DictDataOutput>>> GetDictDataByDictType([FromBody]DictDataInput input)
        {
            try
            {
                return await _dictDataService.GetDictData(input);
            }
            catch (Exception ex)
            {
                return ResponseResult<List<DictDataOutput>>.Fail(ex.Message);
            }

        }
        [HttpPost]
        public async Task<ResponseResult> CreateOrUpdate([FromBody] DictDataInput input)
        {
            try
            {
                return await _dictDataService.CreateOrUpdate(input);
            }
            catch (Exception ex)
            {
                return ResponseResult<ResponseResult>.Fail(ex.Message);
            }

        }

        [HttpPost]
        public async Task<PageResponseResult<DictDataOutput>> GetDictList([FromBody] GetDictDataInput input)
        {
            try
            {
                return await _dictDataService.GetDictList(input);
            }
            catch (Exception ex)
            {
                return PageResponseResult<DictDataOutput>.Fail(ex.Message);
            }
        }

        [HttpPost]
        public async Task<ResponseResult> DeleteAsync([FromBody] IdsInput<Guid> input)
        {
            try
            {
                return await _dictDataService.DeleteAsync(input);
            }
            catch (Exception ex)
            {
                return PageResponseResult<DictDataOutput>.Fail(ex.Message);
            }
        }
        [HttpGet("{id}")]
        public async Task<ResponseResult<DictDataOutput>> GetDictInfo(Guid id)
        {
            try
            {
                return await _dictDataService.GetDictInfo(id);
            }
            catch (Exception ex)
            {
                return ResponseResult<DictDataOutput>.Fail(ex.Message);
            }
        }
    }
}
