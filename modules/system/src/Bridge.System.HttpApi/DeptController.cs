﻿using Bridge.Shared.Models;
using Depts;
using Depts.Input;
using Depts.Output;
using Menus;
using Menus.Input;
using Menus.Output;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.AspNetCore.Mvc;

namespace Bridge.System.HttpApi
{
    [Route("[controller]/[action]")]
    public class DeptController : AbpController
    {
        private readonly IDeptService _deptService;
        public DeptController(IDeptService deptServiceb)
        {
            _deptService = deptServiceb;
        }
        [HttpPost]
        public async Task<PageResponseResult<DeptTreeOutput>> GetDeptList([FromBody] GetDeptInput input)
        {
            try
            {
                return await _deptService.GetDeptList(input);
            }
            catch (Exception ex)
            {
                return PageResponseResult<DeptTreeOutput>.Fail(ex.Message);
            }
        }
        [HttpPost]
        public async Task<ResponseResult<string>> CreateDept([FromBody] CreateDeptInput input)
        {
            try
            {
                return await _deptService.CreateDept(input);
            }
            catch (Exception ex)
            {
                return ResponseResult<string>.Fail(ex.Message);
            }
        }
        [HttpPost]
        public async Task<ResponseResult> DeleteAsync([FromBody] IdsInput<Guid> input)
        {
            try
            {
                return await _deptService.DeleteAsync(input);
            }
            catch (Exception ex)
            {
                return ResponseResult.Fail(ex.Message);
            }
        }
        [HttpGet("{id}")]
        public async Task<ResponseResult<DeptOutput>> GetDeptInfo(Guid id)
        {
            try
            {
                return await _deptService.GetDeptInfo(id);
            }
            catch (Exception ex)
            {
                return ResponseResult<DeptOutput>.Fail(ex.Message);
            }
        }
        [HttpPost]
        public async Task<ResponseResult> UpdateDept([FromBody]  UpdateDeptInput input)
        {
            try
            {
                return await _deptService.UpdateDept(input);
            }
            catch (Exception ex)
            {
                return ResponseResult.Fail(ex.Message);
            }
        }


    }
}
