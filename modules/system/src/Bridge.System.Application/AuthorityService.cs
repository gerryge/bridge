﻿using Authoritys;
using Authoritys.Input;
using Authoritys.Output;
using Bridge.System.Domain;
using Bridge.Shared.Models;
using Mapster;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Users;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;
using Volo.Abp.Identity;

namespace Bridge.System.Application
{
    public class AuthorityService : ApplicationService, IAuthorityService
    {
       private IRepository<Authority, Guid> _authRepository;
        public AuthorityService(IRepository<Authority, Guid> authRepository) {
            _authRepository = authRepository; 
        }
        public async Task<ResponseResult<AuthorityOutput>> Create(CreateAuthorityInput input)
        {
            var auth = new Authority(GuidGenerator.Create(), input.MenuId, input.Operate, input.OperateName);

            await _authRepository.InsertAsync(auth);
            var dto = auth.Adapt<AuthorityOutput>();
            return ResponseResult<AuthorityOutput>.Success("ok",dto);
        }

        public async Task<ResponseResult> Delete(IdsInput<Guid> input)
        {
            var authList = await _authRepository.GetListAsync(a=>input.Ids.Contains(a.Id));
            if (!authList.Any())
            {
                return ResponseResult.Fail("记录不存在！");
            }

          await   _authRepository.DeleteManyAsync(authList);

            return ResponseResult<AuthorityOutput>.Success("ok");
        }

        public async Task<ResponseResult<AuthorityOutput>> GetAuthority(Guid id)
        {
            var auth = _authRepository.FirstOrDefaultAsync(a=>a.Id == id);
            if(auth == null)
            {
                return ResponseResult<AuthorityOutput>.Fail("记录不存在！");
            }
            var dto = auth.Adapt<AuthorityOutput>();
            return ResponseResult<AuthorityOutput>.Success("ok", dto);
        }

        public async Task<ResponseResult<List<AuthorityOutput>>> GetListByUserId(Guid userId)
        {
            throw new NotImplementedException();
        }

        public async Task<PageResponseResult<AuthorityOutput>> GetPageList(GetAuthorityInput input)
        {
            var query = (await _authRepository.GetQueryableAsync()).WhereIf(!string.IsNullOrEmpty(input.OperateName), a => a.OperateName == input.OperateName);

            var totalCount = query.Count();
            if(totalCount == 0)
            {
                return PageResponseResult<AuthorityOutput>.Fail("暂无数据");
            }

            var list = await query.Skip(input.PageIndex * input.PageSize).Take(input.PageSize).ToListAsync();

            var dto = list.Adapt<List<AuthorityOutput>>();

            return PageResponseResult<AuthorityOutput>.Success("ok",dto,input.PageSize,input.PageIndex,totalCount);
        }

        public async Task<ResponseResult> Update(UpdateAuthorityInput input)
        {
            var auth = await _authRepository.FirstOrDefaultAsync(a => a.Id == input.Id);
            if(auth == null)
            {
                return ResponseResult.Fail("记录不存在！");
            }

            auth.Operate = input.Operate;
            auth.OperateName=input.OperateName;

            await _authRepository.UpdateAsync(auth);

            return ResponseResult<ResponseResult>.Success("ok");
        }
    }
}
