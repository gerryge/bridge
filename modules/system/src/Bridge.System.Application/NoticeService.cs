﻿using Bridge.Shared.Extensions;
using Bridge.Shared.Models;
using Bridge.System.Domain;
using Mapster;
using Menus;
using Microsoft.AspNetCore.SignalR;
using Notices;
using Notices.Input;
using Notices.Output;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;

namespace Bridge.System.Application
{
    public class NoticeService : ApplicationService, INoticeService
    {
        private IRepository<Notice, Guid> _noticeRepository;
        private readonly IHubContext<MessageHub> _hubContext;
        public NoticeService(IRepository<Notice, Guid> noticeRepository, IHubContext<MessageHub> hubContext = null)
        {
            _noticeRepository = noticeRepository;
            _hubContext = hubContext;   
        }
        public async Task<ResponseResult<NoticeOutput>> CreateAsync(CreateNoticeInput input)
        {
            if (string.IsNullOrEmpty(input.Title)|| string.IsNullOrEmpty(input.NoticeContent))
            {
                return ResponseResult<NoticeOutput>.Fail("题目和内容不能为空！");
            }

            var notice = new Notice(GuidGenerator.Create())
            { Title = input.Title, NoticeContent = input.NoticeContent,NoticeType=input.NoticeType,Status=input.Status,CreateBy = CurrentUser.UserName  };

            await _noticeRepository.InsertAsync(notice);
            var dto = notice.Adapt<NoticeOutput>();
            return ResponseResult<NoticeOutput>.Success("操作成功", dto);

        }

        public async Task<ResponseResult> DeleteAsync(IdsInput<Guid> input)
        {
            var notices = (await _noticeRepository.GetQueryableAsync()).Where(a => input.Ids.Contains(a.Id)).ToList();
            if (!notices.Any())
            {
                return ResponseResult.Fail("记录不存在！");
            }

            await _noticeRepository.DeleteManyAsync(notices);
            return ResponseResult.Success("操作成功！");
        }

        public async Task<ResponseResult<NoticeOutput>> GetAsync(Guid id)
        {
           var notice =await _noticeRepository.FirstOrDefaultAsync(a => a.Id == id);
            if(notice == null)
            {
                return ResponseResult<NoticeOutput>.Fail("通知通告记录不存在！");
            }

            var dto = notice.Adapt<NoticeOutput>();
            return ResponseResult<NoticeOutput>.Success("操作成功！", dto);
        }

        public async Task<PageResponseResult<NoticeOutput>> GetPageListAsync(NoticePageListInput input)
        {
            var query = (await _noticeRepository.GetQueryableAsync()).WhereIf(!string.IsNullOrEmpty(input.Title),a=>a.Title.Contains(input.Title))
                                                                     .WhereIf(!string.IsNullOrEmpty(input.NoticeContent),a=>a.NoticeContent.Contains(input.NoticeContent))
                                                                     .WhereIf(input.Status.HasValue,a=>a.Status == input.Status); 
            var totalCount = query.Count();

            var list = query.Skip(input.PageIndex * input.PageSize).Take(input.PageSize).ToList();

            var dto = list.Adapt<List<NoticeOutput>>();

            return PageResponseResult<NoticeOutput>.Success("操作成功！",dto, input.PageSize, input.PageIndex, totalCount);
        }

        public async Task<ResponseResult> SendNotice(Guid id)
        {
            var notice = await _noticeRepository.FirstOrDefaultAsync(a=>a.Id== id);
            if(notice == null)
            {
                return ResponseResult.Fail("记录不存在！");
            }
           
             await   _hubContext.Clients.All.SendAsync(HubsConstant.ReceiveNotice, notice.Title, notice.NoticeContent);

            return ResponseResult.Success("操作成功！");
        }

        public async Task<ResponseResult<NoticeOutput>> UpdateAsync(UpdateNoticeInput input)
        {
            var notice = await _noticeRepository.FirstOrDefaultAsync(a => a.Id == input.Id);

            if (notice == null)
            {
                return ResponseResult<NoticeOutput>.Fail("记录不存在！");
            }

            notice.NoticeContent = input.NoticeContent;
            notice.Status = input.Status;
            notice.Title = input.Title;
            notice.Remark = input.Remark;  
           await _noticeRepository.UpdateAsync(notice);
            var dto = notice.Adapt<NoticeOutput>();
            return ResponseResult<NoticeOutput>.Success("操作成功！", dto);
        }

        public async Task<ResponseResult<List<NoticeOutput>>> GetAllAsync()
        {
            var query = (await _noticeRepository.GetQueryableAsync());
            var totalCount = query.Count();

            var list = query.ToList();

            var dto = list.Adapt<List<NoticeOutput>>();

            return ResponseResult<List<NoticeOutput>>.Success("操作成功！", dto);
        }
    }
}
