﻿using Bridge.Shared.Extensions;
using Bridge.Shared.Models;
using Bridge.System.Domain;
using Depts;
using Depts.Input;
using Depts.Output;
using Mapster;
using Microsoft.EntityFrameworkCore;
using Roles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;

namespace Bridge.System.Application
{
    public class DeptService : ApplicationService, IDeptService
    {
        private  IRepository<Dept, Guid> _deptRepository { get; set; }
        public DeptService(IRepository<Dept, Guid> deptRepository) {
            _deptRepository= deptRepository;
        }

        public async Task<ResponseResult<string>> CreateDept(CreateDeptInput input)
        {
            var dept = await _deptRepository.FirstOrDefaultAsync(a => a.DeptName == input.DeptName);
            if(dept !=null)
            {
                return ResponseResult<string>.Fail("部门名已存在！");
            }

            var newdept = new Dept(GuidGenerator.Create())
            {
                DeptName = input.DeptName,
                ParentId = input.ParentId.HasValue ? input.ParentId.Value : Guid.Empty,
                OrderNum = input.OrderNum,
                Phone = input.Phone,
                Leader = input.Leader,
                Email = input.Email,
                Status = 1

            };
           await _deptRepository.InsertAsync(newdept);

            return ResponseResult<string>.Success("操作成功！",newdept.Id.ToString());
        }

        public async Task<ResponseResult> DeleteAsync(IdsInput<Guid> input)
        {
            var depts = (await _deptRepository.GetQueryableAsync()).Where(a => input.Ids.Contains(a.Id)).ToList();

            if (!depts.Any())
            {
                return ResponseResult.Fail("记录不存在！");
            }
            await _deptRepository.DeleteManyAsync(depts);

            return ResponseResult.Success("操作成功！");

        }

        public async Task<ResponseResult<DeptOutput>> GetDeptInfo(Guid id)
        {
            var dept = await _deptRepository.FirstOrDefaultAsync(a => a.Id == id);
            if (dept == null)
            {
                return ResponseResult<DeptOutput>.Fail("记录不存在！");
            }

            var dto = dept.Adapt<DeptOutput>();

            return ResponseResult<DeptOutput>.Success("操作成功！", dto);
        }

        public async Task<PageResponseResult<DeptTreeOutput>> GetDeptList(GetDeptInput input)
        {
            var query = (await _deptRepository.GetQueryableAsync()).WhereIf(!string.IsNullOrEmpty(input.DeptName), a => a.DeptName.Contains(input.DeptName));

            int totalCount = query.Count();
            if (totalCount == 0)
            {
                return PageResponseResult<DeptTreeOutput>.Success("操作成功！",null, input.PageSize, input.PageIndex, totalCount);
            }
            var deptList = await query.ToListAsync();
            var deptTree = deptList.Adapt<List<DeptTreeOutput>>();
           //var deptTree = BindDeptTree(deptList, Guid.Empty);

            return PageResponseResult<DeptTreeOutput>.Success("操作成功！", deptTree,input.PageSize,input.PageIndex,totalCount);
                                                             
        }

        private List<DeptTreeOutput> BindDeptTree(List<Dept> deptList, Guid parentId)
        {

            var deptTrees = new List<DeptTreeOutput>();
            
            if(!deptList.Any())
            {
                return deptTrees;
            }
            foreach (var item in deptList)
            {
                if(parentId == item.ParentId)
                {
                    DeptTreeOutput deptTree = new DeptTreeOutput()
                    {
                        DeptName = item.DeptName,
                        ParentId = item.ParentId,
                        Id = item.Id,
                        Status = item.Status,
                        OrderNum = item.OrderNum
                    };
                    deptTree.Children = BindDeptTree(deptList,item.Id);
                    deptTrees.Add(deptTree);
                }
            }

            return deptTrees;
        }

        public async Task<ResponseResult> UpdateDept(UpdateDeptInput input)
        {
            var dept =await _deptRepository.FirstOrDefaultAsync(a=>a.Id == input.Id);
            if(dept == null)
            {
                return ResponseResult.Fail("操作失败！");
            }

            dept.DeptName = input.DeptName;
            dept.Phone = input.Phone;
            dept.Status = input.Status;
            dept.Leader = input.Leader;
            dept.Email = input.Email;
            await _deptRepository.UpdateAsync(dept);

            return ResponseResult.Success("操作成功！");
        }
    }
}
