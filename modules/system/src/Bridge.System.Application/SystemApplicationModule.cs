﻿using Bridge.System.Application;
using System;
using Volo.Abp.AspNetCore.SignalR;
using Volo.Abp.AutoMapper;
using Volo.Abp.Modularity;


[DependsOn(
    typeof(AbpAspNetCoreSignalRModule),
    typeof(SystemDomainModule),
    typeof(SystemApplicationContractsModule),
    typeof(AbpAutoMapperModule)
    )]
public class SystemApplicationModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        Configure<AbpAutoMapperOptions>(options =>
        {
            options.AddProfile<SystemApplicationAutoMapperProfile>(validate: true);
        });

        #region SignalR配置

        Configure<AbpSignalROptions>(options =>
        {
            options.Hubs.AddOrUpdate(
                typeof(MessageHub), //Hub type
                config => //Additional configuration
                {
                    config.RoutePattern = "/api/v1/sys/signalr-hubs"; //override the default route
                    config.ConfigureActions.Add(hubOptions =>
                    {
                        //Additional options
                        hubOptions.LongPolling.PollTimeout = TimeSpan.FromSeconds(30);
                    });
                }
            );
        });
        #endregion
    }
}

