﻿using Bridge.System.Domain;
using Bridge.Shared.Models;
using DictDatas;
using DictDatas.Input;
using DictDatas.Output;
using Mapster;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using NPOI.OpenXmlFormats.Wordprocessing;

namespace Bridge.System.Application
{
    public class DictDataService : ApplicationService, IDictDataService
    {
        private readonly IRepository<DictData, Guid> _dictDataRepository;
        public DictDataService(IRepository<DictData, Guid> dictDataRepository) 
        { 
           this._dictDataRepository = dictDataRepository;
        }
        public async Task<ResponseResult<List<DictDataOutput>>> GetDictDataByDictType(string dictType)
        {
            var dict = (await _dictDataRepository.GetQueryableAsync()).WhereIf(!string.IsNullOrEmpty(dictType), a => a.DictType == dictType).ToList();

            var dto = dict.Adapt<List<DictDataOutput>>();
            return ResponseResult<List<DictDataOutput>>.Success("ok", dto);
        }
        public async Task<ResponseResult<List<DictDataOutput>>> GetDictData(DictDataInput input)
        {
            var dict = (await _dictDataRepository.GetQueryableAsync()).WhereIf(!string.IsNullOrEmpty(input.DictType), a => a.DictType == input.DictType).ToList();

            var dto = dict.Adapt<List<DictDataOutput>>();
            return ResponseResult<List<DictDataOutput>>.Success("ok", dto);
        }
        public async Task<ResponseResult> CreateOrUpdate(DictDataInput input)
        {
            DictData dict = new DictData();
            if (!input.Id.HasValue)
            {
               dict = new DictData(GuidGenerator.Create());
                dict.DictName = input.DictName;
                dict.DictType = input.DictType;
                dict.DictValue = input.DictValue;
                dict.Status = input.Status;
               await _dictDataRepository.InsertAsync(dict);
            }
            else
            {
                 dict = await _dictDataRepository.FirstOrDefaultAsync(a => a.Id == input.Id);
                if(dict == null)
                {
                    return ResponseResult.Fail("记录不存在！");
                }
                dict.DictName = input.DictName;
                dict.DictType = input.DictType;
                dict.DictValue = input.DictValue;
                dict.Status = input.Status;

                await _dictDataRepository.UpdateAsync(dict);
            }

           
            return ResponseResult.Success("ok");
        }

        public async Task<PageResponseResult<DictDataOutput>> GetDictList(GetDictDataInput input)
        {
            try
            {

                var dto = new List<DictDataOutput>();
                var query = (await _dictDataRepository.GetQueryableAsync())
                            .WhereIf(!string.IsNullOrEmpty(input.DictName), a => a.DictName.Contains(input.DictName))
                            .WhereIf(!string.IsNullOrEmpty(input.DictType), a => a.DictType.Contains(input.DictType))
                            .WhereIf(input.Status.HasValue,a=>a.Status == input.Status)
                            .WhereIf(input.StartTime.HasValue, a => a.CreationTime > input.StartTime)
                            .WhereIf(input.EndTime.HasValue, a => a.CreationTime > input.EndTime);

                int totalCount = await _dictDataRepository.CountAsync();

                if (totalCount == 0)
                {
                    return PageResponseResult<DictDataOutput>.Success("操作成功", null, input.PageSize, input.PageIndex, 0);
                }

                var list = await query.Skip(input.PageIndex * input.PageSize).Take(input.PageSize).ToListAsync();

                dto = list.Adapt<List<DictDataOutput>>();

                return PageResponseResult<DictDataOutput>.Success("操作成功", dto, input.PageSize, input.PageIndex, totalCount);


            }
            catch (Exception ex)
            {
                return PageResponseResult<DictDataOutput>.Fail($"操作失败，原因：{ex.Message}");
            }

        }

        public async Task<ResponseResult> DeleteAsync(IdsInput<Guid> input)
        {
            var dicts = (await _dictDataRepository.GetQueryableAsync()).Where(a => input.Ids.Contains(a.Id));
            if (!dicts.Any())
            {
                return ResponseResult.Fail("记录不存在！");
            }

            await _dictDataRepository.DeleteManyAsync(dicts);
            return PageResponseResult<ResponseResult>.Success("操作成功！");
        }

        public async Task<ResponseResult<DictDataOutput>> GetDictInfo(Guid id)
        {
            var dict =await _dictDataRepository.FirstOrDefaultAsync(a => a.Id == id);
            if (dict == null)
            {
                return ResponseResult<DictDataOutput>.Fail("记录不存在！");
            }

            var dto = dict.Adapt<DictDataOutput>();
            return ResponseResult<DictDataOutput>.Success("操作成功！", dto);
        }
    }
}
