﻿using Bridge.Shared.Models;
using Bridge.System.Domain;
using Depts;
using GenTables.Output;
using GenTables;
using GenTables.Input;
using GenTables.Output;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using Mapster;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Volo.Abp.EntityFrameworkCore;

namespace Bridge.System.Application
{
    public class GenTableService : ApplicationService, IGenTableService
    {
        
        private readonly IRepository<GenTable, Guid> _genTableRepository;
        private readonly IRepository<GenTableColumn, Guid> _genTableColumnRepository;
        public GenTableService(IRepository<GenTable, Guid> genTableRepository, IRepository<GenTableColumn, Guid> genTableColumnRepository) {
            _genTableRepository= genTableRepository ;
            _genTableColumnRepository= genTableColumnRepository;

        }
        public Task<ResponseResult> CreateOrUpdate(GenTableInput input)
        {
            throw new NotImplementedException();
        }

        public async Task<ResponseResult> DeleteAsync(IdsInput<Guid> input)
        {
            var list =await (await _genTableRepository.GetQueryableAsync()).Where(a=>input.Ids.Contains(a.Id)).ToListAsync();

            if(!list.Any())
            {
                return ResponseResult.Fail("记录不存在！");
            }

            await _genTableRepository.DeleteManyAsync(list);

            return ResponseResult.Success("操作成功！");

        }

        public async Task<ResponseResult<List<string>>> GetDbList()
        {
            var list = new List<string>();
   
            return ResponseResult<List<string>>.Success("操作成功！",list);
        }

        public Task<ResponseResult<List<GenTableOutput>>> GetGenTable(GenTableInput input)
        {
            throw new NotImplementedException();
        }

       

        public async Task<ResponseResult<GenTableOutput>> GetGenTableInfo(Guid id)
        {
            var genTable =await _genTableRepository.FirstOrDefaultAsync(x => x.Id == id);
            if(genTable == null)
            {
                return ResponseResult<GenTableOutput>.Fail("记录不存在！");
            }
            var dto = genTable.Adapt<GenTableOutput>();

            return ResponseResult<GenTableOutput>.Success("操作成功！", dto);
        }

        public async Task<PageResponseResult<GenTableOutput>> GetGenTableList(GetGenTableInput input)
        {
            try
            {

                var dto = new List<GenTableOutput>();
                var query = (await _genTableRepository.GetQueryableAsync())
                            .WhereIf(!string.IsNullOrEmpty(input.TableName),a=>a.TableName == input.TableName)
                            .WhereIf(input.StartTime.HasValue, a => a.CreationTime > input.StartTime)
                            .WhereIf(input.EndTime.HasValue, a => a.CreationTime > input.EndTime);

                int totalCount = await _genTableRepository.CountAsync();

                if (totalCount == 0)
                {
                    return PageResponseResult<GenTableOutput>.Success("操作成功", null, input.PageSize, input.PageIndex, 0);
                }

                var list = await query.Skip(input.PageIndex * input.PageSize).Take(input.PageSize).ToListAsync();

                dto = list.Adapt<List<GenTableOutput>>();

                return PageResponseResult<GenTableOutput>.Success("操作成功", dto, input.PageSize, input.PageIndex, totalCount);


            }
            catch (Exception ex)
            {
                return PageResponseResult<GenTableOutput>.Fail($"操作失败，原因：{ex.Message}");
            }
        }
    }

}
