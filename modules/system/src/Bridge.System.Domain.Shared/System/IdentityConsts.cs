﻿namespace Bridge.System
{
    public static class IdentityConsts
    {
        public const int MaxCodeLength = 32;

        public const int MaxNameLength = 256;

        public const int MaxImageNameLength = 128;
    }
}
