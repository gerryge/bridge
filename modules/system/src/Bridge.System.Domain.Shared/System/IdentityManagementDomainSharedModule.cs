﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Modularity;
using Volo.Abp.Localization;
using Bridge.System.Localization;
namespace Bridge.System
{
    [DependsOn(
        typeof(AbpLocalizationModule)
        )]
    public class SystemDomainSharedModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            Configure<AbpLocalizationOptions>(options =>
            {
                options.Resources.Add<SystemResource>("en");
            });
        }
    }
}
