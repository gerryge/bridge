﻿using Volo.Abp.Localization;

namespace Bridge.System.Localization
{
    [LocalizationResourceName("System")]
    public class SystemResource
    {
        
    }
}
