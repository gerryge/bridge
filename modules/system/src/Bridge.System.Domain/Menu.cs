﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Entities.Auditing;
using Volo.Abp.MultiTenancy;
using Volo.Abp;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using NLog.LayoutRenderers;

namespace Bridge.System.Domain
{
    [Comment("菜单表")]
    [Table("t_sys_menu")]
    public class Menu : AuditedAggregateRoot<Guid>, ISoftDelete, IMultiTenant
    {
        public Menu() { }

        public Menu(Guid key)
        {
          this.Id = key;
        }
        [MaxLength(200)]
        [Comment("菜单名")]
        public string Name { get; set; }
       
        [MaxLength(36)]
        [Comment("父级菜单标识")]
        public Guid ParentId { get; set; }


        [MaxLength(200)]
        [Comment("图标路径")]
        public string Icon { get; set; }

        [MaxLength(200)]
        [Comment("跳转路径")]
        public string Path { get; set; }

        [Comment("客户端类型 0 APP 1 web")]
        public int ClientType { get; set; }


        /// <summary>
        /// 类型（M目录 C菜单 F按钮 L链接）
        /// </summary>
        [MaxLength(1)]
        public string MenuType { get; set; } = string.Empty;


        public string Component { get; set; }
     
        [Comment("排序")]
        public int Sort { get; set; }
      
        [Comment("是否启用")]
        public bool IsActive { get; set; }

        [Comment("权限字符串")]
        [MaxLength(200)]
        public string Perms { get; set; }
        /// <summary>
        /// 是否外链 1、是 0、否
        /// </summary>
        public string IsFrame { get; set; } = "0";

        [Comment("是否删除")]
        public bool IsDeleted { get; set; }
  
        [Comment("租户Id")]
        public Guid? TenantId { set; get; }

       
        [Comment("用户归属系统Id")]
        [MaxLength(20)]
        public string SystemId { get; set; }
    
        [Comment("备注")]
        [MaxLength(500)]
        public string Remark { get; set; }
    }
}
