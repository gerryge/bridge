﻿using Volo.Abp;


public class SystemCodeAlreadyExistsException : BusinessException
{
    public SystemCodeAlreadyExistsException(string productCode)
        : base("PM:000001", $"A product with code {productCode} has already exists!")
    {

    }
}
