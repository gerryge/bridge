﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Entities.Auditing;
using Volo.Abp.MultiTenancy;
using Volo.Abp;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Cors.Infrastructure;
using System.ComponentModel;

namespace Bridge.System.Domain
{
    [Comment("代码生成表")]
    [Table("t_sys_gentable")]
    public class GenTable : AuditedAggregateRoot<Guid>, ISoftDelete, IMultiTenant
    {
        public GenTable() { }

        public GenTable(Guid key)
        {
            this.Id = key;
        }
        /// <summary>
        /// 数据库名
        /// </summary>
        [MaxLength(50)]
        [Comment("数据库名称")]
        public string DbName { get; set; }
        [MaxLength(100)]
        [Comment("表名")]
        public string TableName { get; set; }
        [MaxLength(200)]
        [Comment("表描述")]
        public string TableComment { get; set; }
        /// <summary>
        /// 关联父表的表名
        /// </summary>
        [MaxLength(150)]
        [Comment("关联父表的表名")]
        public string SubTableName { get; set; }
        /// <summary>
        /// 本表关联父表的外键名
        /// </summary>
        [MaxLength(150)]
        [Comment("本表关联父表的外键名")]
        public string SubTableFkName { get; set; }
        /// <summary>
        /// csharp类名
        /// </summary>
        [MaxLength(150)]
        [Comment("csharp类名")]
        public string ClassName { get; set; }
        /// <summary>
        /// 使用的模板（crud单表操作 tree树表操作 sub主子表操作）
        /// </summary>
        [MaxLength(50)]
        [Comment("使用的模板")]
        public string TplCategory { get; set; }
        /// <summary>
        /// 基本命名空间前缀
        /// </summary>
        [MaxLength(150)]
        [Comment("基本命名空间前缀")]
        public string BaseNameSpace { get; set; }
        /// <summary>
        /// 生成模块名
        /// </summary>
        [MaxLength(50)]
        [Comment("生成模块名")]
        public string ModuleName { get; set; }
        /// <summary>
        /// 生成业务名
        /// </summary>
        [MaxLength(50)]
        [Comment("生成业务名")]
        public string BusinessName { get; set; }
        /// <summary>
        /// 生成功能名
        /// </summary>
        [Comment("生成功能名")]
        [MaxLength(100)]
        public string FunctionName { get; set; }
        /// <summary>
        /// 生成作者名
        /// </summary>
        [Comment("生成作者名")]
        [MaxLength(100)]
        public string FunctionAuthor { get; set; }
        /// <summary>
        /// 生成代码方式（0zip压缩包 1自定义路径）
        /// </summary>
        [MaxLength(1)]
        [Comment("生成代码方式")]
        public string GenType { get; set; }
        /// <summary>
        /// 代码生成保存路径
        /// </summary>
        [DefaultValue("/")]
        [MaxLength(10)]
        [Comment("代码生成保存路径")]
        public string GenPath { get; set; }
        /// <summary>
        /// 其他生成选项
        /// </summary>
        [NotMapped]
        public CodeOptions Options { get; set; }

        #region 表额外字段
        /// <summary>
        /// 表列信息
        /// </summary>
        [NotMapped]
        public List<GenTableColumn> Columns { get; set; }

        /// <summary>
        /// 字表信息
        /// </summary>
        [NotMapped]
        public GenTable SubTable { get; set; }
        #endregion
        public bool IsDeleted { get; set; }



        public Guid? TenantId { get; set; }
    }

    public class CodeOptions
    {
        public long ParentMenuId { get; set; }
        public string SortType { get; set; } = "asc";
        public string SortField { get; set; } = string.Empty;
        public string TreeCode { get; set; } = string.Empty;
        public string TreeName { get; set; } = string.Empty;
        public string TreeParentCode { get; set; } = string.Empty;
        public string PermissionPrefix { get; set; } = string.Empty;
        /// <summary>
        /// 额外参数字符串
        /// </summary>
        public int[] CheckedBtn { get; set; } = new int[] { 1, 2, 3 };
        /// <summary>
        /// 列大小 12,24
        /// </summary>
        public int ColNum { get; set; } = 12;
        /// <summary>
        /// 是否生成仓储层
        /// </summary>
        public int GenerateRepo { get; set; }
        /// <summary>
        /// 自动生成菜单
        /// </summary>
        public bool GenerateMenu { get; set; }
        /// <summary>
        /// 操作按钮样式
        /// </summary>
        public int OperBtnStyle { get; set; } = 1;
        /// <summary>
        /// 是否使用雪花id
        /// </summary>
        public bool UseSnowflakeId { get; set; } = false;
        /// <summary>
        /// 是否启用日志(编辑、删除)自动记录日志
        /// </summary>
        public bool EnableLog { get; set; }
        /// <summary>
        /// 前端模板 1、element ui 2、element plus
        /// </summary>
        public int FrontTpl { get; set; } = 2;
    }
}
