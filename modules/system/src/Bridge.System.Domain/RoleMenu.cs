﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Entities.Auditing;
using Volo.Abp;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Bridge.System.Domain
{

    [Comment("角色菜单中间表")]
    [Table("t_sys_role_menu")]
    public class RoleMenu : AuditedAggregateRoot<Guid>, ISoftDelete
    {
       
        public RoleMenu() { }
        public RoleMenu(Guid Id)
        {
            this.Id = Id;
        }
        [MaxLength(36)]
        [Comment("角色标识")]
        public Guid RoleId { get; set; }

       
        [MaxLength(36)]
        [Comment("菜单标识")]
        public Guid MenuId { get; set; }
        

        [Comment("是否删除")]
        public bool IsDeleted { get; set; }
    }
}
