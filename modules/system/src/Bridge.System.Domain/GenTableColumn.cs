﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Entities.Auditing;
using Volo.Abp.MultiTenancy;
using Volo.Abp;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations.Schema;

namespace Bridge.System.Domain
{
    public class GenTableColumn : AuditedAggregateRoot<Guid>, ISoftDelete, IMultiTenant
    {
        public GenTableColumn() { }
        public GenTableColumn(Guid key)
        {
            this.Id = key;
        }
        /// <summary>
        /// 导入代码生成表列名 首字母转了小写
        /// </summary>
        [MaxLength(100)]
        [Comment("列名")]
        public string ColumnName { get; set; }
        [Comment("表标识")]
        public Guid TableId { get; set; }

       [MaxLength(50)]
        [Comment("表名")]
        public string TableName { get; set; }
        

        /// <summary>
        /// 列说明
        /// </summary>
        [Comment("列说明")]
        [MaxLength(500)]
        public string ColumnComment
        {
            get
            {
                return string.IsNullOrEmpty(ColumnComment) ? CsharpField : ColumnComment;
            }
            
        }
        /// <summary>
        /// 数据库列类型
        /// </summary>

       [MaxLength(50)]
        [Comment("数据库列类型")]
        public string ColumnType { get; set; }
        /// <summary>
        /// C#类型
        /// </summary>
        [MaxLength(50)]
        [Comment("C#类型")]
        public string CsharpType { get; set; }
        /// <summary>
        /// C# 字段名 首字母大写
        /// </summary>
        [MaxLength(50)]
        [Comment("字段名")]
        public string CsharpField { get; set; }
        /// <summary>
        /// 是否主键（1是）
        /// </summary>
        [Comment("是否主键（1是 0否）")]
        public bool IsPk { get; set; }
        /// <summary>
        /// 是否必填（1是）
        /// </summary>
        [Comment("是否必填（1是）")]
        public bool IsRequired { get; set; }
        /// <summary>
        /// 是否自增（1是）
        /// </summary>
        [Comment("是否自增（1是）")]
        public bool IsIncrement { get; set; }
        /// <summary>
        /// 是否插入（1是）
        /// </summary>
        [Comment("是否插入（1是）")]
        public bool IsInsert { get; set; }
        /// <summary>
        /// 是否需要编辑（1是）
        /// </summary>
        [Comment("是否需要编辑（1是）")]
        public bool IsEdit { get; set; }
        /// <summary>
        /// 是否显示列表（1是）
        /// </summary>
        [Comment("是否显示列表（1是）")]
        public bool IsList { get; set; }
        /// <summary>
        /// 是否查询（1是）
        /// </summary>
        [Comment("是否查询（1是）")]
        public bool IsQuery { get; set; }
        /// <summary>
        /// 是否排序（1是）
        /// </summary>
        [Comment("是否排序（1是）")]
        public bool IsSort { get; set; }
        /// <summary>
        /// 是否导出（1是）
        /// </summary>
        [Comment("是否导出（1是）")]
        public bool IsExport { get; set; }
        /// <summary>
        /// 显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）
        /// </summary>
        [Comment("显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）")]
        public string HtmlType { get; set; }
        /// <summary>
        /// 查询类型（等于、不等于、大于、小于、范围）
        /// </summary>
        [MaxLength(50)]
        [DefaultValue("EQ")]
        [Comment("查询类型（等于、不等于、大于、小于、范围）")]
        public string QueryType { get; set; } = "EQ";
        public int Sort { get; set; }
        /// <summary>
        /// 字典类型
        /// </summary>
        [MaxLength (50)]
        [Comment("字典类型")]
        public string DictType { get; set; } = "";
        /// <summary>
        /// 自动填充类型 1、添加 2、编辑 3、添加编辑
        /// </summary>
        [Comment("自动填充类型 1、添加 2、编辑 3、添加编辑")]
        public int AutoFillType { get; set; }

        [NotMapped]
        public string RequiredStr
        {
            get
            {
                string[] arr = new string[] { "int", "long" };
                return (!IsRequired && HtmlType != "selectMulti" && (arr.Any(f => f.Contains(CsharpType))) || typeof(DateTime).Name == CsharpType) ? "?" : "";
            }
        }
        /// <summary>
        /// 前端排序字符串
        /// </summary>
        [NotMapped]
        public string SortStr
        {
            get
            {
                return IsSort ? " sortable" : "";
            }
        }
        /// <summary>
        /// C# 字段名 首字母小写，用于前端
        /// </summary>
        [NotMapped]
        public string CsharpFieldFl
        {
            get
            {
                return CsharpField[..1].ToLower() + CsharpField[1..];
            }
        }
        /// <summary>
        /// 前端 只读字段
        /// </summary>
        [NotMapped]
        public string DisabledStr
        {
            get
            {
                return ((IsPk) && !IsRequired) ? " :disabled=\"true\"" : "";
            }
        }

        public bool IsDeleted {get; set;}

        public Guid? TenantId {get; set;}
    }
}
