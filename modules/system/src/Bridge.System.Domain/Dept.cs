﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Entities.Auditing;
using Volo.Abp.MultiTenancy;
using Volo.Abp;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Bridge.System.Domain
{
    [Comment("部门表")]
    [Table("t_sys_dept")]
    public class Dept : AuditedAggregateRoot<Guid>, ISoftDelete, IMultiTenant
    {
        public Dept() { }
        public Dept(Guid parentId, string deptName, int orderNum, string phone, int status)
        {
            ParentId = parentId;
            DeptName = deptName;
            OrderNum = orderNum;
            Phone = phone;
            Status = status;
        }
        public Dept(Guid key)
        {
            this.Id = key;
        }

        /// <summary>
        /// 父部门标识
        /// </summary>
        public Guid ParentId { get; set; }
        /// <summary>
        /// 部门名称
        /// </summary>
        [MaxLength(200)]
        public string DeptName { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int OrderNum { get; set; }
        /// <summary>
        /// 负责人
        /// </summary>
        [MaxLength(50)]
        public string Leader { get; set; }
        /// <summary>
        /// 联系电话
        /// </summary>
        [MaxLength(11)]
        public string Phone { get; set; }

        [MaxLength(50)]
        public string Email { get; set; }

        public int Status { get; set; }

        public bool IsDeleted { get; set; }

        public Guid? TenantId {get;set;}
    }
}
