﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Entities.Auditing;
using Volo.Abp.MultiTenancy;
using Volo.Abp;
using System.ComponentModel.DataAnnotations;

namespace Bridge.System.Domain
{
    /// <summary>
    /// 页面操作权限
    /// </summary>
    public class Authority : AuditedAggregateRoot<Guid>, ISoftDelete, IMultiTenant
    {
        /// <summary>
        /// 页面代码
        /// </summary>
        [MaxLength(64)]
        public string PageCode { get; set; }

        /// <summary>
        /// 页面名
        /// </summary>
        [MaxLength(64)]
        public string PageName { get; set; }

        /// <summary>
        /// 菜单Id
        /// </summary>
        public Guid? MenuId { get; set; }

        /// <summary>
        /// add/del/update/disable等等操作
        /// </summary>
        [MaxLength(64)]
        public string Operate { get; set; }

        /// <summary>
        /// 新增/删除/修改/禁用等等
        /// </summary>
        [MaxLength(64)]
        public string OperateName { get; set; }
        /// <summary>
        /// 是否删除
        /// </summary>
        public bool IsDeleted { get; set; }
        /// <summary>
        /// 租户Id
        /// </summary>
        public Guid? TenantId { get; set; }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="id"></param>
        /// <param name="tenantId"></param>
        /// <param name="menuId"></param>
        /// <param name="operate"></param>
        /// <param name="operateName"></param>
        public Authority(Guid id, Guid? menuId, string operate, string operateName)
        {
            Id = id;
            MenuId = menuId;
            Operate = operate;
            OperateName = operateName;
        }
    }
}
