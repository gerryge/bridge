﻿using Bridge.Shared.Enums.IdentityService;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Entities.Auditing;

namespace Bridge.System.Domain
{
    [Comment("用户扩展表")]
    [Table("t_sys_user_expand")]
    public  class UserExpand : AuditedAggregateRoot<Guid>
    {
        public UserExpand(Guid Id, int UserType, Guid UserId, UserStatusEnum userStatus = UserStatusEnum.Enable, int isConfirm = 0)
        {
            this.Id = Id;
            this.UserId = UserId;
            this.UserType = UserType;
            this.UserStatus = userStatus;
            this.IsConfirm = isConfirm;
        }
     
        [MaxLength(36)]
        [Comment("用户标识")]
        public Guid UserId { get; set; }
       

        [Comment("用户类型")]
        public int UserType { get; set; }


        [Comment("用户状态")]
        public UserStatusEnum UserStatus { get; set; }

     

        [Comment("是否已确认结果，0未确认，1已确认")]
        public int IsConfirm { get; set; }

     
        [MaxLength(200)]
        [Comment("用户图像")]
        public string HeadFileUrl { get; set; }

     
        [MaxLength(20)]
        [Comment("归属系统Id")]
        public string SystemId { get; set; }
        [Comment("锁定状态")]
        public bool? IsLock { get; set; }
 
        [Comment("登录失败次数")]
        public int? LoginFailedCount { get; set; }

        [Comment("锁定时间")]
        public DateTime? LockTime { get; set; }


        [Comment("锁定人")]
        public Guid? LockUserId { get; set; }

       
        [Comment("解锁时间")]
        public DateTime? UnLockTime { get; set; }

     
        [Comment("解锁人")]
        public Guid? UnLockUserId { get; set; }
    }
}
