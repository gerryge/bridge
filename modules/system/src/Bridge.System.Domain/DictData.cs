﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Entities.Auditing;
using Volo.Abp;
using System.ComponentModel.DataAnnotations;
using Volo.Abp.MultiTenancy;

namespace Bridge.System.Domain
{
    [Comment("字典表")]
    [Table("t_sys_dictdata")]
    public class DictData : AuditedAggregateRoot<Guid>, ISoftDelete, IMultiTenant
    {
        public DictData() { }

        public DictData(Guid Id)
        {
            this.Id = Id;
        }
        /// <summary>
        /// 字典名称
        /// </summary>
        [MaxLength(200)]
        [Comment("字典名称")]
        public string DictName { get; set; }
        /// <summary>
        /// 字典值
        /// </summary>
        [MaxLength(200)]
        [Comment("字典值")]
        public int DictValue { get; set; }
        /// <summary>
        /// 字典类型
        /// </summary>
        [MaxLength(200)]
        [Comment("字典类型")]
        public string DictType { get; set; }
        /// <summary>
        /// 状态（0正常 1停用）
        /// </summary>
        [Comment("状态")]
        public int Status { get; set; }
        /// <summary>
        /// 字典排序
        /// </summary>
         [Comment("排序")]
        public int DictSort { get; set; }
        [Comment("是否删除")]
        public bool IsDeleted { get; set; } = false;
        [Comment("租户Id")]
        public Guid? TenantId { get; set; }
    }
}
