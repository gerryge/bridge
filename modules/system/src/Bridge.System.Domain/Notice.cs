﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Entities.Auditing;
using Volo.Abp.MultiTenancy;
using Volo.Abp;
using System.ComponentModel.DataAnnotations;

namespace Bridge.System.Domain
{
    [Comment("通知通告")]
    [Table("t_sys_notice")]
    public class Notice : AuditedAggregateRoot<Guid>, ISoftDelete, IMultiTenant
    {
        public Notice() { }
        public Notice(Guid key)
        {
            this.Id = key;
        }
        [Comment("标题")]
        [MaxLength(200)]
        public string Title { get; set; }
        /// <summary>
        /// 公告类型 (1通知 2公告)
        /// </summary>
        public int NoticeType { get; set; }


        [Comment("内容")]
        public string NoticeContent { get; set; }

        /// <summary>
        /// 公告状态 (0正常 1关闭)
        /// </summary>
        public int Status { get; set; }
        [Comment("创建人")]
        [MaxLength(50)]
        public string CreateBy { get; set; }
        [Comment("是否删除")]
        public bool IsDeleted { get; set; }

        [Comment("租户Id")]
        public Guid? TenantId { set; get; }
        [Comment("备注")]
        [MaxLength(500)]
        public string Remark { get; set; }
    }
}
