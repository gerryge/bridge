﻿using Microsoft.AspNetCore.Mvc;

namespace Bridge.PublicWebSiteGateway.Host.Controller
{
    public class HomeController : ControllerBase
    {
        public ActionResult Index()
        {
            return Redirect("/swagger");
        }
    }
}
