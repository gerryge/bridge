#!/bin/bash




set -e

DIR="$(cd "$(dirname "$0")" && pwd)"
source $DIR/common.sh

set +o noglob

item=0

#拷资料卷开始
if [ ! -d "/home/data" ]
then

 mkdir /home/data

 
fi

cp -a ${DIR}/oms-volume /home/data
cp -a ${DIR}/database /home/data
#拷资料卷结束

workdir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $workdir

h2 "[Step $item]: checking if docker is installed ..."; let item+=1
check_docker

h2 "[Step $item]: checking docker-compose is installed ..."; let item+=1
check_dockercompose

h2 "[Step $item]: preparing environment ...";  let item+=1
if [ -f "install.conf" ]
then
	cat ${DIR}/install.conf > .env 
	source ${DIR}/install.conf
	source ${DIR}/.env 
    
else
	error "install.conf file does not exist"
	exit 1
fi

h2 "[Step $item]: loading OMS images ..."; let item+=1
if [ -f spd*.tar ]
then
    docker load -i ./spd*.tar
else
	docker login -u=${OMS_DOCKER_USER} -p=${OMS_DOCKER_PASSWORD} ${OMS_DOCKER_HOST}
fi
echo ""

if [ -n "$(docker-compose ps -q)"  ]
then
    note "stopping existing OMS instance ..." 
    docker-compose down -v
fi
echo ""

h2 "[Step $item]: starting OMS ..."
docker-compose up -d

success $"----OMS has been installed and started successfully.----"
