#!/bin/bash
#docker version: 17.06.0+
#docker-compose version: 1.18.0+

DIR="$(cd "$(dirname "$0")" && pwd)"

set +e
set -o noglob

#
# Set Colors
#

bold=$(tput bold)
underline=$(tput sgr 0 1)
reset=$(tput sgr0)

red=$(tput setaf 1)
green=$(tput setaf 76)
white=$(tput setaf 7)
tan=$(tput setaf 202)
blue=$(tput setaf 25)

#
# Headers and Logging
#

underline() { printf "${underline}${bold}%s${reset}\n" "$@"
}
h1() { printf "\n${underline}${bold}${blue}%s${reset}\n" "$@"
}
h2() { printf "\n${underline}${bold}${white}%s${reset}\n" "$@"
}
debug() { printf "${white}%s${reset}\n" "$@"
}
info() { printf "${white}➜ %s${reset}\n" "$@"
}
success() { printf "${green}✔ %s${reset}\n" "$@"
}
error() { printf "${red}✖ %s${reset}\n" "$@"
}
warn() { printf "${tan}➜ %s${reset}\n" "$@"
}
bold() { printf "${bold}%s${reset}\n" "$@"
}
note() { printf "\n${underline}${bold}${blue}Note:${reset} ${blue}%s${reset}\n" "$@"
}

set -e

function check_docker {
	if ! docker --version &> /dev/null
	then
		install_docker
	#	error "Need to install docker(17.06.0+) first and run this script again."
	#	exit 1
	fi

	# docker has been installed and check its version
	if [[ $(docker --version) =~ (([0-9]+)\.([0-9]+)([\.0-9]*)) ]]
	then
		docker_version=${BASH_REMATCH[1]}
		docker_version_part1=${BASH_REMATCH[2]}
		docker_version_part2=${BASH_REMATCH[3]}

		note "docker version: $docker_version"
		# the version of docker does not meet the requirement
		if [ "$docker_version_part1" -lt 17 ] || ([ "$docker_version_part1" -eq 17 ] && [ "$docker_version_part2" -lt 6 ])
		then
			error "Need to upgrade docker package to 17.06.0+."
		fi
	else
		error "Failed to parse docker version."
		exit 1
	fi
}

function check_dockercompose {
	if ! docker-compose --version &> /dev/null
	then
		install_docker_compose
	#	error "Need to install docker-compose(1.18.0+) by yourself first and run this script again."
	#	exit 1
	fi

	# docker-compose has been installed, check its version
	if [[ $(docker-compose --version) =~ (([0-9]+)\.([0-9]+)([\.0-9]*)) ]]
	then
		docker_compose_version=${BASH_REMATCH[1]}
		docker_compose_version_part1=${BASH_REMATCH[2]}
		docker_compose_version_part2=${BASH_REMATCH[3]}

		note "docker-compose version: $docker_compose_version"
		# the version of docker-compose does not meet the requirement
		if [ "$docker_compose_version_part1" -lt 1 ] || ([ "$docker_compose_version_part1" -eq 1 ] && [ "$docker_compose_version_part2" -lt 18 ])
		then
			error "Need to upgrade docker-compose package to 1.18.0+."
			exit 1
		fi
	else
		error "Failed to parse docker-compose version."
		exit 1
	fi
}

# install docker 
function install_docker {
	if [ ! -d "/etc/docker/" ];
	then
		mkdir /etc/docker
	else
		echo "Folder already exists"
	fi
	cp daemon.json /etc/docker/

	if [[ -d docker ]]; then
	  info "... offline install docker"
	  cp docker/* /usr/bin/
	  cp docker.service /usr/lib/systemd/system/
	  chmod +x /usr/bin/docker*
	  chmod 754 /usr/lib/systemd/system/docker.service
    else
	   info "... online install docker"
	   curl -fsSL https://get.docker.com -o get-docker.sh 2>&1 | tee -a ${DIR}/install.log
	   sudo sh get-docker.sh --mirror Aliyun 2>&1 | tee -a ${DIR}/install.log
    fi
	
	if [ ! -f "/usr/lib/systemd/system/docker.service" ];
	then
		cp docker.service /usr/lib/systemd/system/
	else
		echo "file already exists"
	fi
	
	info "... start docker"
	systemctl daemon-reload && systemctl start docker 2>&1 | tee -a ${DIR}/install.log
	
	systemctl enable docker	
	
	docker ps 1>/dev/null 2>/dev/null
	if [ $? != 0 ];then
		error "Docker 未正常启动，请先安装并启动 Docker 服务后再次执行本脚本"
		exit 1
	fi
}

function install_docker_compose {
	if [[ -d docker ]]; then
		info "... offline docker-compose"
		cp docker/bin/docker-compose /usr/bin/
		chmod +x /usr/bin/docker-compose
    else
		info "... online docker-compose"
		curl -L https://get.daocloud.io/docker/compose/releases/download/v2.2.3/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose 2>&1 | tee -a ${DIR}/install.log
		chmod +x /usr/local/bin/docker-compose
		ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
    fi
   
   docker-compose version 1>/dev/null 2>/dev/null
	if [ $? != 0 ];then
	   error "docker-compose 未正常安装，请先安装 docker-compose 后再次执行本脚本"
	   exit 1
	fi
}