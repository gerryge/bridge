﻿using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore;
using Pomelo.EntityFrameworkCore.MySql.Infrastructure;

namespace Bridge.EFCoreMigration.Shared.MigrationDbContext.Identity
{
    public class SystemEFCoreDbContextFactory : IDesignTimeDbContextFactory<SystemEFCoreMigrationDbContext>
    {

        public SystemEFCoreMigrationDbContext CreateDbContext(string[] args)
        {

            var configuration = BuildConfiguration();

            var builder = new DbContextOptionsBuilder<SystemEFCoreMigrationDbContext>()
                .UseMySql(configuration.GetConnectionString("Default"),

                ServerVersion.AutoDetect(configuration.GetConnectionString("Default")), o => o.SchemaBehavior(MySqlSchemaBehavior.Ignore));

            return new SystemEFCoreMigrationDbContext(builder.Options);
        }

        private static IConfigurationRoot BuildConfiguration()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false);

            return builder.Build();
        }
    }
}
