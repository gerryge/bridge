﻿using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;
using Volo.Abp.AuditLogging.EntityFrameworkCore;
using Bridge.System.Domain;
using Volo.Abp.EntityFrameworkCore.Modeling;

namespace Bridge.EFCoreMigration.Shared.MigrationDbContext.Identity
{
    public class SystemEFCoreMigrationDbContext : AbpDbContext<SystemEFCoreMigrationDbContext>
    {
        public SystemEFCoreMigrationDbContext(
            DbContextOptions<SystemEFCoreMigrationDbContext> options
            ) : base(options)
        {

        }
        #region 用户权限管理模块
        public DbSet<Menu> Menus { get; set; }
        public DbSet<Authority> Authoritys { get; set; }
        public DbSet<RoleMenu> RoleMenus { get; set; }
        public DbSet<UserExpand> UserExpands { get; set; }
        public DbSet<DictData> DictDatas { get; set; }
        public DbSet<Dept> Depts { get; set; }
        public DbSet<Notice> Notices { get; set; }

        #endregion


        protected override void OnModelCreating(ModelBuilder builder)
        {


            builder.ConfigureSystem();
            base.OnModelCreating(builder);

        }
    }
}
