﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Bridge.EFCoreMigration.Shared.Migrations.System
{
    /// <inheritdoc />
    public partial class gencode : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CreateBy",
                table: "t_sys_notice",
                type: "varchar(50)",
                maxLength: 50,
                nullable: true,
                comment: "创建人")
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "t_sys_gentable",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "char(36)", nullable: false, collation: "ascii_general_ci"),
                    DbName = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true, comment: "数据库名称")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    TableName = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true, comment: "表名")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    TableComment = table.Column<string>(type: "varchar(200)", maxLength: 200, nullable: true, comment: "表描述")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    SubTableName = table.Column<string>(type: "varchar(150)", maxLength: 150, nullable: true, comment: "关联父表的表名")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    SubTableFkName = table.Column<string>(type: "varchar(150)", maxLength: 150, nullable: true, comment: "本表关联父表的外键名")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    ClassName = table.Column<string>(type: "varchar(150)", maxLength: 150, nullable: true, comment: "csharp类名")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    TplCategory = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true, comment: "使用的模板")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    BaseNameSpace = table.Column<string>(type: "varchar(150)", maxLength: 150, nullable: true, comment: "基本命名空间前缀")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    ModuleName = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true, comment: "生成模块名")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    BusinessName = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true, comment: "生成业务名")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    FunctionName = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true, comment: "生成功能名")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    FunctionAuthor = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true, comment: "生成作者名")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    GenType = table.Column<string>(type: "varchar(1)", maxLength: 1, nullable: true, comment: "生成代码方式")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    GenPath = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true, comment: "代码生成保存路径")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    IsDeleted = table.Column<bool>(type: "tinyint(1)", nullable: false, defaultValue: false),
                    TenantId = table.Column<Guid>(type: "char(36)", nullable: true, collation: "ascii_general_ci"),
                    ExtraProperties = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    ConcurrencyStamp = table.Column<string>(type: "varchar(40)", maxLength: 40, nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    CreationTime = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    CreatorId = table.Column<Guid>(type: "char(36)", nullable: true, collation: "ascii_general_ci"),
                    LastModificationTime = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    LastModifierId = table.Column<Guid>(type: "char(36)", nullable: true, collation: "ascii_general_ci")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_t_sys_gentable", x => x.Id);
                },
                comment: "代码生成表")
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "t_sys_gentablecolumn",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "char(36)", nullable: false, collation: "ascii_general_ci"),
                    ColumnName = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true, comment: "列名")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    TableId = table.Column<Guid>(type: "char(36)", nullable: false, comment: "表标识", collation: "ascii_general_ci"),
                    TableName = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true, comment: "表名")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    ColumnType = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true, comment: "数据库列类型")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    CsharpType = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true, comment: "C#类型")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    CsharpField = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true, comment: "字段名")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    IsPk = table.Column<bool>(type: "tinyint(1)", nullable: false, comment: "是否主键（1是 0否）"),
                    IsRequired = table.Column<bool>(type: "tinyint(1)", nullable: false, comment: "是否必填（1是）"),
                    IsIncrement = table.Column<bool>(type: "tinyint(1)", nullable: false, comment: "是否自增（1是）"),
                    IsInsert = table.Column<bool>(type: "tinyint(1)", nullable: false, comment: "是否插入（1是）"),
                    IsEdit = table.Column<bool>(type: "tinyint(1)", nullable: false, comment: "是否需要编辑（1是）"),
                    IsList = table.Column<bool>(type: "tinyint(1)", nullable: false, comment: "是否显示列表（1是）"),
                    IsQuery = table.Column<bool>(type: "tinyint(1)", nullable: false, comment: "是否查询（1是）"),
                    IsSort = table.Column<bool>(type: "tinyint(1)", nullable: false, comment: "是否排序（1是）"),
                    IsExport = table.Column<bool>(type: "tinyint(1)", nullable: false, comment: "是否导出（1是）"),
                    HtmlType = table.Column<string>(type: "longtext", nullable: true, comment: "显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    QueryType = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true, comment: "查询类型（等于、不等于、大于、小于、范围）")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Sort = table.Column<int>(type: "int", nullable: false),
                    DictType = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true, comment: "字典类型")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    AutoFillType = table.Column<int>(type: "int", nullable: false, comment: "自动填充类型 1、添加 2、编辑 3、添加编辑"),
                    IsDeleted = table.Column<bool>(type: "tinyint(1)", nullable: false, defaultValue: false),
                    TenantId = table.Column<Guid>(type: "char(36)", nullable: true, collation: "ascii_general_ci"),
                    ExtraProperties = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    ConcurrencyStamp = table.Column<string>(type: "varchar(40)", maxLength: 40, nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    CreationTime = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    CreatorId = table.Column<Guid>(type: "char(36)", nullable: true, collation: "ascii_general_ci"),
                    LastModificationTime = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    LastModifierId = table.Column<Guid>(type: "char(36)", nullable: true, collation: "ascii_general_ci")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_t_sys_gentablecolumn", x => x.Id);
                })
                .Annotation("MySql:CharSet", "utf8mb4");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "t_sys_gentable");

            migrationBuilder.DropTable(
                name: "t_sys_gentablecolumn");

            migrationBuilder.DropColumn(
                name: "CreateBy",
                table: "t_sys_notice");
        }
    }
}
