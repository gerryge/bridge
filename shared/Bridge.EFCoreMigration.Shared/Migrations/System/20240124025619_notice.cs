﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Bridge.EFCoreMigration.Shared.Migrations.System
{
    /// <inheritdoc />
    public partial class notice : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "IsFrame",
                table: "t_sys_menu",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AlterColumn<int>(
                name: "DictValue",
                table: "t_sys_dictdata",
                type: "int",
                maxLength: 200,
                nullable: false,
                defaultValue: 0,
                comment: "字典值",
                oldClrType: typeof(string),
                oldType: "varchar(200)",
                oldMaxLength: 200,
                oldNullable: true,
                oldComment: "字典值")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "t_sys_dept",
                type: "varchar(50)",
                maxLength: 50,
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AddColumn<string>(
                name: "Leader",
                table: "t_sys_dept",
                type: "varchar(50)",
                maxLength: 50,
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "t_sys_notice",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "char(36)", nullable: false, collation: "ascii_general_ci"),
                    Title = table.Column<string>(type: "varchar(200)", maxLength: 200, nullable: true, comment: "标题")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    NoticeType = table.Column<int>(type: "int", nullable: false),
                    NoticeContent = table.Column<string>(type: "longtext", nullable: true, comment: "内容")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Status = table.Column<int>(type: "int", nullable: false),
                    IsDeleted = table.Column<bool>(type: "tinyint(1)", nullable: false, defaultValue: false, comment: "是否删除"),
                    TenantId = table.Column<Guid>(type: "char(36)", nullable: true, comment: "租户Id", collation: "ascii_general_ci"),
                    Remark = table.Column<string>(type: "varchar(500)", maxLength: 500, nullable: true, comment: "备注")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    ExtraProperties = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    ConcurrencyStamp = table.Column<string>(type: "varchar(40)", maxLength: 40, nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    CreationTime = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    CreatorId = table.Column<Guid>(type: "char(36)", nullable: true, collation: "ascii_general_ci"),
                    LastModificationTime = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    LastModifierId = table.Column<Guid>(type: "char(36)", nullable: true, collation: "ascii_general_ci")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_t_sys_notice", x => x.Id);
                },
                comment: "通知通告")
                .Annotation("MySql:CharSet", "utf8mb4");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "t_sys_notice");

            migrationBuilder.DropColumn(
                name: "IsFrame",
                table: "t_sys_menu");

            migrationBuilder.DropColumn(
                name: "Email",
                table: "t_sys_dept");

            migrationBuilder.DropColumn(
                name: "Leader",
                table: "t_sys_dept");

            migrationBuilder.AlterColumn<string>(
                name: "DictValue",
                table: "t_sys_dictdata",
                type: "varchar(200)",
                maxLength: 200,
                nullable: true,
                comment: "字典值",
                oldClrType: typeof(int),
                oldType: "int",
                oldMaxLength: 200,
                oldComment: "字典值")
                .Annotation("MySql:CharSet", "utf8mb4");
        }
    }
}
