﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Bridge.EFCoreMigration.Shared.Migrations.Identity
{
    /// <inheritdoc />
    public partial class init : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Code",
                table: "t_identity_menu");

            migrationBuilder.DropColumn(
                name: "Level",
                table: "t_identity_menu");

            migrationBuilder.DropColumn(
                name: "MenuInOut",
                table: "t_identity_menu");

            migrationBuilder.AlterColumn<string>(
                name: "MenuType",
                table: "t_identity_menu",
                type: "varchar(1)",
                maxLength: 1,
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int",
                oldComment: "菜单类型 0 APP 1 web")
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AddColumn<int>(
                name: "ClientType",
                table: "t_identity_menu",
                type: "int",
                nullable: false,
                defaultValue: 0,
                comment: "客户端类型 0 APP 1 web");

            migrationBuilder.AddColumn<string>(
                name: "Component",
                table: "t_identity_menu",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AddColumn<string>(
                name: "Perms",
                table: "t_identity_menu",
                type: "varchar(200)",
                maxLength: 200,
                nullable: true,
                comment: "权限字符串")
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "t_identity_dictdata",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "char(36)", nullable: false, collation: "ascii_general_ci"),
                    DictName = table.Column<string>(type: "varchar(200)", maxLength: 200, nullable: true, comment: "字典名称")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    DictValue = table.Column<string>(type: "varchar(200)", maxLength: 200, nullable: true, comment: "字典值")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    DictType = table.Column<string>(type: "varchar(200)", maxLength: 200, nullable: true, comment: "字典类型")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Status = table.Column<int>(type: "int", nullable: false, comment: "状态"),
                    DictSort = table.Column<int>(type: "int", nullable: false, comment: "排序"),
                    IsDeleted = table.Column<bool>(type: "tinyint(1)", nullable: false, defaultValue: false, comment: "是否删除"),
                    TenantId = table.Column<Guid>(type: "char(36)", nullable: true, comment: "租户Id", collation: "ascii_general_ci"),
                    ExtraProperties = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    ConcurrencyStamp = table.Column<string>(type: "varchar(40)", maxLength: 40, nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    CreationTime = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    CreatorId = table.Column<Guid>(type: "char(36)", nullable: true, collation: "ascii_general_ci"),
                    LastModificationTime = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    LastModifierId = table.Column<Guid>(type: "char(36)", nullable: true, collation: "ascii_general_ci")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_t_identity_dictdata", x => x.Id);
                },
                comment: "字典表")
                .Annotation("MySql:CharSet", "utf8mb4");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "t_identity_dictdata");

            migrationBuilder.DropColumn(
                name: "ClientType",
                table: "t_identity_menu");

            migrationBuilder.DropColumn(
                name: "Component",
                table: "t_identity_menu");

            migrationBuilder.DropColumn(
                name: "Perms",
                table: "t_identity_menu");

            migrationBuilder.AlterColumn<int>(
                name: "MenuType",
                table: "t_identity_menu",
                type: "int",
                nullable: false,
                defaultValue: 0,
                comment: "菜单类型 0 APP 1 web",
                oldClrType: typeof(string),
                oldType: "varchar(1)",
                oldMaxLength: 1,
                oldNullable: true)
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AddColumn<int>(
                name: "Code",
                table: "t_identity_menu",
                type: "int",
                maxLength: 50,
                nullable: false,
                defaultValue: 0,
                comment: "菜单编码");

            migrationBuilder.AddColumn<int>(
                name: "Level",
                table: "t_identity_menu",
                type: "int",
                nullable: false,
                defaultValue: 0,
                comment: "层级");

            migrationBuilder.AddColumn<int>(
                name: "MenuInOut",
                table: "t_identity_menu",
                type: "int",
                nullable: false,
                defaultValue: 0,
                comment: "地址类型");
        }
    }
}
