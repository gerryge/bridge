﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Bridge.EFCoreMigration.Shared.Migrations.Identity
{
    /// <inheritdoc />
    public partial class dept : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "t_identity_role_authority");

            migrationBuilder.DropTable(
                name: "t_identity_role_expand");

            migrationBuilder.DropPrimaryKey(
                name: "PK_t_identity_user_expand",
                table: "t_identity_user_expand");

            migrationBuilder.DropPrimaryKey(
                name: "PK_t_identity_role_menu",
                table: "t_identity_role_menu");

            migrationBuilder.DropPrimaryKey(
                name: "PK_t_identity_menu",
                table: "t_identity_menu");

            migrationBuilder.DropPrimaryKey(
                name: "PK_t_identity_dictdata",
                table: "t_identity_dictdata");

            migrationBuilder.RenameTable(
                name: "t_identity_user_expand",
                newName: "t_sys_user_expand");

            migrationBuilder.RenameTable(
                name: "t_identity_role_menu",
                newName: "t_sys_role_menu");

            migrationBuilder.RenameTable(
                name: "t_identity_menu",
                newName: "t_sys_menu");

            migrationBuilder.RenameTable(
                name: "t_identity_dictdata",
                newName: "t_sys_dictdata");

            migrationBuilder.RenameColumn(
                name: "IconUrl",
                table: "t_sys_menu",
                newName: "Icon");

            migrationBuilder.AddPrimaryKey(
                name: "PK_t_sys_user_expand",
                table: "t_sys_user_expand",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_t_sys_role_menu",
                table: "t_sys_role_menu",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_t_sys_menu",
                table: "t_sys_menu",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_t_sys_dictdata",
                table: "t_sys_dictdata",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "t_sys_dept",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "char(36)", nullable: false, collation: "ascii_general_ci"),
                    ParentId = table.Column<Guid>(type: "char(36)", nullable: false, collation: "ascii_general_ci"),
                    DeptName = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    OrderNum = table.Column<int>(type: "int", nullable: false),
                    Phone = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Status = table.Column<int>(type: "int", nullable: false),
                    IsDeleted = table.Column<bool>(type: "tinyint(1)", nullable: false, defaultValue: false),
                    TenantId = table.Column<Guid>(type: "char(36)", nullable: true, collation: "ascii_general_ci"),
                    ExtraProperties = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    ConcurrencyStamp = table.Column<string>(type: "varchar(40)", maxLength: 40, nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    CreationTime = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    CreatorId = table.Column<Guid>(type: "char(36)", nullable: true, collation: "ascii_general_ci"),
                    LastModificationTime = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    LastModifierId = table.Column<Guid>(type: "char(36)", nullable: true, collation: "ascii_general_ci")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_t_sys_dept", x => x.Id);
                },
                comment: "部门表")
                .Annotation("MySql:CharSet", "utf8mb4");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "t_sys_dept");

            migrationBuilder.DropPrimaryKey(
                name: "PK_t_sys_user_expand",
                table: "t_sys_user_expand");

            migrationBuilder.DropPrimaryKey(
                name: "PK_t_sys_role_menu",
                table: "t_sys_role_menu");

            migrationBuilder.DropPrimaryKey(
                name: "PK_t_sys_menu",
                table: "t_sys_menu");

            migrationBuilder.DropPrimaryKey(
                name: "PK_t_sys_dictdata",
                table: "t_sys_dictdata");

            migrationBuilder.RenameTable(
                name: "t_sys_user_expand",
                newName: "t_identity_user_expand");

            migrationBuilder.RenameTable(
                name: "t_sys_role_menu",
                newName: "t_identity_role_menu");

            migrationBuilder.RenameTable(
                name: "t_sys_menu",
                newName: "t_identity_menu");

            migrationBuilder.RenameTable(
                name: "t_sys_dictdata",
                newName: "t_identity_dictdata");

            migrationBuilder.RenameColumn(
                name: "Icon",
                table: "t_identity_menu",
                newName: "IconUrl");

            migrationBuilder.AddPrimaryKey(
                name: "PK_t_identity_user_expand",
                table: "t_identity_user_expand",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_t_identity_role_menu",
                table: "t_identity_role_menu",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_t_identity_menu",
                table: "t_identity_menu",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_t_identity_dictdata",
                table: "t_identity_dictdata",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "t_identity_role_authority",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "char(36)", nullable: false, collation: "ascii_general_ci"),
                    AuthId = table.Column<Guid>(type: "char(36)", maxLength: 36, nullable: false, comment: "权限标识", collation: "ascii_general_ci"),
                    ConcurrencyStamp = table.Column<string>(type: "varchar(40)", maxLength: 40, nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    CreationTime = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    CreatorId = table.Column<Guid>(type: "char(36)", nullable: true, collation: "ascii_general_ci"),
                    ExtraProperties = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    IsDeleted = table.Column<bool>(type: "tinyint(1)", nullable: false, defaultValue: false, comment: "是否删除"),
                    LastModificationTime = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    LastModifierId = table.Column<Guid>(type: "char(36)", nullable: true, collation: "ascii_general_ci"),
                    RoleId = table.Column<Guid>(type: "char(36)", maxLength: 36, nullable: false, comment: "角色标识", collation: "ascii_general_ci"),
                    TenantId = table.Column<Guid>(type: "char(36)", nullable: true, collation: "ascii_general_ci")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_t_identity_role_authority", x => x.Id);
                },
                comment: "角色权限中间表")
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "t_identity_role_expand",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "char(36)", nullable: false, collation: "ascii_general_ci"),
                    ConcurrencyStamp = table.Column<string>(type: "varchar(40)", maxLength: 40, nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    CreationTime = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    CreatorId = table.Column<Guid>(type: "char(36)", nullable: true, collation: "ascii_general_ci"),
                    ExtraProperties = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    LastModificationTime = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    LastModifierId = table.Column<Guid>(type: "char(36)", nullable: true, collation: "ascii_general_ci"),
                    RoleId = table.Column<Guid>(type: "char(36)", maxLength: 36, nullable: false, comment: "角色标识", collation: "ascii_general_ci"),
                    RoleStatus = table.Column<int>(type: "int", maxLength: 36, nullable: false, comment: "用户状态：0启用、1禁用"),
                    RoleType = table.Column<int>(type: "int", nullable: false, comment: "角色类型"),
                    SystemId = table.Column<string>(type: "varchar(20)", maxLength: 20, nullable: true, comment: "归属系统Id")
                        .Annotation("MySql:CharSet", "utf8mb4")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_t_identity_role_expand", x => x.Id);
                },
                comment: "角色扩展表")
                .Annotation("MySql:CharSet", "utf8mb4");
        }
    }
}
