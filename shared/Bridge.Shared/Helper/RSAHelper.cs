﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridge.Shared.Helper
{


    /// <summary>
    /// RAS非对称算法
    /// </summary>
    //public class RSAHelper
    //{
    //    public static string privateKey { get; set; } = "-----BEGIN RSA PRIVATE KEY-----\r\nMIICXAIBAAKBgQDFOfSSMQCnmHv2AjSq3t7+91mDVviwdGUFS1gsrGZo4eZgS0lH\r\nnpWrjPMMBAccitwg0nhf85qJX77gUw8df46yXO+TcUxb1bY8zhswuaaS7qnUTSxY\r\nw/WcTGJFizqloDRRmjuvYR8SAxaBSFLg9Mmxo95EA/IeFL5mcu4oN2tyJQIDAQAB\r\nAoGALUzjhRSmTthyWI4nJLwuQif0MnuugXEqpu2oYhJzL1H7xVBlk4K0ZAPq626d\r\nZ4vphcxyA7ADZdxNDl9lkGP1FVEhIPltJ01hfm46c4MpiZUAW7/ia3RtBkzBuzj2\r\nHBBccjsUnM1ibo4Zy2Rppkihe7vip2BbCirWzmmLJyBN/t0CQQD2r3qsbXgvoriu\r\niouyVSo9d6vFtQF2aCHdmnb6LOiJhNFXtjxgoRZitV5EVle60SJQ7mEH29lycQvS\r\nn8lKced/AkEAzKxkkyzeeDrfXkmLZ//iPvsUCDhXyCC+LHROEfU293yf0Ko4LR0H\r\nif4qyLfEQGLprHP9Qny08o6Afm+57+XYWwJAFNhrQ1rUxyG2/XjTqTQtJ5iPOVu8\r\nTecqxXEiEK3u0IAauJC7tWanX5v35Upui2jUv4YFSQa470TCPQAnoFTTvQJALUPb\r\nImmD8hcjG1ksiZ2fzcn9jivXzhzfsjkuou4Wb/mR5rYeomuPJUee8rEhqBO4Fjm4\r\nwyGNay9034GaH6I3jQJBALugkXWXjIq5sdIVo0q+aG8poM8xzYf/wgZF6PH0fjEW\r\nZG90QrOV2Qm7qK2hPufBh1p7jkeyjFQeghq3ZFbgp+4=\r\n-----END RSA PRIVATE KEY-----";
    //    public static string publicKey { get; set; } = "-----BEGIN PUBLIC KEY-----\r\nMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDFOfSSMQCnmHv2AjSq3t7+91mD\r\nVviwdGUFS1gsrGZo4eZgS0lHnpWrjPMMBAccitwg0nhf85qJX77gUw8df46yXO+T\r\ncUxb1bY8zhswuaaS7qnUTSxYw/WcTGJFizqloDRRmjuvYR8SAxaBSFLg9Mmxo95E\r\nA/IeFL5mcu4oN2tyJQIDAQAB\r\n-----END PUBLIC KEY-----";

    //    /// <summary>
    //    /// RSA解密
    //    /// </summary>
    //    /// <param name="decryptstring">待解密的字符串(Base64)</param>
    //    /// <returns>解密后的字符串</returns>
    //    public static string Decrypt(string decryptstring)
    //    {
    //        using (TextReader reader = new StringReader(privateKey))
    //        {
    //            dynamic key = new PemReader(reader).ReadObject();
    //            var rsaDecrypt = new Pkcs1Encoding(new RsaEngine());
    //            if (key is AsymmetricKeyParameter)
    //            {
    //                key = (AsymmetricKeyParameter)key;
    //            }
    //            else if (key is AsymmetricCipherKeyPair)
    //            {
    //                key = ((AsymmetricCipherKeyPair)key).Private;
    //            }
    //            rsaDecrypt.Init(false, key);  //这里加密是true；解密是false  

    //            byte[] entData = Convert.FromBase64String(decryptstring);
    //            entData = rsaDecrypt.ProcessBlock(entData, 0, entData.Length);
    //            return Encoding.UTF8.GetString(entData);
    //        }
    //    }


    //    /// <summary>
    //    /// 加密
    //    /// </summary>
    //    /// <param name="encryptstring">待加密的字符串</param>
    //    /// <returns>加密后的Base64</returns>
    //    public static string Encrypt(string encryptstring)
    //    {
    //        using (TextReader reader = new StringReader(publicKey))
    //        {
    //            AsymmetricKeyParameter key = new PemReader(reader).ReadObject() as AsymmetricKeyParameter;
    //            Pkcs1Encoding pkcs1 = new Pkcs1Encoding(new RsaEngine());
    //            pkcs1.Init(true, key);//加密是true；解密是false;
    //            byte[] entData = Encoding.UTF8.GetBytes(encryptstring);
    //            entData = pkcs1.ProcessBlock(entData, 0, entData.Length);
    //            return Convert.ToBase64String(entData);
    //        }
    //    }
    //}
}
