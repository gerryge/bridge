﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Bridge.Shared.Helper
{
    public class EnumHelper
    {
        public static List<Type> EnumTypes = null;

        static EnumHelper()
        {
            EnumTypes = Assembly.GetExecutingAssembly().GetTypes().Where(p => p.BaseType == typeof(System.Enum)).ToList();
        }


        /// <summary>
        /// 通过枚举名称获取字典信息
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static List<EnumItem> GetEnumItemListByEnumName(string name)
        {
            var list = new List<EnumItem>();
            var lowerName = name.ToLower();
            if (!lowerName.EndsWith("enum"))
            {
                lowerName += "enum";
            }

            Type type = EnumTypes.FirstOrDefault(p => p.Name.ToLower() == lowerName || p.Name.ToLower() == name.ToLower());
            if (type == null)
            {
                throw new Exception("枚举类型不存在");
            }

            var members = type.GetMembers();

            foreach (var member in members)
            {
                DisplayAttribute attribute = Attribute.GetCustomAttribute(member, typeof(DisplayAttribute)) as DisplayAttribute;

                if (attribute != null)
                {
                    var returnValues = new EnumItem
                    {
                        Key = member.Name,
                        Value = Convert.ToInt32(System.Enum.Parse(type, member.Name)).ToString()
                    };

                    if (attribute.Name != null)
                    {
                        returnValues.Desc = attribute.Name;
                    }
                    else if (attribute.Description != null)
                    {
                        returnValues.Desc = attribute.Description;
                    }
                    else
                    {
                        returnValues.Desc = member.Name;
                    }

                    list.Add(returnValues);

                }
            }

            return list;
        }

        /// <summary>
        /// 通过枚举名称获取字典信息
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static Dictionary<string, string> GetEnumDataListByEnumName(string name)
        {

            var lowerName = name.ToLower();
            if (!lowerName.EndsWith("enum"))
            {
                lowerName += "enum";
            }

            Type type = EnumTypes.FirstOrDefault(p => p.Name.ToLower() == lowerName || p.Name.ToLower() == name.ToLower());
            if (type == null)
            {
                throw new Exception("枚举类型不存在");
            }
            var dic = GetCommonEnumMembers(type);

            return dic;
        }

        public static Dictionary<string, string> GetCommonEnumMembers(Type type)
        {
            var returnValues = new Dictionary<string, string>();
            var members = type.GetMembers();

            foreach (var member in members)
            {
                DisplayAttribute attribute = Attribute.GetCustomAttribute(member, typeof(DisplayAttribute)) as DisplayAttribute;

                if (attribute != null)
                {
                    if (attribute.Name != null)
                    {
                        returnValues.Add(Convert.ToInt32(System.Enum.Parse(type, member.Name)).ToString(), attribute.Name);
                    }
                    else if (attribute.Description != null)
                    {
                        returnValues.Add(Convert.ToInt32(System.Enum.Parse(type, member.Name)).ToString(), attribute.Description);
                    }
                    else
                    {
                        returnValues.Add(Convert.ToInt32(System.Enum.Parse(type, member.Name)).ToString(), member.Name);
                    }

                }
            }

            return returnValues;
        }

        /// <summary>
        /// 获取Display名
        /// </summary>
        /// <param name="type"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetDisplayAttributeName(Type type, object value)
        {

            string name = System.Enum.GetName(type, value);
            if (name == null)
            {
                return null;
            }
            FieldInfo field = type.GetField(name);
            DisplayAttribute attribute = Attribute.GetCustomAttribute(field, typeof(DisplayAttribute)) as DisplayAttribute;

            return attribute == null ? null : attribute.Name;
        }
        /// <summary>
        /// 获取display描述
        /// </summary>
        /// <param name="type"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetDisplayAttributeDescription(Type type, object value)
        {
            string name = System.Enum.GetName(type, value);
            if (name == null)
            {
                return null;
            }
            FieldInfo field = type.GetField(name);
            DisplayAttribute attribute = Attribute.GetCustomAttribute(field, typeof(DisplayAttribute)) as DisplayAttribute;

            return attribute == null ? null : attribute.Description;
        }

        /// <summary>
        /// 获取枚举字典信息
        /// key 枚举值 value 枚举DisPlay
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        static public Dictionary<string, string> GetEnumMembers(Type type)
        {
            var returnValues = new Dictionary<string, string>();
            var members = type.GetMembers();

            foreach (var member in members)
            {
                DisplayAttribute attribute = Attribute.GetCustomAttribute(member, typeof(DisplayAttribute)) as DisplayAttribute;

                if (attribute != null)
                {
                    returnValues.Add(Convert.ToInt32(System.Enum.Parse(type, member.Name)).ToString(), attribute.Name);
                }
            }

            return returnValues;
        }

        /// <summary>
        /// 获取枚举字典信息
        /// key 枚举值 value 枚举DisPlay
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        static public List<KeyValuePair<string, string>> GetEnumMembersKeyValuePair(Type type)
        {
            var returnValues = new List<KeyValuePair<string, string>>();
            var members = type.GetMembers();

            foreach (var member in members)
            {
                DisplayAttribute attribute = Attribute.GetCustomAttribute(member, typeof(DisplayAttribute)) as DisplayAttribute;

                if (attribute != null)
                {
                    returnValues.Add(new KeyValuePair<string, string>(Convert.ToInt32(System.Enum.Parse(type, member.Name)).ToString(), attribute.Name));
                }
            }

            return returnValues;
        }

        /// <summary>
        /// 获取枚举字典信息
        /// key 枚举值 value 枚举DisPlay
        /// </summary>
        /// <param name="type"></param>
        /// <param name="isShowAll">0:返回枚举类型，1:返回带全部都枚举类型</param>
        /// <returns></returns>
        static public Dictionary<string, string> GetEnumMembersList(Type type, int isShowAll = 0)
        {
            var returnValues = new Dictionary<string, string>();
            if (isShowAll == 1)
            {
                returnValues.Add("0", "--请选择--");
            }
            var members = type.GetMembers();
            foreach (var member in members)
            {
                DisplayAttribute attribute = Attribute.GetCustomAttribute(member, typeof(DisplayAttribute)) as DisplayAttribute;

                if (attribute != null)
                {
                    returnValues.Add(Convert.ToInt32(System.Enum.Parse(type, member.Name)).ToString(), attribute.Name);
                }
            }
            return returnValues;
        }

        /// <summary>
        /// 获取枚举字典信息
        /// key 枚举属性名 value 枚举DisPlay
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        static public Dictionary<string, string> GetEnumKeyValuePair(Type type)
        {
            var returnValues = new Dictionary<string, string>();
            var members = type.GetMembers();

            foreach (var member in members)
            {
                DisplayAttribute attribute = Attribute.GetCustomAttribute(member, typeof(DisplayAttribute)) as DisplayAttribute;

                if (attribute != null)
                {
                    returnValues.Add(member.Name, attribute.Name);
                }
            }

            return returnValues;
        }

        /// <summary>
        /// 是否合法枚举
        /// </summary>
        /// <returns></returns>
        public static bool IsValidEnumValue(Type type, int value)
        {
            var list = GetEnumMembers(type);
            if (list != null && list.Any())
            {
                return list.ContainsKey(value.ToString());
            }
            return false;
        }


        /// <summary>
        /// 将枚举转为集合
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static List<EnumEntity> EnumToList<T>()
        {
            List<EnumEntity> list = new List<EnumEntity>();

            foreach (var e in System.Enum.GetValues(typeof(T)))
            {
                EnumEntity m = new EnumEntity();
                object[] objArr = e.GetType().GetField(e.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), true);
                if (objArr != null && objArr.Length > 0)
                {
                    DescriptionAttribute da = objArr[0] as DescriptionAttribute;
                    m.Desction = da.Description;
                }
                m.EnumValue = Convert.ToInt32(e);
                m.EnumName = e.ToString();
                list.Add(m);
            }
            return list;
        }


        /// <summary>
        /// 枚举实体
        /// </summary>
        public class EnumEntity
        {
            /// <summary>  
            /// 枚举的描述  
            /// </summary>  
            public string Desction { set; get; }

            /// <summary>  
            /// 枚举名称  
            /// </summary>  
            public string EnumName { set; get; }

            /// <summary>  
            /// 枚举对象的值  
            /// </summary>  
            public int EnumValue { set; get; }
        }

    }

    public class EnumItem
    {
        /// <summary>
        /// Value
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Key
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Desc
        /// </summary>
        public string Desc { get; set; }
    }
}
