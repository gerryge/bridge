﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridge.Shared.Filter
{
    public class CoustomAuthorizeFilter : AuthorizeFilter
    {

        public CoustomAuthorizeFilter(IServiceProvider serviceProvider)
            : base(policyProvider: serviceProvider.GetRequiredService<IAuthorizationPolicyProvider>(), authorizeData: new[] { new AuthorizeAttribute() })
        {
        }
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            OnAuthorizationAsync(context);
        }

        public override Task OnAuthorizationAsync(AuthorizationFilterContext context)
        {
            //过滤动态API
            if (context.HttpContext.Request.Path.Value.EndsWith("/QuartzUI"))
            {
                return Task.CompletedTask;
            }
            return base.OnAuthorizationAsync(context);
        }

    }
}
