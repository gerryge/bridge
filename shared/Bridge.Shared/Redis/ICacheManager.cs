﻿using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Bridge.Shared.Redis
{
    public interface ICacheManager
    {
        /// <summary>
        /// Redis锁
        /// </summary>
        /// <param name="key"></param>
        /// <param name="token"></param>
        /// <param name="span"></param>
        /// <returns></returns>
        Task<T> LockQueryAsync<T>(string key);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        Task<bool> LockTakeAsync(string key, RedisValue token, TimeSpan span);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        Task<bool> ReleaseTakeAsync(string key, RedisValue token);

        #region Hash 操作

        /// <summary>
        /// 判断该字段是否存在 hash 中
        /// </summary>
        /// <param name="redisKey"></param>
        /// <param name="hashField"></param>
        /// <returns></returns>
        Task<bool> HashExistsAsync(string redisKey, string hashField);

        /// <summary>
        /// 从 hash 中移除指定字段
        /// </summary>
        /// <param name="redisKey"></param>
        /// <param name="hashField"></param>
        /// <returns></returns>
        Task<bool> HashDeleteAsync(string redisKey, string hashField);


        /// <summary>
        /// 从 hash 中移除指定字段
        /// </summary>
        /// <param name="redisKey"></param>
        /// <param name="hashField"></param>
        /// <returns></returns>
        Task<long> HashDeleteAsync(string redisKey, IEnumerable<RedisValue> hashField);

        /// <summary>
        /// 在 hash 设定值（序列化）
        /// </summary>
        /// <param name="redisKey"></param>
        /// <param name="hashField"></param>
        /// <param name="value"></param>
        /// <param name="expireTime">过期时间 分钟 默认五分钟</param>
        /// <returns></returns>
        Task<bool> HashSetAsync<T>(string redisKey, string hashField, T value, int expireTime = 5);

        /// <summary>
        /// 在 hash 中获取值（反序列化）
        /// </summary>
        /// <param name="redisKey"></param>
        /// <param name="hashField"></param>
        /// <returns></returns>
        Task<T> HashGetAsync<T>(string redisKey, string hashField, bool prefix = true);

        Task<List<T>> HashGetAsync<T>(string redisKey, List<string> hashFields, bool prefix = true);

        /// <summary>
        /// Redis散列数据类型 获取key中所有field的值。
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="redisKey"></param>
        /// <param name="flags"></param>
        /// <returns></returns>
        Task<List<T>> HashGetAllValuesAsync<T>(string redisKey, CommandFlags flags = CommandFlags.None);

        #endregion Hash 操作

        #region string操作
        /// <summary>
        /// String 写
        /// </summary>
        /// <param name="redisKey"></param>
        /// <param name="hashField"></param>
        ///  <param name="expireTime">过期时间 分钟 默认五分钟</param>
        /// <returns></returns>
        Task<bool> StringSet(string redisKey, string value, int expireTime = 5);

        /// <summary>
        /// String 读
        /// </summary>
        /// <param name="redisKey"></param>
        /// <param name="hashField"></param>
        /// <returns></returns>
        Task<string> StringGet(string redisKey);




        #endregion

        #region 原子递增
        Task<long> IncrementAsync(string redisKey, long value = 1, CommandFlags flags = CommandFlags.None);

        Task<long> IncrementAsyncWithTime(string redisKey, long value = 1, CommandFlags flags = CommandFlags.None, int expireTime = 5);
        #endregion


        #region 有效期
        Task<bool> KeyExpireAsync(string redisKey, TimeSpan? expiry, CommandFlags flags = CommandFlags.None);

        #endregion

        /// <summary>
        /// 移除Key
        /// </summary>
        /// <param name="redisKey"></param>
        /// <param name="flags"></param>
        /// <returns></returns>
        Task<bool> RemoveKeyAsync(string redisKey, CommandFlags flags = CommandFlags.None);

        /// <summary>
        /// 保存
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="expiry"></param>
        /// <param name="when"></param>
        /// <param name="flags"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        Task<bool> AddAsync<T>(string key, T value, TimeSpan? expiry = null, When when = When.Always, CommandFlags flags = CommandFlags.None, CancellationToken token = default);


        /// <summary>
        /// 获取
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="flags"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        Task<T> GetAsync<T>(string key, CommandFlags flags = CommandFlags.None, CancellationToken token = default);



        #region 队列
        /// <summary>
        /// 入队
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="redisKey"></param>
        /// <param name="value"></param>
        /// <param name="flags"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        Task<long> EnqueueList<T>(string redisKey, T value, CommandFlags flags = CommandFlags.None, CancellationToken token = default);

        /// <summary>
        /// 出队列
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="redisKey"></param>
        /// <param name="index"></param>
        /// <param name="flags"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        Task<T> DequeueList<T>(string redisKey, long index, CommandFlags flags = CommandFlags.None, CancellationToken token = default);
        #endregion
    }
}
