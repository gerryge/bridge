﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridge.Shared.Redis
{
    public class RedisConfiguration
    {
        public string Configuration { get; set; }
        public int DatabaseId { get; set; }
        public bool IgnoreTimeoutException { get; set; }
        public string RedisDataProtectionKey { get; set; }
    }
}
