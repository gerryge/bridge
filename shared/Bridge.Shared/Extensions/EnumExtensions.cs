﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridge.Shared.Extensions
{
    public static class EnumExtensions
    {
        /// <summary>
        /// 返回枚举项的描述信息。
        /// </summary>
        /// <param name="value">要获取描述信息的枚举项。</param>
        /// <returns>枚举想的描述信息。</returns>
        public static string GetDescription(this System.Enum value)
        {
            var enumType = value.GetType();
            // 获取枚举常数名称。
            var name = System.Enum.GetName(enumType, value);
            if (name == null) return null;
            // 获取枚举字段。
            var fieldInfo = enumType.GetField(name);
            if (fieldInfo == null) return null;
            // 获取描述的属性。
            if (Attribute.GetCustomAttribute(fieldInfo,
                typeof(DescriptionAttribute), false) is DescriptionAttribute attr)
            {
                return attr.Description;
            }

            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetDisplay(this System.Enum value)
        {
            var enumType = value.GetType();
            // 获取枚举常数名称。
            var name = System.Enum.GetName(enumType, value);
            if (name == null) return null;
            // 获取枚举字段。
            var fieldInfo = enumType.GetField(name);
            if (fieldInfo == null) return null;
            // 获取Display描述的属性。
            if (Attribute.GetCustomAttribute(fieldInfo,
                typeof(DisplayAttribute), false) is DisplayAttribute attr)
            {
                return attr.Name;
            }

            return null;
        }

    }
}
