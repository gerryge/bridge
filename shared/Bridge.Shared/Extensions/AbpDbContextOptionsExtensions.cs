﻿using Bridge.Shared.Enums.Common;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Pomelo.EntityFrameworkCore.MySql.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.EntityFrameworkCore;

namespace Bridge.Shared.Extensions
{
    public static class AbpDbContextOptionsExtensions
    {
        public static void UseDbCon([NotNull] this AbpDbContextOptions options, IConfiguration configuration, bool mysqlSchemaBehavior = false)
        {
            DbServerType dbServer = configuration["DbServerType"] == null ? DbServerType.MySQL : (DbServerType)System.Enum.Parse(typeof(DbServerType), configuration["DbServerType"]);
            switch (dbServer)
            {
                case DbServerType.MySQL:
                    if (mysqlSchemaBehavior)
                    {
                        options.UseMySQL(a => a.SchemaBehavior(MySqlSchemaBehavior.Ignore));
                    }
                    else
                    {
                        options.UseMySQL(a => a.SchemaBehavior(MySqlSchemaBehavior.Translate, (schema, table) =>
                        {
                            return $"{schema}_{table}";
                        }));
                    }

                    break;
                case DbServerType.DmSQL:
                    options.Configure(a =>
                    {
                        a.DbContextOptions.UseDm(a.ConnectionString);

                        //string dmConnectionStr = "Server=LOCALHOST;User=SYSDBA;Password=SYSDBA;";
                        //a.DbContextOptions.UseDm(dmConnectionStr);
                    
                    });
                    break;
                case DbServerType.SQLServer:
                    options.UseSqlServer();
                    break;
                default:
                    options.UseMySQL(a => a.SchemaBehavior(MySqlSchemaBehavior.Translate, (schema, table) =>
                    {
                        return $"{schema}_{table}";
                    }
                    ));
                    break;

            }
        }
    }
}
