﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using NPOI.OpenXmlFormats.Wordprocessing;
using Rong.EasyExcel.Npoi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.AspNetCore.Mvc.AntiForgery;
using Volo.Abp.Data;
using Volo.Abp.EntityFrameworkCore;
using Volo.Abp.Localization;
using Volo.Abp.MultiTenancy;

namespace Bridge.Shared.Extensions
{
    public static class ServiceCollectionExtensions
    {

        public static void ConfigureCommonServices(this IServiceCollection service, InitializerOptions initOptions)
        {
            var configuration = service.GetConfiguration();
            //service.Configure<MvcNewtonsoftJsonOptions>(options =>
            //{
            //    options.SerializerSettings.DateFormatString = "yyyy-MM-dd HH:mm:ss";//对类型为DateTime的生效
            //});
            //配置多租户
            service.Configure<AbpMultiTenancyOptions>(options =>
            {
                options.IsEnabled = BridgeConsts.IsMultiTenancyEnabled;
            });
            //配置JWT
            service.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                 .AddJwtBearer(options =>
                 {
                     options.Authority = configuration["AuthServer:Authority"];
                     options.Audience = configuration["AuthServer:ApiName"];
                     options.RequireHttpsMetadata = Convert.ToBoolean(configuration["AuthServer:RequireHttpsMetadata"]);
                     options.TokenValidationParameters.ValidateIssuer = false;
                 });
            //配置swaggerk中的【Authorize】按钮
            service.AddSwaggerGen(options =>
             {
                 //options.SwaggerDoc("v1", new OpenApiInfo { Title = "Identity Service API", Version = "v1" });
                 //启用Swagger中的【Authorize】按钮。这样就不用每个项目的AddSwaggerGen中单独配置了
                 options.AddAuthenticationHeader();
             });
            //配置多语言
            service.Configure<AbpLocalizationOptions>(options =>
            {
                options.Languages.Add(new LanguageInfo("en", "en", "English"));
            });
            //用环境变量中的用户名密码替换
            service.Configure<AbpDbConnectionOptions>(options =>
            {
                var sqlname = configuration["DBUserName"];
                var sqlpwd = configuration["DBPassword"];
                options.ConnectionStrings.Default = options.ConnectionStrings.Default?.Replace("DBUserName", sqlname).Replace("DBPassword", sqlpwd);
               
            });
            //配置数据库连接
            service.Configure<AbpDbContextOptions>(options =>
            {
                options.UseDbCon(configuration);

            });
            //配置跨域
            service.AddCors(options => options.AddPolicy("CorsPolicy",
        builder =>
        {
            builder.AllowAnyMethod()
                 .SetIsOriginAllowed(_ => true)
                 .AllowAnyHeader()
                 .AllowCredentials();
        }));
            //配置不验证防伪令牌
            service.Configure<AbpAntiForgeryOptions>(options =>
            {
                options.AutoValidate = false; //表示不验证防伪令牌
            });

            //配置redis
            service.UseRedis(configuration);
            //导出Excel依赖
            service.AddNpoiExcel();
        }

       

    }
    public class InitializerOptions
    {
        public InitializerOptions() { }
    }
}
