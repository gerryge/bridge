﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridge.Shared.Extensions
{
    public static class SwaggerGenOptionsExtensions
    {
        /// <summary>
        /// 为Swagger增加Authentication报文头
        /// </summary>
        /// <param name="c"></param>
        public static void AddAuthenticationHeader(this SwaggerGenOptions c)
        {
            c.DocInclusionPredicate((docName, description) => true);
            c.CustomSchemaIds(type => type.FullName);
            c.AddSecurityDefinition("Authorization", new OpenApiSecurityScheme
            {
                Description = "请输入JWT令牌，例如：Bearer 12345abcdef",
                Name = "Authorization",
                In = ParameterLocation.Header,
                Type = SecuritySchemeType.ApiKey,
                Scheme = "Bearer"
            });

            c.AddSecurityRequirement(new OpenApiSecurityRequirement()
            {
                {
                    new OpenApiSecurityScheme
                    {
                        Reference = new OpenApiReference
                        {
                            Type = ReferenceType.SecurityScheme,
                            Id = "Bearer"
                        },
                        Scheme = "oauth2",
                        Name = "Bearer",
                        In = ParameterLocation.Header,
                    },
                    new List<string>()
                }
            });
        }
    }
}
