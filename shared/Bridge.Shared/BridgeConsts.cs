﻿using Bridge.Shared.Enums.Common;

namespace Bridge.Shared
{
    public class BridgeConsts
    {
        public const bool IsMultiTenancyEnabled = true;

         public const DbServerType mysql = DbServerType.MySQL;
         public const DbServerType dm = DbServerType.DmSQL; 
    }
}
