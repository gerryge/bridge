﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridge.Shared.Enums.Common
{
    public enum EnterpriseNature
    {
        /// <summary>
        /// 有限责任公司
        /// </summary>
        [Description("有限责任公司")]
        CompanyWithLimitedLiability = 1,
        /// <summary>
        /// 股份有限公司
        /// </summary>
        [Description("股份有限公司")]
        LimitedCompany = 2,

        /// <summary>
        /// 股份合作公司
        /// </summary>
        [Description("股份合作公司")]
        JointStockCooperativeCompany = 3,

        /// <summary>
        /// 国有企业
        /// </summary>
        [Description("国有企业")]
        StateOwnedEnterprise = 4,

        /// <summary>
        /// 集体所有制
        /// </summary>
        [Description("集体所有制")]
        CollectiveOwnership = 5,

        /// <summary>
        /// 个体工商户
        /// </summary>
        [Description("个体工商户")]
        IndividualIndustrialCommercialHouseholds = 6,

        /// <summary>
        /// 独资企业
        /// </summary>
        [Description("独资企业")]
        SoleProprietorship = 7,

        /// <summary>
        /// 有限合伙
        /// </summary>
        [Description("有限合伙")]
        LimitedPartnership = 8,

        /// <summary>
        /// 普通合伴
        /// </summary>
        [Description("普通合伴")]
        GeneralPartner = 9,

        /// <summary>
        /// 外商投资
        /// </summary>
        [Description("外商投资")]
        ForeignInvestment = 10,

        /// <summary>
        /// 港、澳、台商投资
        /// </summary>
        [Description("港、澳、台商投资")]
        InvestmentByHongKongMacaoAndTaiwanBusinessmen = 11,

        /// <summary>
        /// 联营企业
        /// </summary>
        [Description("联营企业")]
        JointEnterprise = 12,

        /// <summary>
        /// 私营企业
        /// </summary>
        [Description("私营企业")]
        PrivateEnterprise = 13,
    }
}
