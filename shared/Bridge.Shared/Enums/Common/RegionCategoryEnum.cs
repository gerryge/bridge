﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridge.Shared.Enums.Common
{
    /// <summary>
    /// 区域类别
    /// </summary>
    public enum RegionCategoryEnum
    {
        /// <summary>
        /// 整件存放区
        /// </summary>
        [Display(Name = "无")]
        None = 1,
        /// <summary>
        /// 退货区
        /// </summary>
        [Display(Name = "退货区")]
        Return = 2,
        /// <summary>
        /// 不良品
        /// </summary>
        [Display(Name = "不良品")]
        Rejects = 3
    }
}
