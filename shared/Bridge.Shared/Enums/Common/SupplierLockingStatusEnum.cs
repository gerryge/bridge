﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridge.Shared.Enums.Common
{
    public enum SupplierLockingStatusEnum
    {
        /// <summary>
        /// 启用
        /// </summary>
        [Display(Description = "启用")]
        Enable = 1,
        /// <summary>
        /// 禁用
        /// </summary>
        [Display(Description = "禁用")]
        Disable = 2,

    }
}
