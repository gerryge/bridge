﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridge.Shared.Enums.Common
{
    /// <summary>
    /// 供应商企业类型
    /// </summary>
    public enum EnterpriseType
    {
        /// <summary>
        /// 供应商
        /// </summary>
        [Display(Name = "供应商")]
        Supplier = 1,
        /// <summary>
        /// 生产商
        /// </summary>
        [Display(Name = "生产商")]
        Manufacturer = 2,

        /// <summary>
        /// 代理商
        /// </summary>
        [Display(Name = "代理商")]
        Agenter = 3
    }
}
