﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridge.Shared.Enums.Common
{
    /// <summary>
    /// 供应商信息单据状态 0待提交 1已通过审核 2待审核 3已驳回
    /// </summary>
    public enum SupplierStatusEnum
    {
        /// <summary>
        /// 启用
        /// </summary>
        [Display(Description = "启用")]
        Enable = 1,
        /// <summary>
        /// 禁用
        /// </summary>
        [Display(Description = "禁用")]
        Disable = 2,

    }
}
