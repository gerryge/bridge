﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridge.Shared.Enums.Common
{
    /// <summary>
    /// 货位区域属性
    /// </summary>
    public enum RegionAttributeEnum
    {
        /// <summary>
        /// 整件存放区
        /// </summary>
        [Display(Name = "整件存放区")]
        WholePiece = 1,
        /// <summary>
        /// 零货存放区
        /// </summary>
        [Display(Name = "零货存放区")]
        Selling = 2,
        /// <summary>
        /// 混合存放区
        /// </summary>
        [Display(Name = "混合存放区")]
        Blend = 3,
    }
}
