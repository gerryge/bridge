﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridge.Shared.Enums.Common
{
    public enum DbServerType
    {
        /// <summary>
        /// SqlServer
        /// </summary>
        [Description("SqlServer")] 
        SQLServer,


        /// <summary>
        ///   MySQL
        /// </summary>
        [Description("MySql")]
        MySQL,


        /// <summary>
        ///  达梦数据库
        /// </summary>
        [Description("DMSql")]
        DmSQL,
    }
}
