﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridge.Shared.Enums.IdentityService
{
    public enum RoleStatusEnum
    {
        /// <summary>
        /// 启用
        /// </summary>
        [Display(Name = "启用")]
        First = 0,
        /// <summary>
        /// 禁用
        /// </summary>
        [Display(Name = "禁用")]
        Second = 1,
    }
}
