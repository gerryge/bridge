﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridge.Shared.Enums.IdentityService
{
    public enum UserStatusEnum
    {
        /// <summary>
        /// 启用
        /// </summary>
        [Display(Name = "启用")]
        Enable = 0,
        /// <summary>
        /// 停用
        /// </summary>
        [Display(Name = "停用")]
        Disable = 1
    }
}
