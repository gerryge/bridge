﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridge.Shared.Enums.IdentityService
{
    /// <summary>
    /// 登录结果枚举
    /// </summary>
    public enum LoginResultEnum
    {
        /// <summary>
        /// 登录成功
        /// </summary>
        [Display(Name = "登录成功"), Description("登录成功")]
        Success = 0,
        /// <summary>
        /// 您的账号密码输入错误已超过5次！请10分钟后再尝试，也可通过“忘记密码”进行重置后再登录
        /// </summary>
        [Display(Name = "您的账号密码输入错误已超过5次！请10分钟后再尝试，也可通过“忘记密码”进行重置后再登录"), Description("您的账号密码输入错误已超过5次！请10分钟后再尝试，也可通过“忘记密码”进行重置后再登录")]
        PwdErrorFive = 1,


    }
}
