﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridge.Shared.Enums.IdentityService
{
    public enum LoginResultTypeEnum
    {
        /// <summary>
        /// 登录成功
        /// </summary>
        [Display(Name = "登录成功"), Description("登录成功")]
        Success = 1,
        /// <summary>
        /// 无效的用户名或密码
        /// </summary>
        [Display(Name = "无效的用户名或密码"), Description("无效的用户名或密码")]
        InvalidUserNameOrPassword = 2,
        /// <summary>
        /// 不允许登录
        /// </summary>
        [Display(Name = "不允许登录"), Description("不允许登录")]
        NotAllowed = 3,
        /// <summary>
        /// 用户被锁定
        /// </summary>
        [Display(Name = "用户被锁定"), Description("用户被锁定")]
        LockedOut = 4,
        /// <summary>
        /// 请求错误
        /// </summary>
        [Display(Name = "请求错误"), Description("请求错误")]
        RequiresTwoFactor = 5
    }
}
