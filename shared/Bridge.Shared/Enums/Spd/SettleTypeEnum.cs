﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridge.Shared.Enums.Wms
{
    /// <summary>
    /// 结算方式：仓库入库结算、科室收货结算、科室消耗结算
    /// </summary>
    public enum SettleTypeEnum
    {
        /// <summary>
        /// 仓库入库结算
        /// </summary>
        [Display(Name = "仓库入库结算")]
        StockIn = 0,

        /// <summary>
        /// 科室收货结算
        /// </summary>
        [Display(Name = "科室收货结算")]
        DeptHarvest = 1,

        /// <summary>
        /// 科室消耗结算
        /// </summary>
        [Display(Name = "科室消耗结算")]
        DeptConsume = 2
    }
}
