﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridge.Shared.Enums.Wms
{
    public enum DrugTrackCodeTypeEnum
    {
        /// <summary>
        /// 无条码管理
        /// </summary>
        [Display(Description = "无条码管理")]
        NoTrackCode = 0,
        /// <summary>
        /// 外部电子监管码
        /// </summary>
        [Display(Description = "外部电子监管码")]
        OutTrackCode = 1,

        /// <summary>
        /// 院内自生成追溯码
        /// </summary>
        [Display(Description = "院内自生成追溯码")]
        HosptialTrackCode = 2
    }
}
