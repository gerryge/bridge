﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridge.Shared.Enums.Wms
{
    public enum ReadySettleStatusEnum
    {
        /// <summary>
        /// 待生成
        /// 生成结算单之前的状态
        /// </summary>
        [Display(Name = "待生成")]
        Create = 0,
        /// <summary>
        /// 待审核
        /// 结算单生成之后，审核之前
        /// </summary>
        [Display(Name = "待审核")]
        Settle = 1,
        /// <summary>
        /// 待推送
        /// 结算数据审核之后，推送之前
        /// </summary>
        [Display(Name = "待推送")]
        SettleApproval = 2,
        /// <summary>
        /// 待开票
        /// 推送至scm
        /// </summary>
        [Display(Name = "待开票")]
        SynchScm = 3,
        /// <summary>
        /// 待付款
        /// 发票审核通过之后
        /// </summary>
        [Display(Name = "待付款")]
        PaperApproved = 4,
        /// <summary>
        /// 已付款
        /// 发票付款之后
        /// </summary>
        [Display(Name = "已付款")]

        PaymentCompleted = 5
    }
}
