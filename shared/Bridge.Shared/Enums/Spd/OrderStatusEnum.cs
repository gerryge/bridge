﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridge.Shared.Enums.Wms
{
    public enum  OrderStatusEnum
    {
        [Description("请选择")]
        全部 = -1,
        [Description("暂存")]
        暂存 = 0,
        [Description("已作废")]
        已作废 = 1,
        [Description("已提交")]
        已提交 = 2,
        [Description("已驳回")]
        已驳回 = 3,
        [Description("已审核")]
        已审核 = 5,
        [Description("已完成")]
        已完成 = 10
    }
}
