﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridge.Shared.Enums.Wms
{
    public enum InStockStatusEnum
    {
        /// <summary>
        /// 请选择
        /// </summary>
        [Display(Name = "请选择")]
        [Description("请选择")]
        Null = -1,
        /// <summary>
        /// 暂存
        /// </summary>
        [Display(Name = "暂存")]
        [Description("暂存")]
        Draft = 0,
        /// <summary>
        /// 已作废
        /// </summary>
        [Display(Name = "已作废")]
        [Description("已作废")]
        Voided = 1,
        /// <summary>
        /// 已提交
        /// </summary>
        [Display(Name = "已提交")]
        [Description("已提交")]
        Submitted = 2,
        /// <summary>
        /// 已驳回
        /// </summary>
        [Display(Name = "已驳回")]
        [Description("已驳回")]
        Rejected = 3,
        /// <summary>
        /// 已验收
        /// </summary>
        [Display(Name = "已验收")]
        [Description("已验收")]
        Reviewed = 4,
        /// <summary>
        /// 已审核
        /// </summary>
        [Display(Name = "已审核")]
        [Description("已审核")]
        Warehoused = 5,
        /// <summary>
        /// 已冲销
        /// </summary>
        [Display(Name = "已冲销")]
        [Description("已冲销")]
        WrittenOff = 6
    }
}
